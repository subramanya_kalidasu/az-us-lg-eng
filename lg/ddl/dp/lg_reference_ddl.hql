CREATE VIEW `lg_reference.azid_cross_ref` AS select 
`cross_ref`.`old_az_cust_id` as `old_hcp_az_cust_id`,
`cross_ref`.`new_az_cust_id` as `new_hcp_az_cust_id`,
`cross_ref`.`eff_dt` as `eff_date`
from
`lg_base`.`cross_ref`
CREATE VIEW `lg_reference.demog_address` AS select
`wkdem_address`.`imsdr`,
`wkdem_address`.`hcp_az_cust_id`,
`wkdem_address`.`address_typ` as `address_type`,
`wkdem_address`.`addr_1` as `address1`,
`wkdem_address`.`addr_2` as `address2`,
`wkdem_address`.`addr_3` as `address3`,
`wkdem_address`.`city`,
`wkdem_address`.`state`,
`wkdem_address`.`state_name`,
`wkdem_address`.`zip`,
`wkdem_address`.`counter` as `address_count`
from
 `lg_base`.`wkdem_address`
CREATE VIEW `lg_reference.fasenra_demog` AS select
`fasenra_demog`.`ddd_outlet_number` as `outlet_id`,
`fasenra_demog`.`mii_facility_number`,
`fasenra_demog`.`ims_prescriber_number` as `imsdr`,
`fasenra_demog`.`ddd_outlet_name` as `outlet_cust_name`,
`fasenra_demog`.`ddd_outlet_address` as `outlet_address`,
`fasenra_demog`.`ddd_outlet_city` as `outlet_city`,
`fasenra_demog`.`ddd_outlet_state` as `outlet_state`,
`fasenra_demog`.`ddd_outlet_zip` as `outlet_zip`,
`fasenra_demog`.`mii_fclty_prscbr_name` as `hcp_cust_name`,
`fasenra_demog`.`mii_fclty_prscbr_addr1` as `hcp_address1`,
`fasenra_demog`.`mii_fclty_prscbr_addr2` as `hcp_address2`,
`fasenra_demog`.`mii_fclty_prscbr_addr3` as `hcp_address3`,
`fasenra_demog`.`mii_fclty_prscbr_addr4` as `hcp_address4`,
`fasenra_demog`.`mii_fclty_prscbr_city` as `hcp_city`,
`fasenra_demog`.`mii_fclty_prscbr_state` as `hcp_state`,
`fasenra_demog`.`mii_fclty_prscbr_zip` as `hcp_zip`,
`fasenra_demog`.`olet_az_cust_id` as `outlet_cust_id`,
`fasenra_demog`.`prscbr_az_cust_id` as `hcp_az_cust_id`
from 
`lg_base`.`fasenra_demog`
CREATE VIEW `lg_reference.formulary_restrictions` AS select
`lg_formulary_restrictions_f`.`src_health_plan_id` as `src_health_plan_id`,
`lg_formulary_restrictions_f`.`src_drug_id`
`lg_formulary_restrictions_f`.`restriction_code` as `restriction_code`,
`lg_formulary_restrictions_f`.`restriction_detail_identifier` as `restriction_detail_id`,
`lg_formulary_restrictions_f`.`restriction_detail_text`
`lg_formulary_restrictions_f`.`restriction_additional_information_1` as `restriction_additional_info1`,
`lg_formulary_restrictions_f`.`restriction_additional_information_2` as `restriction_additional_info2`, 
`lg_formulary_restrictions_f`.`step_count` as `step_count`,
`lg_formulary_restrictions_f`.`pa_form`
`lg_formulary_restrictions_f`.`indication` as `indication`, 
`lg_formulary_restrictions_f`.`group_restriction_level` as `group_restriction_level`, 
cast(concat(substr(`lg_formulary_restrictions_f`.`cal_sk`,1,4),'-',substr(`lg_formulary_restrictions_f`.`cal_sk`,5,2),'-',substr(`lg_formulary_restrictions_f`.`cal_sk`,7,2)) as date) as `calendar_date`
from
`lg_base`.`lg_formulary_restrictions_f`
CREATE VIEW `lg_reference.hca_address` AS select 
     `lg_cust_hca_addr_d`.`az_cust_id` as `hca_az_cust_id`,








from 
    `lg_base`.`lg_cust_hca_addr_d`
CREATE VIEW `lg_reference.hca_hca_affiliation` AS select
`lg_cust_hca_hca_affil_prnt_d`.`hca1_az_cust_id`,
`lg_cust_hca_hca_affil_prnt_d`.`hca1_cust_nm` as `hca1_cust_name`,
`lg_cust_hca_hca_affil_prnt_d`.`hca1_cust_addr_ln1` as `hca1_cust_address1`,
`lg_cust_hca_hca_affil_prnt_d`.`hca1_cust_addr_ln2` as `hca1_cust_address2`,
`lg_cust_hca_hca_affil_prnt_d`.`hca1_cust_addr_ln3` as `hca1_cust_address3`,
`lg_cust_hca_hca_affil_prnt_d`.`hca1_city`,
`lg_cust_hca_hca_affil_prnt_d`.`hca1_state_id` as `hca1_state`,
`lg_cust_hca_hca_affil_prnt_d`.`hca1_post_cd` as `hca1_zip`,
`lg_cust_hca_hca_affil_prnt_d`.`hca1_cust_typ_desc` as `hca1_cust_type_desc`,
`lg_cust_hca_hca_affil_prnt_d`.`hca1_ddd_olet_ss_cust_id` as `hca1_ddd_outlet_ss_cust_id`,
`lg_cust_hca_hca_affil_prnt_d`.`hca1_ddd_olet_az_cust_id` as `hca1_ddd_outlet_az_cust_id`,
`lg_cust_hca_hca_affil_prnt_d`.`hca2_az_cust_id`,
`lg_cust_hca_hca_affil_prnt_d`.`hca2_cust_nm` as `hca2_cust_name`,
`lg_cust_hca_hca_affil_prnt_d`.`hca2_cust_addr_ln1` as `hca2_cust_address1`,
`lg_cust_hca_hca_affil_prnt_d`.`hca2_cust_addr_ln2` as `hca2_cust_address2`,
`lg_cust_hca_hca_affil_prnt_d`.`hca2_cust_addr_ln3` as `hca2_cust_address3`,
`lg_cust_hca_hca_affil_prnt_d`.`hca2_city`,
`lg_cust_hca_hca_affil_prnt_d`.`hca2_state_id` as `hca2_state`,
`lg_cust_hca_hca_affil_prnt_d`.`hca2_post_cd` as `hca2_zip`,
`lg_cust_hca_hca_affil_prnt_d`.`hca2_cust_typ_desc` as `hca2_cust_type_desc`,
`lg_cust_hca_hca_affil_prnt_d`.`hca2_ddd_olet_ss_cust_id` as `hca2_ddd_outlet_ss_cust_id`,
`lg_cust_hca_hca_affil_prnt_d`.`hca2_ddd_olet_az_cust_id` as `hca2_ddd_outlet_az_cust_id`,
`lg_cust_hca_hca_affil_prnt_d`.`rel_type_ind`,
`lg_cust_hca_hca_affil_prnt_d`.`cust_hier_sk`,
`lg_cust_hca_hca_affil_prnt_d`.`rel_cd`
from
`lg_base`.`LG_CUST_HCA_HCA_AFFIL_PRNT_D`
CREATE VIEW `lg_reference.hca_hca_affiliation_child` AS select
`lg_cust_hca_hca_affil_child_d`.`cust_hier_sk`,
`lg_cust_hca_hca_affil_child_d`.`hcos_affil_typ`,
`lg_cust_hca_hca_affil_child_d`.`hcos_affil_grp`,
`lg_cust_hca_hca_affil_child_d`.`hcos_affil_cat`
from
`lg_base`.`LG_CUST_HCA_HCA_AFFIL_CHILD_D`
CREATE VIEW `lg_reference.hca_identifier` AS select 
     `lg_cust_hca_id_d`.`az_cust_id` as `hca_az_cust_id`,




from 
    `lg_base`.`lg_cust_hca_id_d`
CREATE VIEW `lg_reference.hca_profile` AS select 
     `lg_cust_hca_demog_d`.`az_cust_id` as `hca_az_cust_id`,


     `lg_cust_hca_demog_d`.`ph_num` as `hca_ph_num`,
     `lg_cust_hca_demog_d`.`hca_ims_cust_id` as `hca_ims_cust_id`,
     `lg_cust_hca_demog_d`.`fax_num` as `fax_number`,
































from 
    `lg_base`.`lg_cust_hca_demog_d`
CREATE VIEW `lg_reference.hci_block_p12019` AS select
   `lg_block_list_hci`.`ddd` as `outlet_id`,
   `lg_block_list_hci`.`hci_az_cust_id`,
   `lg_block_list_hci`.`ims_org_id`,
   `lg_block_list_hci`.`account_name`,
   `lg_block_list_hci`.`movantik_exc`,
   `lg_block_list_hci`.`seroquel_xr_exc`,
   `lg_block_list_hci`.`crestor_exc`,
   `lg_block_list_hci`.`nexium_exc`,
   `lg_block_list_hci`.`pulmicort_respules_exc`,
   `lg_block_list_hci`.`brilinta_exc`,
   `lg_block_list_hci`.`epanova_exc`,
   `lg_block_list_hci`.`lokelma_exc`,
   `lg_block_list_hci`.`lumoxiti_exc`,
   `lg_block_list_hci`.`lynparza_exc`,
   `lg_block_list_hci`.`lynparza_breast_exc`,
   `lg_block_list_hci`.`lynparza_ovarian_exc`,
   `lg_block_list_hci`.`faslodex_exc`,
   `lg_block_list_hci`.`tagrisso_exc`,
   `lg_block_list_hci`.`imfinzi_exc`,
   `lg_block_list_hci`.`imfinzi_bladder_exc`,
   `lg_block_list_hci`.`imfinzi_la_nsclc_exc`,
   `lg_block_list_hci`.`iressa_exc`,
   `lg_block_list_hci`.`calquence_exc`,
   `lg_block_list_hci`.`calquence_mcl_exc`,
   `lg_block_list_hci`.`zoladex_exc` 
from
   `lg_base`.`lg_block_list_hci`
CREATE VIEW `lg_reference.hcp_demog` AS select
  `wkdem_demog_pro`.`hcp_az_cust_id` , 
  `wkdem_demog_pro`.`imsdr` ,  
  `wkdem_demog_pro`.`hcp_cust_name` , 
  `wkdem_demog_pro`.`fname` as `first_name`, 
  `wkdem_demog_pro`.`lname` as `last_name`,
  `wkdem_demog_pro`.`mname` as `middle_name`,  
  `wkdem_demog_pro`.`phone` as `phone_num`, 
  `wkdem_demog_pro`.`az_adr1` as `address1`, 
  `wkdem_demog_pro`.`az_adr2` as `address2`, 
  `wkdem_demog_pro`.`az_adr3` as `address3`, 
  `wkdem_demog_pro`.`az_stat` as `state`,
  `wkdem_demog_pro`.`state_name` ,  
  `wkdem_demog_pro`.`az_city` as `city`, 
  `wkdem_demog_pro`.`az_zip` as `zip`,  
  `wkdem_demog_pro`.`license` , 
  `wkdem_demog_pro`.`gender` , 
  `wkdem_demog_pro`.`professional_desc` , 
  `wkdem_demog_pro`.`specdesc` as `specialty_desc`, 
  `wkdem_demog_pro`.`special` as `special_code`, 
  `wkdem_demog_pro`.`suffix` , 
  `wkdem_demog_pro`.`status` , 
  `wkdem_demog_pro`.`opt_out_flag` , 
  `wkdem_demog_pro`.`opt_out_date` , 
  `wkdem_demog_pro`.`role` , 
  `wkdem_demog_pro`.`customer_type` , 
  `wkdem_demog_pro`.`customer_subtype` , 
  `wkdem_demog_pro`.`customer_type_desc` , 
  `wkdem_demog_pro`.`customer_subtype_desc` , 
  `wkdem_demog_pro`.`credential_ind` ,  
  `wkdem_demog_pro`.`marketing_email` , 
  `wkdem_demog_pro`.`me` , 
  `wkdem_demog_pro`.`npi` , 
  `wkdem_demog_pro`.`hce` 
from `lg_base`.`wkdem_demog_pro`
CREATE VIEW `lg_reference.hcp_hca_affiliation` AS select
`lg_cust_hca_affil_prnt_d`.`imsdr`,
`lg_cust_hca_affil_prnt_d`.`cust_nm` as `hcp_cust_name`,
`lg_cust_hca_affil_prnt_d`.`hca_az_cust_id`,
`lg_cust_hca_affil_prnt_d`.`hca_cust_nm` as `hca_cust_name`,
`lg_cust_hca_affil_prnt_d`.`cust_addr_ln1` as `hca_address1`,
`lg_cust_hca_affil_prnt_d`.`cust_addr_ln2` as `hca_address2`,
`lg_cust_hca_affil_prnt_d`.`cust_addr_ln3` as `hca_address3`,
`lg_cust_hca_affil_prnt_d`.`city` as `hca_city`,
`lg_cust_hca_affil_prnt_d`.`state_cd` as `hca_state`,
`lg_cust_hca_affil_prnt_d`.`post_cd` as `hca_zip`,
`lg_cust_hca_affil_prnt_d`.`cust_typ_desc` as `hca_cust_type_desc`,
`lg_cust_hca_affil_prnt_d`.`hca_ddd_olet_ss_cust_id` as `hca_ddd_outlet_ss_cust_id`,
`lg_cust_hca_affil_prnt_d`.`hcp_az_cust_id`,
`lg_cust_hca_affil_prnt_d`.`hca_ddd_olet_az_cust_id` as `hca_ddd_outlet_az_cust_id`,
`lg_cust_hca_affil_prnt_d`.`rel_type_ind`,
`lg_cust_hca_affil_prnt_d`.`cust_hier_sk`,
`lg_cust_hca_affil_prnt_d`.`rel_cd`
from
`lg_base`.`LG_CUST_HCA_AFFIL_PRNT_D`
CREATE VIEW `lg_reference.hcp_hca_affiliation_child` AS select 
`lg_cust_hca_affil_child_d`.`cust_hier_sk`,
`lg_cust_hca_affil_child_d`.`hcos_affil_typ`,
`lg_cust_hca_affil_child_d`.`hcos_affil_grp`
from 
`lg_base`.`lg_cust_hca_affil_child_d`
CREATE VIEW `lg_reference.hcp_identifier` AS select
     `lg_cust_hcp_id_d`.`hcp_az_cust_id` ,



from
    `lg_base`.`lg_cust_hcp_id_d`
CREATE VIEW `lg_reference.hcp_specialty` AS select 
`lg_cust_hcp_spec_d`.`hcp_az_cust_id` ,
`lg_cust_hcp_spec_d`.`spec_cd` as `specialty_code`,
`lg_cust_hcp_spec_d`.`spec_desc` as `specialty_desc`
from 
    `lg_base`.`lg_cust_hcp_spec_d`
CREATE VIEW `lg_reference.hcp_specialty_block_p12019` AS select
   `lg_hcp_specialty_block_p12019`.`imsdr`,
   `lg_hcp_specialty_block_p12019`.`hcp_az_cust_id`,
   `lg_hcp_specialty_block_p12019`.`license`,
   `lg_hcp_specialty_block_p12019`.`fname` as `first_name`,
   `lg_hcp_specialty_block_p12019`.`mname` as `middle_name`,
   `lg_hcp_specialty_block_p12019`.`lname` as `last_name`,
   `lg_hcp_specialty_block_p12019`.`ama_mddo`,
   `lg_hcp_specialty_block_p12019`.`special` as `special_code`,
   `lg_hcp_specialty_block_p12019`.`specdesc` as `specialty_desc`,
   `lg_hcp_specialty_block_p12019`.`address1`,
   `lg_hcp_specialty_block_p12019`.`address2`,
   `lg_hcp_specialty_block_p12019`.`address3`,
   `lg_hcp_specialty_block_p12019`.`city`,
   `lg_hcp_specialty_block_p12019`.`state`,
   `lg_hcp_specialty_block_p12019`.`zip`,
   `lg_hcp_specialty_block_p12019`.`brilinta_exc`,
   `lg_hcp_specialty_block_p12019`.`brilinta_block`,
   `lg_hcp_specialty_block_p12019`.`fasenra_exc`,
   `lg_hcp_specialty_block_p12019`.`fasenra_block`,
   `lg_hcp_specialty_block_p12019`.`calquence_exc`,
   `lg_hcp_specialty_block_p12019`.`calquence_block`,
   `lg_hcp_specialty_block_p12019`.`calquence_mcl_exc`,
   `lg_hcp_specialty_block_p12019`.`calquence_mcl_block`,
   `lg_hcp_specialty_block_p12019`.`lumoxiti_exc`,
   `lg_hcp_specialty_block_p12019`.`lumoxiti_block`,
   `lg_hcp_specialty_block_p12019`.`faslodex_exc`,
   `lg_hcp_specialty_block_p12019`.`faslodex_block`,
   `lg_hcp_specialty_block_p12019`.`bydureon_exc`,
   `lg_hcp_specialty_block_p12019`.`bydureon_block`,
   `lg_hcp_specialty_block_p12019`.`bydureon_bcise_exc`,
   `lg_hcp_specialty_block_p12019`.`bydureon_bcise_block`,
   `lg_hcp_specialty_block_p12019`.`byetta_exc`,
   `lg_hcp_specialty_block_p12019`.`byetta_block`,
   `lg_hcp_specialty_block_p12019`.`farxiga_exc`,
   `lg_hcp_specialty_block_p12019`.`farxiga_block`,
   `lg_hcp_specialty_block_p12019`.`kombiglyze_xr_exc`,
   `lg_hcp_specialty_block_p12019`.`kombiglyze_xr_block`,
   `lg_hcp_specialty_block_p12019`.`onglyza_exc`,
   `lg_hcp_specialty_block_p12019`.`onglyza_block`,
   `lg_hcp_specialty_block_p12019`.`qtern_exc`,
   `lg_hcp_specialty_block_p12019`.`qtern_block`,
   `lg_hcp_specialty_block_p12019`.`symlin_exc`,
   `lg_hcp_specialty_block_p12019`.`symlin_block`,
   `lg_hcp_specialty_block_p12019`.`xigduo_xr_exc`,
   `lg_hcp_specialty_block_p12019`.`xigduo_xr_block`,
   `lg_hcp_specialty_block_p12019`.`imfinzi_la_nsclc_exc`,
   `lg_hcp_specialty_block_p12019`.`imfinzi_la_nsclc_block`,
   `lg_hcp_specialty_block_p12019`.`imfinzi_bladder_exc`,
   `lg_hcp_specialty_block_p12019`.`imfinzi_bladder_block`,
   `lg_hcp_specialty_block_p12019`.`lokelma_exc`,
   `lg_hcp_specialty_block_p12019`.`lokelma_block`,
   `lg_hcp_specialty_block_p12019`.`movantik_exc`,
   `lg_hcp_specialty_block_p12019`.`movantik_block`,
   `lg_hcp_specialty_block_p12019`.`atacand_hct_exc`,
   `lg_hcp_specialty_block_p12019`.`atacand_hct_block`,
   `lg_hcp_specialty_block_p12019`.`lynparza_exc`,
   `lg_hcp_specialty_block_p12019`.`lynparza_block`,
   `lg_hcp_specialty_block_p12019`.`lynparza_breast_exc`,
   `lg_hcp_specialty_block_p12019`.`lynparza_breast_block`,
   `lg_hcp_specialty_block_p12019`.`lynparza_ovarian_exc`,
   `lg_hcp_specialty_block_p12019`.`lynparza_ovarian_block`,
   `lg_hcp_specialty_block_p12019`.`tagrisso_exc`,
   `lg_hcp_specialty_block_p12019`.`tagrisso_block`,
   `lg_hcp_specialty_block_p12019`.`symbicort_exc`,
   `lg_hcp_specialty_block_p12019`.`symbicort_block`,
   `lg_hcp_specialty_block_p12019`.`nexium_exc`,
   `lg_hcp_specialty_block_p12019`.`nexium_block`,
   `lg_hcp_specialty_block_p12019`.`seroquel_xr_exc`,
   `lg_hcp_specialty_block_p12019`.`seroquel_xr_block`,
   `lg_hcp_specialty_block_p12019`.`pulmicort_respules_exc`,
   `lg_hcp_specialty_block_p12019`.`pulmicort_respules_block`,
   `lg_hcp_specialty_block_p12019`.`arimidex_exc`,
   `lg_hcp_specialty_block_p12019`.`arimidex_block`,
   `lg_hcp_specialty_block_p12019`.`zoladex_exc`,
   `lg_hcp_specialty_block_p12019`.`zoladex_block`,
   `lg_hcp_specialty_block_p12019`.`iressa_exc`,
   `lg_hcp_specialty_block_p12019`.`iressa_block`,
   `lg_hcp_specialty_block_p12019`.`atacand_exc`,
   `lg_hcp_specialty_block_p12019`.`atacand_block`,
   `lg_hcp_specialty_block_p12019`.`bevespi_aerosphere_exc`,
   `lg_hcp_specialty_block_p12019`.`bevespi_aerosphere_block`,
   `lg_hcp_specialty_block_p12019`.`tudorza_pressair_exc`,
   `lg_hcp_specialty_block_p12019`.`tudorza_pressair_block`,
   `lg_hcp_specialty_block_p12019`.`flumist_exc`,
   `lg_hcp_specialty_block_p12019`.`flumist_block`,
   `lg_hcp_specialty_block_p12019`.`synagis_exc`,
   `lg_hcp_specialty_block_p12019`.`synagis_block`,
   `lg_hcp_specialty_block_p12019`.`imfinzi_exc`,
   `lg_hcp_specialty_block_p12019`.`imfinzi_block`,
   `lg_hcp_specialty_block_p12019`.`daliresp_exc`,
   `lg_hcp_specialty_block_p12019`.`daliresp_block`,
   `lg_hcp_specialty_block_p12019`.`pulmicort_flexhaler_exc`,
   `lg_hcp_specialty_block_p12019`.`pulmicort_flexhaler_block`,
   `lg_hcp_specialty_block_p12019`.`crestor_exc`,
   `lg_hcp_specialty_block_p12019`.`crestor_block`,
   `lg_hcp_specialty_block_p12019`.`epanova_exc`,
   `lg_hcp_specialty_block_p12019`.`epanova_block`,
   `lg_hcp_specialty_block_p12019`.`sna_bydureon_exc`,
   `lg_hcp_specialty_block_p12019`.`sna_movantik_exc`,
   `lg_hcp_specialty_block_p12019`.`sna_onglyza_exc`,
   `lg_hcp_specialty_block_p12019`.`sna_qtern_exc`,
   `lg_hcp_specialty_block_p12019`.`sna_seroquel_xr_exc`,
   `lg_hcp_specialty_block_p12019`.`sna_symbicort_exc`,
   `lg_hcp_specialty_block_p12019`.`sna_symlin_exc`,
   `lg_hcp_specialty_block_p12019`.`sna_xigduo_xr_exc`,
   `lg_hcp_specialty_block_p12019`.`sna_arimidex_exc`,
   `lg_hcp_specialty_block_p12019`.`sna_bydureon_bcise_exc`,
   `lg_hcp_specialty_block_p12019`.`sna_byetta_exc`,
   `lg_hcp_specialty_block_p12019`.`sna_farxiga_exc`,
   `lg_hcp_specialty_block_p12019`.`sna_faslodex_exc`,
   `lg_hcp_specialty_block_p12019`.`sna_kombiglyze_xr_exc`,
   `lg_hcp_specialty_block_p12019`.`legal_comp_exc`,
   `lg_hcp_specialty_block_p12019`.`dns_exc`,
   `lg_hcp_specialty_block_p12019`.`aspen_exc`,
   cast(from_unixtime(unix_timestamp(substr(`lg_hcp_specialty_block_p12019`.`last_update_date`, 1, 11), 'dd-MMM-yyyy'), 'yyyy-MM-dd') as date) as `last_update_date` 
from
   `lg_base`.`lg_hcp_specialty_block_p12019`
CREATE VIEW `lg_reference.hcp_title` AS select 
     `lg_cust_hcp_title_d`.`hcp_az_cust_id` ,

from 
    `lg_base`.`lg_cust_hcp_title_d`
CREATE VIEW `lg_reference.outlet_demog` AS select
`lg_ddd_hcos_olet_f`.`ddd_outlet_number` as `outlet_id`,
`lg_ddd_hcos_olet_f`.`subcategory_code` as `sub_catgeory`,
`lg_ddd_hcos_olet_f`.`ddd_outlet_name`
`lg_ddd_hcos_olet_f`.`ddd_outlet_address` as `outlet_address`,
`lg_ddd_hcos_olet_f`.`city` as `outlet_city`,
`lg_ddd_hcos_olet_f`.`state` as `outlet_state`,
`lg_ddd_hcos_olet_f`.`activity_date` as `activity_date`,
`lg_ddd_hcos_olet_f`.`dea_number` as `dea_number`,
`lg_ddd_hcos_olet_f`.`special_outlet_number` as `special_outlet_id`,
`lg_ddd_hcos_olet_f`.`filler` as `filler`,
`lg_ddd_hcos_olet_f`.`outlet_status` as `outlet_status`,
`lg_ddd_hcos_olet_f`.`hcos_number`
`lg_ddd_hcos_olet_f`.`hcos_az_id` as `hcos_az_id`,
`lg_ddd_hcos_olet_f`.`olet_az_id` as `outlet_cust_id`
from
`lg_base`.`lg_ddd_hcos_olet_f`
CREATE VIEW `lg_reference.payer_cab_apr2019` AS select
`payer_cab_apr2019`.`plan_id`,
`payer_cab_apr2019`.`az_account_name` ,
`payer_cab_apr2019`.`az_account_bridge_id` ,
`payer_cab_apr2019`.`az_pbm_name` ,
`payer_cab_apr2019`.`az_pbm_bridge_id` ,
`payer_cab_apr2019`.`az_payer_name` ,
`payer_cab_apr2019`.`az_payer_bridge_id` ,
`payer_cab_apr2019`.`az_pay_type` ,
`payer_cab_apr2019`.`az_pay_type_id` ,
`payer_cab_apr2019`.`brand_nm` as `prod_name`,
`payer_cab_apr2019`.`concat` as `formulary`,
`payer_cab_apr2019`.`formulary_position` ,
`payer_cab_apr2019`.`broad_market_trx`,
`payer_cab_apr2019`.`az_payer_hca_cust_sk` ,
`payer_cab_apr2019`.`az_pbm_hca_cust_sk` ,
`payer_cab_apr2019`.`az_payer_corporate_parent_name` ,
`payer_cab_apr2019`.`az_payer_corporate_patent_id` ,
`payer_cab_apr2019`.`hq_state` ,
`payer_cab_apr2019`.`imspayer_id` ,
`payer_cab_apr2019`.`imspbm_id` ,
`payer_cab_apr2019`.`ims_plan_id` ,
`payer_cab_apr2019`.`ims_plan_name` ,
`payer_cab_apr2019`.`ims_plan_bridge_id` ,
`payer_cab_apr2019`.`ims_plan_hcm_id` ,
`payer_cab_apr2019`.`plan_hca_cust_sk` ,
`payer_cab_apr2019`.`plan_lives` ,
`payer_cab_apr2019`.`ims_modelvartype` ,
`payer_cab_apr2019`.`ims_modelvartype_id` ,
`payer_cab_apr2019`.`account_hca_cust_sk` ,
`payer_cab_apr2019`.`ims_payer_name` ,
`payer_cab_apr2019`.`ims_payer_id` ,
`payer_cab_apr2019`.`ims_pbm_name` ,
`payer_cab_apr2019`.`ims_pbm_id`
from
`lg_base`.`payer_cab_apr2019`
CREATE VIEW `lg_reference.payer_cab_aug2019` AS select    `plan_id`,   `az_account_name` ,    `az_account_bridge_id` ,   `az_pbm_name` ,    `az_pbm_bridge_id` ,   `az_payer_name` ,    `az_payer_bridge_id` ,    `az_pay_type` ,    `az_pay_type_id` ,    `brand_nm` as `prod_name`,   `concat` as `formulary`,   `formulary_position` ,   `broad_market_trx`,   `az_payer_hca_cust_sk` ,   `az_pbm_hca_cust_sk` ,   `az_payer_corporate_parent_name` ,    `az_payer_corporate_patent_id` ,    `hq_state` ,    `imspayer_id` ,    `imspbm_id` ,    `ims_plan_id` ,    `ims_plan_name` ,    `ims_plan_bridge_id` ,    `ims_plan_hcm_id` ,    `plan_hca_cust_sk` ,    `plan_lives` ,    `ims_modelvartype` ,    `ims_modelvartype_id` ,      `account_hca_cust_sk` ,    `ims_payer_name` ,    `ims_payer_id` ,    `ims_pbm_name` ,    `ims_pbm_id`  from      `lg_base`.`payer_cab_aug2019`
CREATE VIEW `lg_reference.payer_cab_feb2019` AS select
`payer_cab_feb2019`.`plan_id`,
`payer_cab_feb2019`.`az_account_name` ,
`payer_cab_feb2019`.`az_account_bridge_id` ,
`payer_cab_feb2019`.`az_pbm_name` ,
`payer_cab_feb2019`.`az_pbm_bridge_id` ,
`payer_cab_feb2019`.`az_payer_name` ,
`payer_cab_feb2019`.`az_payer_bridge_id` ,
`payer_cab_feb2019`.`az_pay_type` ,
`payer_cab_feb2019`.`az_pay_type_id` ,
`payer_cab_feb2019`.`brand_nm` as `prod_name`,
`payer_cab_feb2019`.`concat` as `formulary`,
`payer_cab_feb2019`.`formulary_position` ,
`payer_cab_feb2019`.`broad_market_trx`,
`payer_cab_feb2019`.`az_payer_hca_cust_sk` ,
`payer_cab_feb2019`.`az_pbm_hca_cust_sk` ,
`payer_cab_feb2019`.`az_payer_corporate_parent_name` ,
`payer_cab_feb2019`.`az_payer_corporate_patent_id` ,
`payer_cab_feb2019`.`hq_state` ,
`payer_cab_feb2019`.`imspayer_id` ,
`payer_cab_feb2019`.`imspbm_id` ,
`payer_cab_feb2019`.`ims_plan_id` ,
`payer_cab_feb2019`.`ims_plan_name` ,
`payer_cab_feb2019`.`ims_plan_bridge_id` ,
`payer_cab_feb2019`.`ims_plan_hcm_id` ,
`payer_cab_feb2019`.`plan_hca_cust_sk` ,
`payer_cab_feb2019`.`plan_lives` ,
`payer_cab_feb2019`.`ims_modelvartype` ,
`payer_cab_feb2019`.`ims_modelvartype_id` ,
`payer_cab_feb2019`.`account_hca_cust_sk` ,
`payer_cab_feb2019`.`ims_payer_name` ,
`payer_cab_feb2019`.`ims_payer_id` ,
`payer_cab_feb2019`.`ims_pbm_name` ,
`payer_cab_feb2019`.`ims_pbm_id`
from
`lg_base`.`payer_cab_feb2019`
CREATE VIEW `lg_reference.payer_cab_jul2019` AS select
`payer_cab_jul2019`.`plan_id`,
`payer_cab_jul2019`.`az_account_name` ,
`payer_cab_jul2019`.`az_account_bridge_id` ,
`payer_cab_jul2019`.`az_pbm_name` ,
`payer_cab_jul2019`.`az_pbm_bridge_id` ,
`payer_cab_jul2019`.`az_payer_name` ,
`payer_cab_jul2019`.`az_payer_bridge_id` ,
`payer_cab_jul2019`.`az_pay_type` ,
`payer_cab_jul2019`.`az_pay_type_id` ,
`payer_cab_jul2019`.`brand_nm` as `prod_name`,
`payer_cab_jul2019`.`concat` as `formulary`,
`payer_cab_jul2019`.`formulary_position` ,
`payer_cab_jul2019`.`broad_market_trx`,
`payer_cab_jul2019`.`az_payer_hca_cust_sk` ,
`payer_cab_jul2019`.`az_pbm_hca_cust_sk` ,
`payer_cab_jul2019`.`az_payer_corporate_parent_name` ,
`payer_cab_jul2019`.`az_payer_corporate_patent_id` ,
`payer_cab_jul2019`.`hq_state` ,
`payer_cab_jul2019`.`imspayer_id` ,
`payer_cab_jul2019`.`imspbm_id` ,
`payer_cab_jul2019`.`ims_plan_id` ,
`payer_cab_jul2019`.`ims_plan_name` ,
`payer_cab_jul2019`.`ims_plan_bridge_id` ,
`payer_cab_jul2019`.`ims_plan_hcm_id` ,
`payer_cab_jul2019`.`plan_hca_cust_sk` ,
`payer_cab_jul2019`.`plan_lives` ,
`payer_cab_jul2019`.`ims_modelvartype` ,
`payer_cab_jul2019`.`ims_modelvartype_id` ,
`payer_cab_jul2019`.`account_hca_cust_sk` ,
`payer_cab_jul2019`.`ims_payer_name` ,
`payer_cab_jul2019`.`ims_payer_id` ,
`payer_cab_jul2019`.`ims_pbm_name` ,
`payer_cab_jul2019`.`ims_pbm_id`
from
`lg_base`.`payer_cab_jul2019`
CREATE VIEW `lg_reference.payer_cab_jun2019` AS select
`payer_cab_jun2019`.`plan_id`,
`payer_cab_jun2019`.`az_account_name` ,
`payer_cab_jun2019`.`az_account_bridge_id` ,
`payer_cab_jun2019`.`az_pbm_name` ,
`payer_cab_jun2019`.`az_pbm_bridge_id` ,
`payer_cab_jun2019`.`az_payer_name` ,
`payer_cab_jun2019`.`az_payer_bridge_id` ,
`payer_cab_jun2019`.`az_pay_type` ,
`payer_cab_jun2019`.`az_pay_type_id` ,
`payer_cab_jun2019`.`brand_nm` as `prod_name`,
`payer_cab_jun2019`.`concat` as `formulary`,
`payer_cab_jun2019`.`formulary_position` ,
`payer_cab_jun2019`.`broad_market_trx`,
`payer_cab_jun2019`.`az_payer_hca_cust_sk` ,
`payer_cab_jun2019`.`az_pbm_hca_cust_sk` ,
`payer_cab_jun2019`.`az_payer_corporate_parent_name` ,
`payer_cab_jun2019`.`az_payer_corporate_patent_id` ,
`payer_cab_jun2019`.`hq_state` ,
`payer_cab_jun2019`.`imspayer_id` ,
`payer_cab_jun2019`.`imspbm_id` ,
`payer_cab_jun2019`.`ims_plan_id` ,
`payer_cab_jun2019`.`ims_plan_name` ,
`payer_cab_jun2019`.`ims_plan_bridge_id` ,
`payer_cab_jun2019`.`ims_plan_hcm_id` ,
`payer_cab_jun2019`.`plan_hca_cust_sk` ,
`payer_cab_jun2019`.`plan_lives` ,
`payer_cab_jun2019`.`ims_modelvartype` ,
`payer_cab_jun2019`.`ims_modelvartype_id` ,
`payer_cab_jun2019`.`account_hca_cust_sk` ,
`payer_cab_jun2019`.`ims_payer_name` ,
`payer_cab_jun2019`.`ims_payer_id` ,
`payer_cab_jun2019`.`ims_pbm_name` ,
`payer_cab_jun2019`.`ims_pbm_id`
from
`lg_base`.`payer_cab_jun2019`
CREATE VIEW `lg_reference.payer_cab_mar2019` AS select
`payer_cab_mar2019`.`plan_id`,
`payer_cab_mar2019`.`az_account_name` ,
`payer_cab_mar2019`.`az_account_bridge_id` ,
`payer_cab_mar2019`.`az_pbm_name` ,
`payer_cab_mar2019`.`az_pbm_bridge_id` ,
`payer_cab_mar2019`.`az_payer_name` ,
`payer_cab_mar2019`.`az_payer_bridge_id` ,
`payer_cab_mar2019`.`az_pay_type` ,
`payer_cab_mar2019`.`az_pay_type_id` ,
`payer_cab_mar2019`.`brand_nm` as `prod_name`,
`payer_cab_mar2019`.`concat` as `formulary`,
`payer_cab_mar2019`.`formulary_position` ,
`payer_cab_mar2019`.`broad_market_trx`,
`payer_cab_mar2019`.`az_payer_hca_cust_sk` ,
`payer_cab_mar2019`.`az_pbm_hca_cust_sk` ,
`payer_cab_mar2019`.`az_payer_corporate_parent_name` ,
`payer_cab_mar2019`.`az_payer_corporate_patent_id` ,
`payer_cab_mar2019`.`hq_state` ,
`payer_cab_mar2019`.`imspayer_id` ,
`payer_cab_mar2019`.`imspbm_id` ,
`payer_cab_mar2019`.`ims_plan_id` ,
`payer_cab_mar2019`.`ims_plan_name` ,
`payer_cab_mar2019`.`ims_plan_bridge_id` ,
`payer_cab_mar2019`.`ims_plan_hcm_id` ,
`payer_cab_mar2019`.`plan_hca_cust_sk` ,
`payer_cab_mar2019`.`plan_lives` ,
`payer_cab_mar2019`.`ims_modelvartype` ,
`payer_cab_mar2019`.`ims_modelvartype_id` ,
`payer_cab_mar2019`.`account_hca_cust_sk` ,
`payer_cab_mar2019`.`ims_payer_name` ,
`payer_cab_mar2019`.`ims_payer_id` ,
`payer_cab_mar2019`.`ims_pbm_name` ,
`payer_cab_mar2019`.`ims_pbm_id`
from
`lg_base`.`payer_cab_mar2019`
CREATE VIEW `lg_reference.payer_cab_may2019` AS select
`payer_cab_may2019`.`plan_id`,
`payer_cab_may2019`.`az_account_name` ,
`payer_cab_may2019`.`az_account_bridge_id` ,
`payer_cab_may2019`.`az_pbm_name` ,
`payer_cab_may2019`.`az_pbm_bridge_id` ,
`payer_cab_may2019`.`az_payer_name` ,
`payer_cab_may2019`.`az_payer_bridge_id` ,
`payer_cab_may2019`.`az_pay_type` ,
`payer_cab_may2019`.`az_pay_type_id` ,
`payer_cab_may2019`.`brand_nm` as `prod_name`,
`payer_cab_may2019`.`concat` as `formulary`,
`payer_cab_may2019`.`formulary_position` ,
`payer_cab_may2019`.`broad_market_trx`,
`payer_cab_may2019`.`az_payer_hca_cust_sk` ,
`payer_cab_may2019`.`az_pbm_hca_cust_sk` ,
`payer_cab_may2019`.`az_payer_corporate_parent_name` ,
`payer_cab_may2019`.`az_payer_corporate_patent_id` ,
`payer_cab_may2019`.`hq_state` ,
`payer_cab_may2019`.`imspayer_id` ,
`payer_cab_may2019`.`imspbm_id` ,
`payer_cab_may2019`.`ims_plan_id` ,
`payer_cab_may2019`.`ims_plan_name` ,
`payer_cab_may2019`.`ims_plan_bridge_id` ,
`payer_cab_may2019`.`ims_plan_hcm_id` ,
`payer_cab_may2019`.`plan_hca_cust_sk` ,
`payer_cab_may2019`.`plan_lives` ,
`payer_cab_may2019`.`ims_modelvartype` ,
`payer_cab_may2019`.`ims_modelvartype_id` ,
`payer_cab_may2019`.`account_hca_cust_sk` ,
`payer_cab_may2019`.`ims_payer_name` ,
`payer_cab_may2019`.`ims_payer_id` ,
`payer_cab_may2019`.`ims_pbm_name` ,
`payer_cab_may2019`.`ims_pbm_id`
from
`lg_base`.`payer_cab_may2019`
CREATE VIEW `lg_reference.payer_cab_oct2019` AS select    `plan_id`,   `az_account_name` ,    `az_account_bridge_id` ,   `az_pbm_name` ,    `az_pbm_bridge_id` ,   `az_payer_name` ,    `az_payer_bridge_id` ,    `az_pay_type` ,    `az_pay_type_id` ,    `brand_nm` as `prod_name`,   `concat` as `formulary`,   `formulary_position` ,   `broad_market_trx`,   `az_payer_hca_cust_sk` ,   `az_pbm_hca_cust_sk` ,   `az_payer_corporate_parent_name` ,    `az_payer_corporate_patent_id` ,    `hq_state` ,    `imspayer_id` ,    `imspbm_id` ,    `ims_plan_id` ,    `ims_plan_name` ,    `ims_plan_bridge_id` ,    `ims_plan_hcm_id` ,    `plan_hca_cust_sk` ,    `plan_lives` ,    `ims_modelvartype` ,    `ims_modelvartype_id` ,      `account_hca_cust_sk` ,    `ims_payer_name` ,    `ims_payer_id` ,    `ims_pbm_name` ,    `ims_pbm_id`  from      `lg_base`.`payer_cab_oct2019`
CREATE VIEW `lg_reference.payer_cab_sep2019` AS select    `plan_id`,   `az_account_name` ,    `az_account_bridge_id` ,   `az_pbm_name` ,    `az_pbm_bridge_id` ,   `az_payer_name` ,    `az_payer_bridge_id` ,    `az_pay_type` ,    `az_pay_type_id` ,    `brand_nm` as `prod_name`,   `concat` as `formulary`,   `formulary_position` ,   `broad_market_trx`,   `az_payer_hca_cust_sk` ,   `az_pbm_hca_cust_sk` ,   `az_payer_corporate_parent_name` ,    `az_payer_corporate_patent_id` ,    `hq_state` ,    `imspayer_id` ,    `imspbm_id` ,    `ims_plan_id` ,    `ims_plan_name` ,    `ims_plan_bridge_id` ,    `ims_plan_hcm_id` ,    `plan_hca_cust_sk` ,    `plan_lives` ,    `ims_modelvartype` ,    `ims_modelvartype_id` ,      `account_hca_cust_sk` ,    `ims_payer_name` ,    `ims_payer_id` ,    `ims_pbm_name` ,    `ims_pbm_id`  from      `lg_base`.`payer_cab_sep2019`
CREATE VIEW `lg_reference.prod_ref` AS select 
`lg_product_ref_t`.`mkt_nm` as `market_name`,
`lg_product_ref_t`.`az_mkt_id`,
`lg_product_ref_t`.`submarket_nm` as `submarket_name`,
`lg_product_ref_t`.`az_sub_mkt_id`,
`lg_product_ref_t`.`prod_nm` as `prod_name`,
`lg_product_ref_t`.`prod_level`,
`lg_product_ref_t`.`prod_group_nm` as `prod_group_name`,
`lg_product_ref_t`.`brand` as `prod_id`,
`lg_product_ref_t`.`az_prod_group_id`,
`lg_product_ref_t`.`data_month`,
`lg_product_ref_t`.`ims_number`,
`lg_product_ref_t`.`ims_mkt_num`,
`lg_product_ref_t`.`ims_prod_code4`,
`lg_product_ref_t`.`source_system`
from `lg_base`.`lg_product_ref_t`
CREATE VIEW `lg_reference.profession_type` AS select 
`lg_cust_hcp_prof_typ_d`.`hcp_profsnl_id` as `hcp_professional_id`,
`lg_cust_hcp_prof_typ_d`.`hcp_profsnl_desc` as `hcp_professional_desc`
from 
`lg_base`.`LG_CUST_HCP_PROF_TYP_D`
CREATE VIEW `lg_reference.salesforce_roster` AS select
`lg_persn_d`.`bus_cntr_nm` as `bc_name`,
`lg_persn_d`.`regn_nm` as `region_name`,
`lg_persn_d`.`dist_nm` as `district_name`,
`lg_persn_d`.`fnct_terr_nm` as `terr_name`,
`lg_persn_d`.`az_fnct_terr_id` as `terr_id`,
`lg_persn_d`.`team_nm` as `team`,
`lg_persn_d`.`fnct_terr_role` as `role`,
`lg_persn_d`.`frst_nm`
`lg_persn_d`.`mid_nm` as `middle_name`,
`lg_persn_d`.`lst_nm` as `last_name`,
`lg_persn_d`.`nick_nm`
`lg_persn_d`.`home_addr1` as `home_address1`,
`lg_persn_d`.`home_addr2` as `home_address2`,
`lg_persn_d`.`home_city` as `home_city`,
`lg_persn_d`.`home_state` as `home_state`,
`lg_persn_d`.`home_zip_cd`
`lg_persn_d`.`octel_node` as `octel_node`,
`lg_persn_d`.`email` as `email`,
`lg_persn_d`.`home_ph`
`lg_persn_d`.`sal_roll_num` as `sal_roll_num`,
to_date(`lg_persn_d`.`svc_dt`) as `svc_date`,
to_date(`lg_persn_d`.`bth_dt`) as `bth_date`,
`lg_persn_d`.`persn_stat_cd` as `status_code`,
`lg_persn_d`.`az_persn_id`
`lg_persn_d`.`persn_class`
from
`lg_base`.`lg_persn_d`
CREATE VIEW `lg_reference.speciality_type` AS select
`lg_cust_hcp_spec_typ_d`.`spec_cd` as `specialty_code`,
`lg_cust_hcp_spec_typ_d`.`spec_desc` as `specialty_desc`
from 
`lg_base`.`LG_CUST_HCP_SPEC_TYP_D`
CREATE VIEW `lg_reference.specialty_block_p12019` AS select
   `lg_block_list_hcp`.`imsdr`,
   `lg_block_list_hcp`.`hcp_az_cust_id`,
   `lg_block_list_hcp`.`license`,
   `lg_block_list_hcp`.`fname` as `first_name`,
   `lg_block_list_hcp`.`mname` as `middle_name`,
   `lg_block_list_hcp`.`lname` as `last_name`,
   `lg_block_list_hcp`.`ama_mddo`,
   `lg_block_list_hcp`.`special` as `special_code`,
   `lg_block_list_hcp`.`specdesc` as `specialty_desc`,
   `lg_block_list_hcp`.`address1`,
   `lg_block_list_hcp`.`address2`,
   `lg_block_list_hcp`.`address3`,
   `lg_block_list_hcp`.`city`,
   `lg_block_list_hcp`.`state`,
   `lg_block_list_hcp`.`zip`,
   `lg_block_list_hcp`.`brilinta_exc`,
   `lg_block_list_hcp`.`brilinta_block`,
   `lg_block_list_hcp`.`fasenra_exc`,
   `lg_block_list_hcp`.`fasenra_block`,
   `lg_block_list_hcp`.`calquence_exc`,
   `lg_block_list_hcp`.`calquence_block`,
   `lg_block_list_hcp`.`calquence_mcl_exc`,
   `lg_block_list_hcp`.`calquence_mcl_block`,
   `lg_block_list_hcp`.`lumoxiti_exc`,
   `lg_block_list_hcp`.`lumoxiti_block`,
   `lg_block_list_hcp`.`faslodex_exc`,
   `lg_block_list_hcp`.`faslodex_block`,
   `lg_block_list_hcp`.`bydureon_exc`,
   `lg_block_list_hcp`.`bydureon_block`,
   `lg_block_list_hcp`.`bydureon_bcise_exc`,
   `lg_block_list_hcp`.`bydureon_bcise_block`,
   `lg_block_list_hcp`.`byetta_exc`,
   `lg_block_list_hcp`.`byetta_block`,
   `lg_block_list_hcp`.`farxiga_exc`,
   `lg_block_list_hcp`.`farxiga_block`,
   `lg_block_list_hcp`.`kombiglyze_xr_exc`,
   `lg_block_list_hcp`.`kombiglyze_xr_block`,
   `lg_block_list_hcp`.`onglyza_exc`,
   `lg_block_list_hcp`.`onglyza_block`,
   `lg_block_list_hcp`.`qtern_exc`,
   `lg_block_list_hcp`.`qtern_block`,
   `lg_block_list_hcp`.`symlin_exc`,
   `lg_block_list_hcp`.`symlin_block`,
   `lg_block_list_hcp`.`xigduo_xr_exc`,
   `lg_block_list_hcp`.`xigduo_xr_block`,
   `lg_block_list_hcp`.`imfinzi_la_nsclc_exc`,
   `lg_block_list_hcp`.`imfinzi_la_nsclc_block`,
   `lg_block_list_hcp`.`imfinzi_bladder_exc`,
   `lg_block_list_hcp`.`imfinzi_bladder_block`,
   `lg_block_list_hcp`.`lokelma_exc`,
   `lg_block_list_hcp`.`lokelma_block`,
   `lg_block_list_hcp`.`movantik_exc`,
   `lg_block_list_hcp`.`movantik_block`,
   `lg_block_list_hcp`.`atacand_hct_exc`,
   `lg_block_list_hcp`.`atacand_hct_block`,
   `lg_block_list_hcp`.`lynparza_exc`,
   `lg_block_list_hcp`.`lynparza_block`,
   `lg_block_list_hcp`.`lynparza_breast_exc`,
   `lg_block_list_hcp`.`lynparza_breast_block`,
   `lg_block_list_hcp`.`lynparza_ovarian_exc`,
   `lg_block_list_hcp`.`lynparza_ovarian_block`,
   `lg_block_list_hcp`.`tagrisso_exc`,
   `lg_block_list_hcp`.`tagrisso_block`,
   `lg_block_list_hcp`.`symbicort_exc`,
   `lg_block_list_hcp`.`symbicort_block`,
   `lg_block_list_hcp`.`nexium_exc`,
   `lg_block_list_hcp`.`nexium_block`,
   `lg_block_list_hcp`.`seroquel_xr_exc`,
   `lg_block_list_hcp`.`seroquel_xr_block`,
   `lg_block_list_hcp`.`pulmicort_respules_exc`,
   `lg_block_list_hcp`.`pulmicort_respules_block`,
   `lg_block_list_hcp`.`arimidex_exc`,
   `lg_block_list_hcp`.`arimidex_block`,
   `lg_block_list_hcp`.`zoladex_exc`,
   `lg_block_list_hcp`.`zoladex_block`,
   `lg_block_list_hcp`.`iressa_exc`,
   `lg_block_list_hcp`.`iressa_block`,
   `lg_block_list_hcp`.`atacand_exc`,
   `lg_block_list_hcp`.`atacand_block`,
   `lg_block_list_hcp`.`bevespi_aerosphere_exc`,
   `lg_block_list_hcp`.`bevespi_aerosphere_block`,
   `lg_block_list_hcp`.`tudorza_pressair_exc`,
   `lg_block_list_hcp`.`tudorza_pressair_block`,
   `lg_block_list_hcp`.`flumist_exc`,
   `lg_block_list_hcp`.`flumist_block`,
   `lg_block_list_hcp`.`synagis_exc`,
   `lg_block_list_hcp`.`synagis_block`,
   `lg_block_list_hcp`.`imfinzi_exc`,
   `lg_block_list_hcp`.`imfinzi_block`,
   `lg_block_list_hcp`.`daliresp_exc`,
   `lg_block_list_hcp`.`daliresp_block`,
   `lg_block_list_hcp`.`pulmicort_flexhaler_exc`,
   `lg_block_list_hcp`.`pulmicort_flexhaler_block`,
   `lg_block_list_hcp`.`crestor_exc`,
   `lg_block_list_hcp`.`crestor_block`,
   `lg_block_list_hcp`.`epanova_exc`,
   `lg_block_list_hcp`.`epanova_block`,
   `lg_block_list_hcp`.`sna_bydureon_exc`,
   `lg_block_list_hcp`.`sna_movantik_exc`,
   `lg_block_list_hcp`.`sna_onglyza_exc`,
   `lg_block_list_hcp`.`sna_qtern_exc`,
   `lg_block_list_hcp`.`sna_seroquel_xr_exc`,
   `lg_block_list_hcp`.`sna_symbicort_exc`,
   `lg_block_list_hcp`.`sna_symlin_exc`,
   `lg_block_list_hcp`.`sna_xigduo_xr_exc`,
   `lg_block_list_hcp`.`sna_arimidex_exc`,
   `lg_block_list_hcp`.`sna_bydureon_bcise_exc`,
   `lg_block_list_hcp`.`sna_byetta_exc`,
   `lg_block_list_hcp`.`sna_farxiga_exc`,
   `lg_block_list_hcp`.`sna_faslodex_exc`,
   `lg_block_list_hcp`.`sna_kombiglyze_xr_exc`,
   `lg_block_list_hcp`.`legal_comp_exc`,
   `lg_block_list_hcp`.`dns_exc`,
   `lg_block_list_hcp`.`aspen_exc`,
   cast(from_unixtime(unix_timestamp(substr(`lg_block_list_hcp`.`last_update_date`, 1, 11), 'dd-MMM-yyyy'), 'yyyy-MM-dd') as date) as `last_update_date` 
from
   `lg_base`.`lg_block_list_hcp`
