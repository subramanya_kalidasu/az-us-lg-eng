
import argparse

from pyspark.sql import SparkSession
import boto3
from botocore.exceptions import ClientError

import mysql.connector
from datetime import datetime, date, timedelta
import random



def getLastTuesday():
    today = date.today()
    offset = (today.weekday() - 5) % 7
    lastSaturday = today - timedelta(days=offset)
    print(lastSaturday)
    return str(lastSaturday)


def getDefaultDagId():
    print('ERROR# DAG_ID not passed. Using a generic')
    return "LGStepExecutor-Generic" + str(random.random())


def executeStatements(spark, statements, file):
    if len(statements) == 0:
        print('Error# There are no SQL Statements to Execute for the Step for HSQL File #{}'.format(file))
        return

    for s in statements:
        s = s.strip()
        if len(s) > 0:
            s = s.replace(';', '')
            s = s.replace('\n', '')
            s = s.replace('<=', '=')

            print("Executing Statement--> {}".format(s))
            spark.sql(s)

def skipProcess(args):
    if args.skip_process == 'False':
        return False

    cnx = mysql.connector.connect(user='emrHiveDevUser', password='dz234WilTq',
                                  host='devhive.cbmqljx60hvs.us-east-1.rds.amazonaws.com',
                                  database='lgetl')
    try:
        cursor = cnx.cursor()

        query = "select status, MAX(START_TIME) from lgetl.JOB_AUDIT WHERE LOAD_DATE = %(load_date)s and JOB_ID = %(dag_id)s"

        cursor.execute(query, {'load_date': args.load_date, 'dag_id': args.dag_id})

        r = cursor.fetchone()
        if r is not None:
            if r[0] == 'SUCCESS':
                print('There is already a successful run for the job {} and load_date {}. So, skipping it'.format(args.dag_id, args.load_date))
                return True

        print('Last Run is NOT a successful run for the job {} and load_date {}. So, moving to job execution'.format(
            args.dag_id, args.load_date))
    finally:
        if cnx is not None:
            cnx.close()

    return False

def process(args):

    spark = SparkSession\
        .builder\
        .master('yarn')\
        .appName(args.dag_id) \
        .config("hive.exec.dynamic.partition", "true")\
        .config("hive.exec.dynamic.partition.mode", "nonstrict")\
        .config('spark.executor.extraJavaOptions', '-XX:+PrintGCDetails -XX:+PrintGCTimeStamps')\
        .config('spark.executor.extraJavaOptions', '-XX:+PrintGCDetails -XX:+PrintGCTimeStamps')\
        .enableHiveSupport()\
        .getOrCreate()

    print('Spark Session Initialized# {}'.format(spark))

    if skipProcess(args) == True:
        return

    # temp_table = args.table + '_tmp'
    # spark.sql("create table lg_stage." + temp_table + "_v2 like lg_stage." + temp_table)
    # spark.sql("drop table lg_stage." + temp_table)
    # spark.sql("alter table lg_stage." + temp_table + "_v2 rename to lg_stage." + temp_table)

    try:
        lines = spark.read.text(args.metadata_file_name).collect()
        statements = list()
        for line in lines:
            line = line['value'].replace('${bus_date}', args.load_date)
            line = line.replace('{bus_date}', args.load_date)
            statements.append(line)

        executeStatements(spark, statements, args.metadata_file_name)
    except :
        print("Error# Invalid metadata-file-name. Either file is not found or invalid one".format(args.metadata_file_name))
        raise

    # table_name = args.table

    # data = [pyspark.sql.Row(status='SUCCESS')]
    # df = spark.createDataFrame(data).coalesce(1)
    # df.write.mode('overwrite').text('s3://az-us-lg-pilot-config/etl/audit/hsql/dag_' + args.dag_id + '_task_' + table_name + '_loaddate_' + args.load_date)

    # client = boto3.client('s3')
    # client.put_object(Body=success_data, Bucket='az-us-lg-pilot-config', Key='etl/audit/hsql/dag_' + args.dag_id + '_task_' + table_name + '_loaddate_' + args.load_date + '_SUCCESS')
    #
    # print('Created Job Tracker File {}'.format('s3://az-us-lg-pilot-config/etl/audit/hsql/dag_' + args.dag_id + '_task_' + table_name + '_loaddate_' + args.load_date + '_SUCCESS'))

    spark.stop()


def insertJobTracker(dag_id, run_id, load_date):
    cnx = mysql.connector.connect(user='emrHiveDevUser', password='dz234WilTq',
                                  host='devhive.cbmqljx60hvs.us-east-1.rds.amazonaws.com',
                                  database='lgetl')

    insert_new_run_sql = (
        "INSERT INTO JOB_AUDIT (RUN_ID, STATUS, JOB_ID, START_TIME, LOAD_DATE) "
        "VALUES (%s, %s, %s, %s, %s)")

    try:
        insert_cursor = cnx.cursor()

        insert_cursor.execute(insert_new_run_sql,
                              (run_id, 'STARTED', dag_id, datetime.now(), load_date))

        cnx.commit()

        print('Job Tracker inserted for {}'.format(run_id))

    finally:
        if cnx is not None:
          cnx.close()

def updateJobTracker(run_id, status):
    cnx = mysql.connector.connect(user='emrHiveDevUser', password='dz234WilTq',
                                  host='devhive.cbmqljx60hvs.us-east-1.rds.amazonaws.com',
                                  database='lgetl')
    try:
        update_new_run_sql = (
            "UPDATE JOB_AUDIT SET STATUS = %s, END_TIME = %s "
            "WHERE RUN_ID = %s")

        update_cursor = cnx.cursor()

        update_cursor.execute(update_new_run_sql,
                              (status, datetime.now(), run_id))

        cnx.commit()
    finally:
        if cnx is not None:
          cnx.close()

    print('Job Tracker Updated for {}'.format(run_id))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--metadata-file-name', required=True)
    parser.add_argument('-b', '--load-date', default=getLastTuesday())
    parser.add_argument('-d', '--dag-id', default=getDefaultDagId())
    parser.add_argument('-r', '--run-id', required=True)
    parser.add_argument('--table', required=True)
    parser.add_argument('--skip-process', default='False')

    args = parser.parse_args()

    print("Using metadata-file-name# " + args.metadata_file_name)
    print("Using load-date# " + args.load_date)
    print("Using dag_id# " + args.dag_id)
    print("Using run_id# " + args.run_id)

    insertJobTracker(args.dag_id, args.run_id, args.load_date)

    try:
        process(args)
        updateJobTracker(args.run_id, 'SUCCESS')
    except :
        updateJobTracker(args.run_id, 'FAIL')
        raise

