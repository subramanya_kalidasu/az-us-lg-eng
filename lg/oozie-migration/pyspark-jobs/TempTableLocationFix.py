
import argparse
import os

from pyspark.sql import SparkSession
from pyspark import SparkConf
from pyspark import SparkContext

#from ordered_set import OrderedSet
from datetime import date
from datetime import timedelta
import pyspark

import boto3
from botocore.exceptions import ClientError
import mysql.connector
from datetime import datetime, date, timedelta
import random


def getLastTuesday():
    today = date.today()
    offset = (today.weekday() - 5) % 7
    lastSaturday = today - timedelta(days=offset)
    print(lastSaturday)
    return str(lastSaturday)

def cleanUpTempTables(spark, statements):
    for s in statements:
        if (s.startswith('drop table')):
            print("Executing Drop Table Statement--> {}".format(s))
            if (s.startswith('drop table lg_stage')):
                s = s.replace('drop table lg_stage', 'drop table if exists lg_stage')
                spark.sql(s)
            else:
                spark.sql(s)


def getDefaultDagId():
    print('ERROR# DAG_ID not passed. Using a generic')
    return "LGStepExecutor-Generic" + str(random.random())


def executeStatements(spark, statements, table):
    if len(statements) == 0:
        print('Error# There are no SQL Statements to Execute for the Step Table#{}'.format(table))
        return

    for s in statements:
        print("Executing Statement--> {}".format(s))
        spark.sql(s)

#We can't use set as it doesnt preserve original order. ordered set is not supported on 2.7x
def removeDuplicates(li):
    my_set = set()
    res = []
    for e in li:
        if e not in my_set:
            res.append(e)
            my_set.add(e)
    #
    return res

def getSpakOverrides(args):
    overrides = []

    for key in args.__dict__.keys():
        if key.startswith('spark') and args.__dict__[key] is not None:
            newKey = key.replace('_', '.')
            overrides.append((newKey, args.__dict__[key]))
            print('Using Property value for Spark Conf key {} is {}'.format(newKey, args.__dict__[key]))
    return overrides

def skipProcess(args):
    if args.skip_process == 'False':
        return False

    # cnx = mysql.connector.connect(user='emrHiveDevUser', password='dz234WilTq',
    #                               host='devhive.cbmqljx60hvs.us-east-1.rds.amazonaws.com',
    #                               database='lgetl')
    # try:
    #     cursor = cnx.cursor()
    #
    #     query = "select status, MAX(START_TIME) from lgetl.JOB_AUDIT WHERE LOAD_DATE = %(load_date)s and JOB_ID = %(dag_id)s"
    #
    #     cursor.execute(query, {'load_date': args.load_date, 'dag_id': args.dag_id})
    #
    #     r = cursor.fetchone()
    #     if r is not None:
    #         if r[0] == 'SUCCESS':
    #             print('There is already a successful run for the job {} and load_date {}. So, skipping it'.format(args.dag_id, args.load_date))
    #             return True
    #
    #     print('Last Run is NOT a successful run for the job {} and load_date {}. So, moving to job execution'.format(
    #         args.dag_id, args.load_date))
    # finally:
    #     if cnx is not None:
    #         cnx.close()

    return False


def insertJobTracker(dag_id, run_id, batch_id, load_date, applicationId):
    cnx = mysql.connector.connect(user='emrHiveDevUser', password='dz234WilTq',
                                  host='devhive.cbmqljx60hvs.us-east-1.rds.amazonaws.com',
                                  database='lgetl')

    insert_new_run_sql = (
        "INSERT INTO JOB_AUDIT (RUN_ID, STATUS, JOB_ID, BATCH_ID, YARN_APP_ID, TYPE, START_TIME, LOAD_DATE, WORKFLOW_ID) "
        "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)")

    try:
        insert_cursor = cnx.cursor()

        x1 = dag_id.find('.')
        x2 = dag_id.rfind('.')

        workflow_id = dag_id[: x2]
        if x1 != x2:
            workflow_id = dag_id[x1 + 1: x2]

        insert_cursor.execute(insert_new_run_sql,
                              (run_id, 'STARTED', dag_id, batch_id, applicationId, 'TABLE', datetime.now(), load_date, workflow_id))

        cnx.commit()

        print('Job Tracker inserted for {}'.format(run_id))

    finally:
        if cnx is not None:
          cnx.close()

def updateJobTracker(run_id, status, msg):
    cnx = mysql.connector.connect(user='emrHiveDevUser', password='dz234WilTq',
                                  host='devhive.cbmqljx60hvs.us-east-1.rds.amazonaws.com',
                                  database='lgetl')
    try:
        update_new_run_sql = (
            "UPDATE JOB_AUDIT SET STATUS = %s, END_TIME = %s, ERROR_MSG = %s "
            "WHERE RUN_ID = %s")

        update_cursor = cnx.cursor()

        update_cursor.execute(update_new_run_sql,
                              (status, datetime.now(), msg, run_id))

        cnx.commit()
    finally:
        if cnx is not None:
          cnx.close()

    print('Job Tracker Updated for {} with status {}'.format(run_id, status))

def process(args):

    spark = SparkSession\
        .builder\
        .master('yarn')\
        .appName(args.dag_id) \
        .config("hive.exec.dynamic.partition", "true")\
        .config("hive.exec.dynamic.partition.mode", "nonstrict")\
        .config("spark.sql.crossJoin.enabled", "true")\
        .config('spark.executor.extraJavaOptions', '-XX:+PrintGCDetails -XX:+PrintGCTimeStamps')\
        .config('spark.executor.extraJavaOptions', '-XX:+PrintGCDetails -XX:+PrintGCTimeStamps')\
        .config('spark.sql.parquet.writeLegacyFormat', 'true')\
        .config('spark.yarn.maxAppAttempts', '1')\
        .enableHiveSupport()\
        .getOrCreate()

    print('Spark Session Initialized# {}'.format( spark))

    if skipProcess(args) == True:
        return False

    insertJobTracker(args.dag_id, args.run_id, args.batch_id, args.load_date, spark.sparkContext.applicationId)
    try:
        lines = spark.read.text("s3://az-us-lg-pilot-config/oozie-migration/" + args.metadata_file_name).collect()
        data = lines[0]['value'].replace('"', '').replace(';', '|').replace('kjfg254', '').replace('lg_load_sql',
                                                                                                   '').replace(
            'lg_load', '').replace('2017-06-10 21:54:49.042958000', '').split('|')
    except:
        print("Error# Invalid metadata-file-name. Either file is not found or invalid one".format(args.metadata_file_name))
        raise

    try:
        drop_partition = "alter table lg_stage." + args.metadata_file_name + " drop if exists partition (load_date='" + args.load_date + "')"
        print('Dropping current partition')
        print(drop_partition)
        spark.sql(drop_partition)
    except:
        pass

    statements = list()
    configs = list()
    tableName = data[1].strip()

    if len(tableName) == 0:
        print('Error# Invalid Table name in metadata-file-name file {}'.format(args.metadata_file_name))
        raise

    i = 2
    while i < len(data):
        d = data[i].strip()
        i += 1
        if (d.startswith('set')):
            configs.append(d)
        elif(d.isdigit() or len(d) == 0):
            continue
        else:
            d = d.replace('${bus_date}', args.load_date)
            d = d.replace('{bus_date}', args.load_date)
            statements.append(d)


    print('Table Name# ' + tableName)

    statements = removeDuplicates(statements)


    print("Using the Spark Configuration passed in# {}".format(configs))

    cleanUpTempTables(spark, statements)

    executeStatements(spark, statements, tableName)


    spark.stop()

    return True

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--metadata-file-name', required=True)
    parser.add_argument('-b', '--load-date', default=getLastTuesday())
    parser.add_argument('-d', '--dag-id', default=getDefaultDagId())
    parser.add_argument('-r', '--run-id', required=True)
    parser.add_argument('--skip-process', default='False')
    parser.add_argument('--spark_default_parallelism')
    parser.add_argument('--spark_serializer')
    parser.add_argument('--spark_kryoserializer_buffer_max')
    parser.add_argument('--spark_rdd_compress')
    parser.add_argument('--spark_executor_cores')
    parser.add_argument('--spark_broadcast_blockSize')
    parser.add_argument('--spark_files_overwrite')
    parser.add_argument('--spark_python_worker_memory')
    parser.add_argument('--spark_shuffle.file_buffer')
    parser.add_argument('--spark_reducer_maxSizeInFlight')
    parser.add_argument('--batch-id', required=True)

    args = parser.parse_args()

    print("Using metadata-file-name# " + args.metadata_file_name)
    print("Using load-date# " + args.load_date)
    print("Using dag_id# " + args.dag_id)
    print("Using run_id# " + args.run_id)
    print("Using batch_id# " + args.batch_id)

    for key in args.__dict__.keys():
        if key.startswith('spark'):
            print('Using Property value for Spark Conf key {} is {}'.format(key, args.__dict__[key]))


    try:
        if process(args):
            updateJobTracker(args.run_id, 'SUCCESS', None)
        else:
            print('Skipped processing with successful execution.')
    except Exception as e:
        msg = str(e)
        msg = (msg[:499] if len(msg) > 499 else msg)
        print('LG Job Failure {} Error msg {}'.format(args.run_id, msg))
        updateJobTracker(args.run_id, 'FAIL', msg)
        raise


