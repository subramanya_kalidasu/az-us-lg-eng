
import asyncio
import json
import json
import subprocess
import textwrap
from datetime import date
from datetime import timedelta

import boto3
import requests
import time
from livy import LivySession, SessionKind, SessionState

mylist = ["a", "b", "a", "c", "c"]


def getLastPartitionDay():
    today = date.today()
    offset = (today.weekday() - 5) % 7
    lastSaturday = today - timedelta(days=offset)
    print(lastSaturday)
    return str(lastSaturday)

def sendQuery(session_url, data):
    statements_url = session_url + '/statements'
    response = requests.post(statements_url, data=json.dumps(data), headers={'Content-Type': 'application/json'})
    print(response.json())


def getTblValidationQuery():
    tableName = 'lg_smpl_prsbr_terr_alloc_f'

    data = {
        'code': textwrap.dedent("""
		val d = spark.sql("SELECT COUNT(*) FROM lg_stage.lg_smpl_prsbr_terr_alloc_f where load_date='2019-06-08'")
		val e = d.collect()[0]
		%json e
		""")}
    return data





# emrX = boto3.client('emr', region_name='us-east-1')
# instances = emrX.list_instances(ClusterId='j-1ILSTXWNI7VJ5', InstanceGroupTypes=['MASTER'])
# master_ip = instances['Instances'][0]['PrivateIpAddress']
# print (master_ip)
#
# host = 'http://' + master_ip + ':8998'
# data = {'kind': 'spark'}
# headers = {'Content-Type': 'application/json'}
# # response = requests.post(host + '/sessions', data=json.dumps(data), headers=headers)
#
# status = ''
# host = 'http://' + master_ip + ':8998'

# print(response.headers)
# headers=response.headers
# session_url = host + headers['location']
# while status != 'idle':
#     time.sleep(3)
#     status_response = requests.get(session_url, headers=headers)
#     status = status_response.json()['state']

# sendQuery(session_url, data=json.dumps(getTblValidationQuery()))

# with LivySession(host, kind=SessionKind.PYSPARK) as session:
#     assert session.state == SessionState.IDLE
#     table_name = 'lg_smpl_prsbr_terr_alloc_f'
#     #session.run('v = spark.sql("SELECT COUNT(*) FROM lg_stage.lg_smpl_prsbr_terr_alloc_f where load_date=\'2019-06-08\'")')
#     session.run(
#         'v = spark.sql("SELECT COUNT(*) FROM lg_stage.' + table_name + ' where load_date=\'' + getLastPartitionDay() + '\'")')
#     v = session.read('v')
#     print('Record Count {}'.format(v['count(1)'][0]))


# url = 'http://' + master_ip + ':8998/batches'
#
# job_config = {"name":"lg_cust_mcp_plan_demog_d", "file": "s3://az-us-lg-pilot-config/oozie-migration/source_code/StepExecutorV2.py",
#                 "conf": {"spark.default.parallelism": "150"},
#                 "args":["--metadata-file-name", "lg_cust_mcp_plan_demog_d.txt",
#                 "--load-date", "2019-06-10"]}

#print(json.dumps(job_config))

def sparkSubmit():

    r = requests.post(url, data=json.dumps(job_config))
    if r.status_code != 200:
        r.raise_for_status()

    response = r.json()

    print('RESPONSE FROM THE LIVY SUBMIT# {}'.format(response))
    return str(response['id'])

def track_statement_progress(id, timeout):
    attempt = 0
    print('RESPONSE FROM THE LIVY SUBMIT for task {} and attempt {}'.format(id, attempt))
    job_started = False

    timeout = timeout/10

    while(attempt <= 10):
        attempt = attempt + 1
        r = requests.get(url + '/' + id + '/state')
        if r.status_code != 200:
            print('Livy Session for the previously submitted job is not found. id {}'.format(id))
            r.raise_for_status()

        response = r.json()
        job_state = response['state']
        print('RESPONSE FROM THE LIVY SUBMIT for task {}, attempt {} and state {}'.format(id, attempt, job_state))

        if job_state.lower() == 'accepted' or job_state.lower() == 'starting':
            time.sleep(timeout)
            continue
        else:
            job_started = True
            print ('Final Job Status {} '.format(job_state))
            print(response)
            return True

    print('Job Failed to start within TIME OUT {} on EMR via Livy'.format(timeout))

    return False

def getSessions(id):
    url = 'http://' + master_ip + ':8998'
    taskComplete = False
    attempt = 1
    print('RESPONSE FROM THE LIVY SUBMIT for task {} and attempt {}'.format(id, attempt))
    r = requests.get(url + '/sessions')
    if r.status_code != 200:
        r.raise_for_status()

    response = r.json()
    print(response)

#id = sparkSubmit()
#
# if track_statement_progress(id, 300):
#     print('Job started on EMR via Livy')
# else:
#     print('Job Failed to start within TIME OUT on EMR via Livy')
#
#
#
import mysql.connector
from datetime import datetime, date, timedelta


cnx = mysql.connector.connect(user='emrHiveDevUser', password='dz234WilTq',
                              host='devhive.cbmqljx60hvs.us-east-1.rds.amazonaws.com',
                              database='lgetl')
#
# insert_new_run_sql = (
#   "INSERT INTO JOB_AUDIT (RUN_ID, STATUS, START_TIME) "
#   "VALUES (%s, %s, %s)")

# insert_cursor = cnx.cursor()
#
# insert_cursor.execute(insert_new_run_sql,
#              ('Test-123', 'STARTED', datetime.now()))
#
# cnx.commit()

cursor = cnx.cursor()

#query = ("SELECT count(*) from JOB_AUDIT where LOAD_DATE=%(dag_id)s")

query ="select status, MAX(START_TIME) from lgetl.JOB_AUDIT WHERE LOAD_DATE = %(load_date)s and RUN_ID = %(dag_id)s"

cursor.execute(query, { 'load_date': '2019-06-10','dag_id': 'wf_livySample_lg_cust_mcp_plan_demog_d_distcp_1561669640.369492234' })

r=cursor.fetchone()

if r is not None:
    print(r[0])

def testJobStatus(args):
    try:
        cursor = cnx.cursor()

        query = "select status, MAX(START_TIME) from lgetl.JOB_AUDIT WHERE LOAD_DATE = %(load_date)s and RUN_ID = %(dag_id)s"

        cursor.execute(query, {'load_date': args['load_date'], 'dag_id': args['dag_id']})

        r = cursor.fetchone()
        if r is not None:
            if r[0] == 'SUCCESS':
                print('There is already a successful run for the job {} and load_date {}. So, skipping it'.format(args['dag_id'], args['load_date']))
                return True

        print('Last Run is NOT a successful run for the job {} and load_date {}. So, moving to job execution'.format(
            args['dag_id'], args['load_date']))
    finally:
        if cnx is not None:
            cnx.close()


testJobStatus({'load_date':'2019-06-10','dag_id': 'wf_livySample_lg_cust_mcp_plan_demog_d_distcp_1561669640.369492'})

#print(cursor)
# for (status) in cursor:
#     print(status)

# update_new_run_sql = (
#     "UPDATE JOB_AUDIT SET STATUS = %s, END_TIME = %s "
#     "WHERE RUN_ID = %s")
#
# update_cursor = cnx.cursor()
#
# update_cursor.execute(update_new_run_sql,
#                       ('SUCCESS', datetime.now(), 'Test-123'))
#
# cnx.commit()
#
cnx.close()
