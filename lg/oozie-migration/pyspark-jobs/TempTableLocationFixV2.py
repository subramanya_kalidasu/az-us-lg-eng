

from pyspark.sql import SparkSession
import mysql.connector
from datetime import date, datetime
import argparse

def executeStatements(spark, statements):
    if len(statements) == 0:
        print('Error# There are no SQL Statements to Execute for Temp Table location fix')
        return

    for s in statements:
        try:
            s = s.replace('`','')
            s = s.replace(';','')
            if s.isspace():
                continue
            print("Executing Statement--> {}".format(s))
            spark.sql(s)
        except Exception as e:
            print(e)
            print('Ignoring Exception.')
            pass

def insertJobTracker(dag_id, run_id, batch_id, load_date, applicationId):
    cnx = mysql.connector.connect(user='emrHiveDevUser', password='dz234WilTq',
                                  host='devhive.cbmqljx60hvs.us-east-1.rds.amazonaws.com',
                                  database='lgetl')

    insert_new_run_sql = (
        "INSERT INTO JOB_AUDIT (RUN_ID, STATUS, JOB_ID, BATCH_ID, YARN_APP_ID, TYPE, START_TIME, LOAD_DATE, WORKFLOW_ID) "
        "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)")

    try:
        insert_cursor = cnx.cursor()

        workflow_id = 'Initialization'

        insert_cursor.execute(insert_new_run_sql,
                              (run_id, 'STARTED', dag_id, batch_id, applicationId, 'TABLE', datetime.now(), load_date, workflow_id))

        cnx.commit()

        print('Job Tracker inserted for {}'.format(run_id))
    except mysql.connector.errors.IntegrityError as e:
        print(e)
        print('Ignoring Exception as this is a retry attempt...')
        pass

    finally:
        if cnx is not None:
          cnx.close()
def updateJobTracker(run_id, status, msg):
    cnx = mysql.connector.connect(user='emrHiveDevUser', password='dz234WilTq',
                                  host='devhive.cbmqljx60hvs.us-east-1.rds.amazonaws.com',
                                  database='lgetl')
    try:
        update_new_run_sql = (
            "UPDATE JOB_AUDIT SET STATUS = %s, END_TIME = %s, ERROR_MSG = %s "
            "WHERE RUN_ID = %s")

        update_cursor = cnx.cursor()

        update_cursor.execute(update_new_run_sql,
                              (status, datetime.now(), msg, run_id))

        cnx.commit()
    finally:
        if cnx is not None:
          cnx.close()

    print('Job Tracker Updated for {} with status {}'.format(run_id, status))


def process(args):
    spark = SparkSession\
        .builder\
        .master('yarn')\
        .appName('TempTableLocationFix') \
        .config("hive.exec.dynamic.partition", "true")\
        .config("hive.exec.dynamic.partition.mode", "nonstrict")\
        .config("spark.sql.crossJoin.enabled", "true")\
        .config('spark.executor.extraJavaOptions', '-XX:+PrintGCDetails -XX:+PrintGCTimeStamps')\
        .config('spark.executor.extraJavaOptions', '-XX:+PrintGCDetails -XX:+PrintGCTimeStamps')\
        .config('spark.sql.parquet.writeLegacyFormat', 'true')\
        .config('spark.yarn.maxAppAttempts', '0')\
        .enableHiveSupport()\
        .getOrCreate()

    print('Spark Session Initialized# {}'.format( spark))

    insertJobTracker(args.dag_id, args.run_id, args.batch_id, args.load_date, spark.sparkContext.applicationId)
    try:
        lines = spark.read.text("s3://az-us-lg-pilot-config/oozie-migration/hsql/lg_stage_tmp_tables_dropncreate_v1.hql").collect()

        stmnts = ''
        for f in lines:
            stmnts = stmnts + f['value']

        stmnts = stmnts.split(';')
    except:
        print("Error# Invalid metadata-file-name. Either file is not found or invalid one".format('s3://az-us-lg-pilot-config/oozie-migration/hsql/lg_stage_tmp_tables_dropncreate_v1.hql'))
        raise


    executeStatements(spark, stmnts)


    spark.stop()

    return True

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-b', '--load-date', required=True)
    parser.add_argument('-d', '--dag-id', required=True)
    parser.add_argument('-r', '--run-id', required=True)
    parser.add_argument('--batch-id', required=True)

    args = parser.parse_args()

    print("Using load-date# " + args.load_date)
    print("Using dag_id# " + args.dag_id)
    print("Using run_id# " + args.run_id)
    print("Using batch_id# " + args.batch_id)

    for key in args.__dict__.keys():
        if key.startswith('spark'):
            print('Using Property value for Spark Conf key {} is {}'.format(key, args.__dict__[key]))

    try:
        if process(args):
            updateJobTracker(args.run_id, 'SUCCESS', None)
        else:
            print('Skipped processing with successful execution!!!')
    except Exception as e:
        msg = str(e)
        msg = (msg[:499] if len(msg) > 499 else msg)
        print('LG Job Failure {} Error msg {}'.format(args.run_id, msg))
        updateJobTracker(args.run_id, 'FAIL', msg)
        raise


