import subprocess
from subprocess import CalledProcessError

import argparse
import boto3
from botocore.exceptions import ClientError

from pyspark.sql import SparkSession
from pyspark import SparkConf
import pyspark

from datetime import date
from datetime import timedelta
import mysql.connector
from datetime import datetime, date, timedelta
import random


def getDefaultDagId():
    print('ERROR# DAG_ID not passed. Using a generic')
    return "LGStepExecutor-Generic" + str(random.random())

def getLastTuesday():
    today = date.today()
    offset = (today.weekday() - 5) % 7
    lastSaturday = today - timedelta(days=offset)
    print(lastSaturday)
    return str(lastSaturday)

def getSpakOverrides(args):
    overrides = []

    for key in args.__dict__.keys():
        if key.startswith('spark') and args.__dict__[key] is not None:
            newKey = key.replace('_', '.')
            overrides.append((newKey, args.__dict__[key]))
            print('Using Property value for Spark Conf key {} is {}'.format(newKey, args.__dict__[key]))
    return overrides

def skipProcess(args):
    if args.skip_process == 'False':
        return False

    cnx = mysql.connector.connect(user='emrHiveDevUser', password='dz234WilTq',
                                  host='devhive.cbmqljx60hvs.us-east-1.rds.amazonaws.com',
                                  database='lgetl')
    try:
        cursor = cnx.cursor()

        query = "select status, MAX(START_TIME) from lgetl.JOB_AUDIT WHERE LOAD_DATE = %(load_date)s and JOB_ID = %(dag_id)s"

        cursor.execute(query, {'load_date': args.load_date, 'dag_id': args.dag_id})

        r = cursor.fetchone()
        if r is not None:
            if r[0] == 'SUCCESS':
                print('There is already a successful run for the job {} and load_date {}. So, skipping it'.format(args.dag_id, args.load_date))
                return True

        print('Last Run is NOT a successful run for the job {} and load_date {}. So, moving to job execution'.format(
            args.dag_id, args.load_date))
    finally:
        if cnx is not None:
            cnx.close()

    return False

def process(args):
    spark = SparkSession\
        .builder\
        .master('yarn')\
        .appName(args.dag_id) \
        .config("hive.exec.dynamic.partition", "true")\
        .config("hive.exec.dynamic.partition.mode", "nonstrict")\
        .config('spark.executor.extraJavaOptions', '-XX:+PrintGCDetails -XX:+PrintGCTimeStamps')\
        .config('spark.executor.extraJavaOptions', '-XX:+PrintGCDetails -XX:+PrintGCTimeStamps')\
        .enableHiveSupport()\
        .getOrCreate()

    print('Spark Session Initialized# {}'.format( spark))

    if skipProcess(args) == True:
        return

    copycommand = "hadoop distcp -delete -overwrite " + args.source + "/load_date=" + args.load_date + " s3://az-us-lg-pilot-stage/staging/" + args.target + "/load_date=" + args.load_date
    print('Initializing distributed copy ---> {}'.format(copycommand))


    try:
        distcpoutput = subprocess.check_output(
            copycommand,
            shell=True)

        print(distcpoutput)

        returnValue = subprocess.check_output(
            "hdfs dfs -ls s3://az-us-lg-pilot-stage/staging/" + args.target + "/load_date=" + args.load_date + "/ | wc -l",
            shell=True)

        print('Number of files copied to target# {}'.format(returnValue))
        #Zero records is not an error as it could be data issue. Should be caught in QC

    except CalledProcessError as e:
        print('Distributed Copy failed')
        raise e


    table_name = args.target

    # data = [pyspark.sql.Row(status='SUCCESS')]
    # df = spark.createDataFrame(data).coalesce(1)
    # df.write.mode('overwrite').text('s3://az-us-lg-pilot-config/etl/audit/distcp/dag_' + args.dag_id + '_task_' + table_name + '_loaddate_' + args.load_date)

    success_data = b'SUCCESS'
    client = boto3.client('s3')
    client.put_object(Body=success_data, Bucket='az-us-lg-pilot-config', Key='etl/audit/distcp/dag_' + args.dag_id + '_task_' + table_name + '_loaddate_' + args.load_date + '_SUCCESS')

    print('Created Job Tracker File {}'.format('s3://az-us-lg-pilot-config/etl/audit/distcp/dag_' + args.dag_id + '_task_' + table_name + '_loaddate_' + args.load_date + '_SUCCESS'))

    spark.stop()

def insertJobTracker(dag_id, run_id, load_date):
    cnx = mysql.connector.connect(user='emrHiveDevUser', password='dz234WilTq',
                                  host='devhive.cbmqljx60hvs.us-east-1.rds.amazonaws.com',
                                  database='lgetl')

    insert_new_run_sql = (
        "INSERT INTO JOB_AUDIT (RUN_ID, STATUS, JOB_ID, START_TIME, LOAD_DATE) "
        "VALUES (%s, %s, %s, %s, %s)")

    try:
        insert_cursor = cnx.cursor()

        insert_cursor.execute(insert_new_run_sql,
                              (run_id, 'STARTED', dag_id, datetime.now(), load_date))

        cnx.commit()

        print('Job Tracker inserted for {}'.format(run_id))

    finally:
        if cnx is not None:
          cnx.close()

def updateJobTracker(run_id, status):
    cnx = mysql.connector.connect(user='emrHiveDevUser', password='dz234WilTq',
                                  host='devhive.cbmqljx60hvs.us-east-1.rds.amazonaws.com',
                                  database='lgetl')
    try:
        update_new_run_sql = (
            "UPDATE JOB_AUDIT SET STATUS = %s, END_TIME = %s "
            "WHERE RUN_ID = %s")

        update_cursor = cnx.cursor()

        update_cursor.execute(update_new_run_sql,
                              (status, datetime.now(), run_id))

        cnx.commit()
    finally:
        if cnx is not None:
          cnx.close()

    print('Job Tracker Updated for {}'.format(run_id))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-b', '--load-date', default=getLastTuesday())
    parser.add_argument('-d', '--dag-id', default=getDefaultDagId())
    parser.add_argument('-r', '--run-id', required=True)
    parser.add_argument('--source', required=True)
    parser.add_argument('--target', required=True)
    parser.add_argument('--skip-process', default='False')

    args = parser.parse_args()

    print("Using source# " + args.source)
    print("Using target# " + args.target)
    print("Using load-date# " + args.load_date)
    print("Using dag_id# " + args.dag_id)
    print("Using run_id# " + args.run_id)
    for key in args.__dict__.keys():
        if key.startswith('spark'):
            print('Using Property value for Spark Conf key {} is {}'.format(key, args.__dict__[key]))

    insertJobTracker(args.dag_id, args.run_id, args.load_date)

    try:
        process(args)
        updateJobTracker(args.run_id, 'SUCCESS')
    except :
        updateJobTracker(args.run_id, 'FAIL')
        raise

