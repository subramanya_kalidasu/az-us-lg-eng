from datetime import timedelta, datetime
from datetime import date

from airflow.models import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.models import Variable
from airflow.exceptions import AirflowException
import boto3
from botocore.exceptions import WaiterError
from airflow.operators.subdag_operator import SubDagOperator
import json
import requests
import uuid
import time
from airflow.hooks.mysql_hook import MySqlHook
from airflow.operators.bash_operator import BashOperator

from qcfinal import qcfinal

def buildDefaultArgs():
    args = {}

    try:
        lg_cluster_id = Variable.get("lg_cluster_id")
        args.update(lg_cluster_id= lg_cluster_id)
        print('Using Cluster ID for the Dag# {}'.format(lg_cluster_id))

        lg_host_ip = Variable.get("lg_host_ip")
        args.update(lg_host_ip=lg_host_ip)
        print('Using lg_host_ip for the Dag# {}'.format(lg_host_ip))


        lg_skip_processed = Variable.get("lg_skip_processed")
        args.update(lg_skip_processed=lg_skip_processed)
        print('Using lg_skip_processed for the Dag# {}'.format(lg_skip_processed))

        lg_load_date = Variable.get("lg_load_date")
        args.update(lg_load_date=lg_load_date)
        print('Using lg_load_date for the Dag# {}'.format(lg_load_date))

        lg_task_submit_timeout = Variable.get("lg_task_submit_timeout")
        args.update(lg_task_submit_timeout=lg_task_submit_timeout)
        print('Using lg_task_submit_timeout for the Dag# {}'.format(lg_task_submit_timeout))

        lg_task_timeout = Variable.get("lg_task_timeout")
        args.update(lg_task_timeout=lg_task_timeout)
        print('Using lg_task_timeout for the Dag# {}'.format(lg_task_timeout))

        lg_run_job_mode = Variable.get("lg_run_job_mode")
        args.update(lg_run_job_mode=lg_run_job_mode)
        print('Using lg_run_job_mode for the Dag# {}'.format(lg_run_job_mode))

        args.update(owner='airflow')
        args.update(depends_on_past=False)


        return args
    except Exception as e:
        print(e)
        raise AirflowException(e)




dag = DAG(
    'ClusterLifeCycle',
    concurrency=3,
    max_active_runs=1,
    start_date=datetime.now(),
    catchup=False
)

def getipfromcluster(emrX, c_id):
    cd = emrX.describe_cluster(ClusterId=c_id)
    print(cd)
    print(cd['Cluster'])
    print(cd['Cluster']['MasterPublicDnsName'])

    ip = cd['Cluster']['MasterPublicDnsName']
    c = ip.find('.ec2.internal')
    master_ip = ip[3: c].replace('-', '.')
    Variable.set('lg_host_ip', master_ip)
    return master_ip

def initialize_cluster():
    #Check if the Cluster already exists before spinning up one.
    emrX = boto3.client('emr', region_name='us-east-1')
    response = emrX.list_clusters(
        CreatedAfter=str(date.today() - timedelta(days=10)) + " 00:00:00",
        ClusterStates=['STARTING', 'BOOTSTRAPPING', 'RUNNING', 'WAITING']
    )

    for r in response['Clusters']:
        if r['Name'] == 'AZ-US-Commercial-LG-Ingestion-EMR':
            c_id = r['Id']
            print('Found an existing Cluster {}. Attempting to use it.'.format(c_id))
            Variable.set('lg_cluster_id', c_id)

            getipfromcluster(emrX, c_id)

            # As Cluster already exists, ensure its readiness before exiting the initialization.
            waiter = emrX.get_waiter('cluster_running')
            try:
                waiter.wait(
                    ClusterId=c_id,
                    WaiterConfig={
                        'Delay': 60,
                        'MaxAttempts': 40
                    }
                )
                return
            except WaiterError as e:
                print('ERROR# Failed to spin up Cluster')
                print(e)
                raise e


    #Cluster doesn't exists, create a new one.
    client = boto3.client('lambda', region_name='us-east-1')
    print(client)
    payload3 = b"""{
        "ClusterName": "AZ-US-Commercial-LG-Ingestion-EMR",
        "EMR_CONFIGBUCKET": "az-us-lg-pilot-config",
        "EMR_CONFIGBUCKET_KEYNAME": "Lambda/Config/AZ-Commercial-LG-Ingestion-EMR.json",
        "EMR_CORE_COUNT": "2",
        "EMR_CORE_TYPE": "r5.4xlarge",
        "EMR_TASK_MIN_COUNT": "0",
        "EMR_TASK_MAX_COUNT": "0",
        "EMR_TASK_TYPE": "r5.4xlarge",
        "EMR_MASTER_TYPE": "r5.2xlarge",
        "EMR_TASK_INIT_COUNT": "0",    
        "Action": "Create"    
        }"""
    response = client.invoke(
        FunctionName="AZ-Commercial-LG-EMRCreate",
        InvocationType='RequestResponse',
        Payload=payload3
    )

    cluster_id = response['Payload'].read().decode().replace('\"','').replace('\\','')
    Variable.set('lg_cluster_id', cluster_id)
    print('Using Cluster ID for the Dag# {}'.format(cluster_id))


    waiter = emrX.get_waiter('cluster_running')

    try:
        waiter.wait(
            ClusterId=cluster_id,
            WaiterConfig={
                'Delay': 60,
                'MaxAttempts': 40
            }
        )

    except WaiterError as e:
        print('ERROR# Failed to spin up Cluster')
        print(e)
        raise e

    getipfromcluster(emrX, cluster_id)

clusterCreate = PythonOperator(
    task_id='cluster_create_tsk',
    python_callable=initialize_cluster,
    dag=dag)

DEFAULT_ARGS = buildDefaultArgs()



def clustersetupwatch(**context):
    print(context)
    task_nm = context['templates_dict']['run-id']
    run_id = task_nm
    print('Waiting on job completion. Tracking RUN_ID {}'.format(run_id))

    connection = MySqlHook(mysql_conn_id='lg_job_tracker')
    query = "SELECT STATUS from lgetl.JOB_AUDIT where STATUS != 'STARTED' and RUN_ID=%(run_id)s"

    lg_task_timeout = int(Variable.get("lg_task_timeout"))
    max_attempts = lg_task_timeout / 60
    while (max_attempts >= 0):
        max_attempts -= 1
        print('Attempting to Job Tracker Status')
        row = connection.get_first(query, parameters={'run_id': run_id})
        print('Value for DBhook {}'.format(row))
        if row is not None and len(row) > 0:
            print(row[0])
            if row[0] == 'SUCCESS' or row[0] == 'SKIP':
                return True
            elif row[0] == 'FAIL':
                break
        else:
            time.sleep(60)
            print('Job in progress. No Job Tracker record')

    raise AirflowException('Failed to detect Job Tracker File. Failing the task')

def clustersetup(**kwargs):
    batch_run_id = kwargs['dag_run'].run_id
    run_uuid = str(uuid.uuid1())
    lg_load_date = Variable.get("lg_load_date")
    job_config = {"name": run_uuid,
                  "file": "s3://az-us-lg-pilot-config/oozie-migration/source_code/TempTableLocationFix.py",
                  "conf": {"spark.default.parallelism": 200,
                           "spark.driver.memory": '1g',
                           "spark.submit.deployMode": 'cluster',
                           "spark.executor.memory": '2g'},
                  "args": ["--dag-id", 'wf_lg_wkly_master.initialization',
                           "--run-id", run_uuid,
                           "--batch-id", batch_run_id,
                           "--load-date", lg_load_date
                           ]}

    host = 'http://' + Variable.get('lg_host_ip') + ':8998'
    r = requests.post(host + '/batches', data=json.dumps(job_config))
    if r.status_code >= 200 and r.status_code < 300:
        response = r.json()
        livy_session_id = str(response['id'])
        print('Livy Session Id# {}'.format(livy_session_id))

    elif r.status_code != 200 :
        print('Failed to submit job. HTTP Response# {}'.format(r.json()))
        raise AirflowException('Failed to submit job. HTTP Response# {}'.format(r.json()))

    return run_uuid

# qc1_task = BashOperator(
#     task_id='qc1_task',
#     bash_command="/usr/share/airflow/dags/qc/qc1/edhQCMaster/edhQCMaster_run.sh '" + Variable.get("lg_load_date") + "' ",
#     dag=dag)
#
# schema_switch_setup_tsk = BashOperator(
#     task_id='schema_switch_setup_tsk',
#     bash_command='/usr/share/airflow/dags/schema_switch/setup_schema_switch.sh ' + Variable.get('lg_host_ip') + ' ',
#     dag=dag)


setup = PythonOperator(
    task_id="clustersetup_tsk",
    provide_context=True,
    python_callable=clustersetup,
    dag=dag)

watchStep = PythonOperator(
    task_id='clustersetup_watch',
    provide_context=True,
    python_callable=clustersetupwatch,
    templates_dict={'run-id': "{{ ti.xcom_pull(task_ids='clustersetup_tsk') }}"},
    dag=dag)

qcfinal = SubDagOperator(
    subdag=qcfinal(dag.dag_id, dag.start_date, DEFAULT_ARGS, task_id='qcfinal'),
    task_id = 'qcfinal',
    dag=dag
)

#clusterCreate >> sub_dag1
#clusterCreate >> setup >> watchStep
#clusterCreate >> qc1_task
clusterCreate >> qcfinal