from lgutils.utils import lgfunctions
from airflow.models import DAG
from datetime import datetime
from airflow.models import Variable
from airflow.operators.bash_operator import BashOperator
from airflow.contrib.operators.emr_add_steps_operator import EmrAddStepsOperator


def qc2(parent_dag_name, start_date, default_args, task_id):
    # Initialize lg functions with dag
    # optional arguments skip_processed, load_date, task_submit_timeout, task_timeout, run_job_mode, cluster_id
    # If no overrides passed, it takes values from Airflow Variables defined on Airflow portal running on 8080 port.

    dag = DAG(
        dag_id=parent_dag_name + '.' + task_id,
        concurrency=int(Variable.get("lg_concurrency_val")),
        max_active_runs=1,
        default_args=default_args,
        start_date=datetime.now(),
        catchup=False
    )

    lg_load_date = Variable.get("lg_load_date")

    t1 = BashOperator(
        task_id='qc2_task',
        bash_command='/usr/share/airflow/dags/qc/qc2/lgQCMasterP2_2.0/lgQCMasterP2/lgQCMasterP2_run.sh --context_param load_date="' + lg_load_date + '" --context_param environment="' + Variable.get('lg_host_ip') + '"',
        dag=dag)


    def getqcconfig():
        step = [
            {
                "Name": "QC3",
                "ActionOnFailure": "CONTINUE",
                "HadoopJarStep": {
                    "Jar": "s3://az-us-lg-pilot-config/oozie-migration/source_code/QCRecon-0.0.1-SNAPSHOT.jar",
                    "Args": [
                        lg_load_date
                    ]
                }
            }
        ]
        return step

    t2 = EmrAddStepsOperator(
        task_id="QC3" ,
        job_flow_id=Variable.get('lg_cluster_id'),
        aws_conn_id='lg_emr_ingestion',
        steps=getqcconfig,
        dag=dag
    )

    t1 >> t2
    return dag