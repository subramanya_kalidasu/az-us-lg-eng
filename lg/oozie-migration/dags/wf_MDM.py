from lgutils.utils import lgfunctions
from airflow.models import DAG
from datetime import datetime


DEFAULT_ARGS = {
    'owner': 'airflow',
    'depends_on_past': False
}

dag = DAG(
    'wf_MDM',
    concurrency=3,
    max_active_runs=1,
    default_args=DEFAULT_ARGS,
    start_date=datetime.now(),
    catchup=False
)

#Initialize lg functions with dag
#optional arguments skip_processed, load_date, task_submit_timeout, task_timeout, run_job_mode, cluster_id
#If no overrides passed, it takes values from Airflow Variables defined on Airflow portal running on 8080 port.
lg = lgfunctions(dag=dag)


#Process table ETL
s1 = lg.createlivytablestep(None, table='lg_mdm_prod_d', executor_memory='5g', driver_memory='3g')

s2 = lg.createlivytablestep(None, table='lg_mdm_prod_rel', executor_memory='5g', driver_memory='3g')

s3 = lg.createlivytablestep(None, table='lg_mdm_prod_alt_id', executor_memory='5g', driver_memory='3g')

s4 = lg.createlivytablestep(None, table='lg_mdm_prod_spec_d', executor_memory='5g', driver_memory='3g')

s5 = lg.createlivytablestep(None, table='lg_mdm_cust_merged_xref', executor_memory='5g', driver_memory='3g')

s6 = lg.createlivytablestep(None, table='lg_mdm_cust_alt_id', executor_memory='5g', driver_memory='3g')

s7 = lg.createlivytablestep(None, table='lg_mdm_cust_affil_d', executor_memory='5g', driver_memory='3g')

s8 = lg.createlivytablestep(None, table='lg_mdm_cust_addr_d', executor_memory='5g', driver_memory='3g')

s9 = lg.createlivytablestep(None, table='lg_mdm_cust_d', executor_memory='5g', driver_memory='3g')
