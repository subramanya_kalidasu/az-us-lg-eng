
import airflow
from airflow.models import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import BranchPythonOperator

from airflow.utils.dates import days_ago
from airflow.models import Variable

from lgutils.utils import lgfunctions
from airflow.contrib.sensors.emr_step_sensor import EmrStepSensor
from airflow.contrib.operators.emr_add_steps_operator import EmrAddStepsOperator


DEFAULT_ARGS = {
    'owner': 'airflow',
    'depends_on_past': False
}

skipPreviousSteps = Variable.get("lg_ingestion_skip_processed")
partitionDate = Variable.get("lg_load_date")

# emrX = boto3.client('emr', region_name='us-east-1')
clusterId = 'j-55BV0X84633R'#Variable.get("lg_ingestion_cluster_id")



DEFAULT_ARGS = {
    'owner': 'airflow',
    'depends_on_past': False
}

dag = DAG(
    'wf_lg_cust_mcp_plan_demog_d',
    default_args=DEFAULT_ARGS,
    schedule_interval='@once',
    start_date=days_ago(1),
    catchup=False
)

#Create LG Utility funtions object
lg = lgfunctions(clusterId, partitionDate, skipPreviousSteps, dag)

repair_lg_cust_mcp_plan_demog_d_pre = lg.createHsqlStep(task_type='hsql', table='repair_lg_cust_mcp_plan_demog_d', hsql_file='s3://az-us-lg-pilot-config/oozie-migration/hsql/repair_trunc_lg_cust_mcp_plan_demog_d.hql')
repair_lg_cust_mcp_plan_demog_d_pre_checker = lg.createWatchStep(table='repair_lg_cust_mcp_plan_demog_d', operator=repair_lg_cust_mcp_plan_demog_d_pre)

lg_cust_mcp_plan_demog_d = lg.createTableStep(table='lg_cust_mcp_plan_demog_d')
lg_cust_mcp_plan_demog_d_step_checker = lg.createWatchStep(table='lg_cust_mcp_plan_demog_d', operator=lg_cust_mcp_plan_demog_d)

#lg_cust_mcp_plan_demog_d_distcpy = lg.createDstCpStep(task_type='distcp', table='lg_cust_mcp_plan_demog_d', source='/user/azhueadmin/lg_cust_mcp_plan_demog_d_tmp')
#lg_cust_mcp_plan_demog_d_distcpy_checker = lg.createWatchStep(table='lg_cust_mcp_plan_demog_d', operator=lg_cust_mcp_plan_demog_d_distcpy)


#repair_lg_cust_mcp_plan_demog_d = lg.createHsqlStep(task_type='hsql', table='repair_lg_cust_mcp_plan_demog_d', hsql_file='s3://az-us-lg-pilot-config/oozie-migration/hsql/repair_trunc_lg_cust_mcp_plan_demog_d.hql')
#repair_lg_cust_mcp_plan_demog_d_checker = lg.createWatchStep(table='repair_lg_cust_mcp_plan_demog_d', operator=repair_lg_cust_mcp_plan_demog_d)

repair_lg_cust_mcp_plan_demog_d_pre >> repair_lg_cust_mcp_plan_demog_d_pre_checker >> lg_cust_mcp_plan_demog_d >> lg_cust_mcp_plan_demog_d_step_checker
#lg_cust_mcp_plan_demog_d_step_checker >> lg_cust_mcp_plan_demog_d_distcpy >> lg_cust_mcp_plan_demog_d_distcpy_checker

#lg_cust_mcp_plan_demog_d_distcpy_checker >> repair_lg_cust_mcp_plan_demog_d >> repair_lg_cust_mcp_plan_demog_d_checker