
import airflow
from airflow.models import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import BranchPythonOperator

from airflow.utils.dates import days_ago
from airflow.models import Variable

from lgutils.utils import lgfunctions
from airflow.contrib.sensors.emr_step_sensor import EmrStepSensor
from airflow.contrib.operators.emr_add_steps_operator import EmrAddStepsOperator


DEFAULT_ARGS = {
    'owner': 'airflow',
    'depends_on_past': False
}

skipPreviousSteps = Variable.get("lg_ingestion_skip_processed")
partitionDate = Variable.get("lg_load_date")

# emrX = boto3.client('emr', region_name='us-east-1')
clusterId = 'j-55BV0X84633R'#Variable.get("lg_ingestion_cluster_id")



DEFAULT_ARGS = {
    'owner': 'airflow',
    'depends_on_past': False
}

dag = DAG(
    'wf_recreate_and_repair pivot tables',
    default_args=DEFAULT_ARGS,
    schedule_interval='@once',
    start_date=days_ago(1),
    catchup=False
)

#Create LG Utility funtions object
lg = lgfunctions(clusterId, partitionDate, skipPreviousSteps, dag)

pivot_hql = lg.createHsqlStep(table='pivot_hql', hsql_file='s3://az-us-lg-pilot-config/oozie-migration/hsql/pivot.hql')
pivot_hql_checker = lg.createWatchStep(table='pivot_hql', hsql_id='pivot_hql')


pivot_hql >> pivot_hql_checker
