from datetime import timedelta, datetime
 
from airflow import DAG
from airflow.contrib.operators.emr_create_job_flow_operator import EmrCreateJobFlowOperator
from airflow.contrib.operators.emr_add_steps_operator import EmrAddStepsOperator
from airflow.contrib.sensors.emr_step_sensor import EmrStepSensor
from airflow.contrib.operators.emr_terminate_job_flow_operator import EmrTerminateJobFlowOperator
 
DEFAULT_ARGS = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2019, 5, 29),
    'email': ['subramanya.kalidasu@astrazeneca.com'],
    'email_on_failure': True,
    'email_on_retry': False
}
 
 
SPARK_TEST_STEPS = [
    {
        'Name': 'Test1001',
        'ActionOnFailure': 'CONTINUE',
        'HadoopJarStep': {
            'Jar': 'command-runner.jar',
            'Args': [
                'spark-submit',
                '--deploy-mode',
                'cluster',
                '--files',
                './test.sh',
            ]
        }
    }
]

dag = DAG(
    'Test1001',
    default_args=DEFAULT_ARGS,
    dagrun_timeout=timedelta(hours=2),
    schedule_interval='0 3 * * *'
)
 
step_adder = EmrAddStepsOperator(
    task_id='add_steps',
    job_flow_id="j-39H6ITUFC6OKK",
    aws_conn_id='aws_default',
    steps=SPARK_TEST_STEPS,
    dag=dag
)
 
step_checker = EmrStepSensor(
    task_id='watch_step',
    job_flow_id="j-39H6ITUFC6OKK",
    step_id="{{ task_instance.xcom_pull('add_steps', key='return_value')[0] }}",
    aws_conn_id='aws_default',
    dag=dag
)
 
 
step_adder.set_downstream(step_checker)
