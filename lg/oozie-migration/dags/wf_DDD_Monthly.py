from datetime import timedelta

import airflow
from airflow.models import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.operators.emr_add_steps_operator import EmrAddStepsOperator
from airflow.contrib.sensors.emr_step_sensor import EmrStepSensor

from airflow.utils.dates import days_ago

DEFAULT_ARGS = {
    'owner': 'airflow',
    'depends_on_past': False
}

lg_sls_ddd_olet_mthly_f_step = [
    {
        'Name': 'StepExecution',
        'ActionOnFailure': 'CONTINUE',
        'HadoopJarStep': {
            'Jar': 'command-runner.jar',
            'Args': [
                '/usr/lib/spark/bin/spark-submit',
                '--master',
                'yarn',
                '--deploy-mode',
                'cluster',
                '--executor-memory',
                '5G',
                '--driver-memory',
                '3G',
                's3://az-us-lg-pilot-config/oozie-migration/source_code/StepExecutor.py',
                '--metadata-file-name',
                'lg_sls_ddd_olet_mthly_f.txt'
            ]
        }
    }
]

lg_sls_ddd_zip_mthly_f_step = [
    {
        'Name': 'StepExecution',
        'ActionOnFailure': 'CONTINUE',
        'HadoopJarStep': {
            'Jar': 'command-runner.jar',
            'Args': [
                '/usr/lib/spark/bin/spark-submit',
                '--master',
                'yarn',
                '--deploy-mode',
                'cluster',
                '--executor-memory',
                '5G',
                '--driver-memory',
                '3G',
                's3://az-us-lg-pilot-config/oozie-migration/source_code/StepExecutor.py',
                '--metadata-file-name',
                'lg_sls_ddd_zip_mthly_f.txt'
            ]
        }
    }
]

lg_sls_dddmd_olet_mthly_f_step = [
    {
        'Name': 'StepExecution',
        'ActionOnFailure': 'CONTINUE',
        'HadoopJarStep': {
            'Jar': 'command-runner.jar',
            'Args': [
                '/usr/lib/spark/bin/spark-submit',
                '--master',
                'yarn',
                '--deploy-mode',
                'cluster',
                '--executor-memory',
                '5G',
                '--driver-memory',
                '3G',
                's3://az-us-lg-pilot-config/oozie-migration/source_code/StepExecutor.py',
                '--metadata-file-name',
                'lg_sls_dddmd_olet_mthly_f.txt'
            ]
        }
    }
]


dag = DAG(
    'wf_DDD_Monthly',
    default_args=DEFAULT_ARGS,
    schedule_interval='@once',
    start_date=days_ago(1)
)


lg_sls_ddd_olet_mthly_f = EmrAddStepsOperator(
    task_id='lg_sls_ddd_olet_mthly_f_tsk',
    job_flow_id="j-17TOATNJNS4YZ",
    aws_conn_id='lg_emr_ingestion',
    steps=lg_sls_ddd_olet_mthly_f_step,
    dag=dag
)

lg_sls_ddd_olet_mthly_f_step_checker = EmrStepSensor(
    task_id='lg_sls_ddd_olet_mthly_f_watch_step_tsk',
    job_flow_id="j-17TOATNJNS4YZ",
    step_id="{{ task_instance.xcom_pull('lg_sls_ddd_olet_mthly_f_tsk', key='return_value')[0] }}",
    aws_conn_id='lg_emr_ingestion',
    dag=dag
)

lg_sls_ddd_zip_mthly_f = EmrAddStepsOperator(
    task_id='lg_sls_ddd_zip_mthly_f_tsk',
    job_flow_id="j-17TOATNJNS4YZ",
    aws_conn_id='lg_emr_ingestion',
    steps=lg_sls_ddd_zip_mthly_f_step,
    dag=dag
)

lg_sls_ddd_zip_mthly_f_watch_step_checker = EmrStepSensor(
    task_id='lg_sls_ddd_zip_mthly_f_watch_step_tsk',
    job_flow_id="j-17TOATNJNS4YZ",
    step_id="{{ task_instance.xcom_pull('lg_sls_ddd_zip_mthly_f_tsk', key='return_value')[0] }}",
    aws_conn_id='lg_emr_ingestion',
    dag=dag
)

lg_sls_dddmd_olet_mthly_f = EmrAddStepsOperator(
    task_id='lg_sls_dddmd_olet_mthly_f_tsk',
    job_flow_id="j-17TOATNJNS4YZ",
    aws_conn_id='lg_emr_ingestion',
    steps=lg_sls_dddmd_olet_mthly_f_step,
    dag=dag
)

lg_sls_dddmd_olet_mthly_f_watch_step_checker = EmrStepSensor(
    task_id='lg_sls_dddmd_olet_mthly_f_watch_step_tsk',
    job_flow_id="j-17TOATNJNS4YZ",
    step_id="{{ task_instance.xcom_pull('lg_sls_dddmd_olet_mthly_f_tsk', key='return_value')[0] }}",
    aws_conn_id='lg_emr_ingestion',
    dag=dag
)



op = DummyOperator(task_id='wf_DDD_Monthly_dummy', dag=dag)

op.set_downstream(lg_sls_ddd_olet_mthly_f)
lg_sls_ddd_olet_mthly_f.set_downstream(lg_sls_ddd_olet_mthly_f_step_checker)

lg_sls_ddd_olet_mthly_f_step_checker.set_downstream(lg_sls_ddd_zip_mthly_f)
lg_sls_ddd_zip_mthly_f.set_downstream(lg_sls_ddd_zip_mthly_f_watch_step_checker)

lg_sls_ddd_zip_mthly_f_watch_step_checker.set_downstream(lg_sls_dddmd_olet_mthly_f)
lg_sls_dddmd_olet_mthly_f.set_downstream(lg_sls_dddmd_olet_mthly_f_watch_step_checker)

