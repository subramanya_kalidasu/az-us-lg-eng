from datetime import datetime, timedelta
from airflow.models import DAG
from airflow.operators.subdag_operator import SubDagOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.dummy_operator import DummyOperator

from lgutils.utils import lgfunctions

from wf_Sample_Central import wf_Sample_Central
from wf_dev_Purush_lg_cust_hcp_addr_d import wf_dev_Purush_lg_cust_hcp_addr_d
from wf_DDD_Weekly import wf_DDD_Weekly
from wf_lg_promo_acty_atnd_lkp import wf_lg_promo_acty_atnd_lkp
from wf_lg_cust_mcp_rel_d import wf_lg_cust_mcp_rel_d
from wf_lg_hci_blocklist import wf_lg_hci_blocklist
from wf_lg_tgt_plan_lmr_f import wf_lg_tgt_plan_lmr_f
from wf_MCM_PSKW import wf_MCM_PSKW
from wf_lg_curr_algn_d import wf_lg_curr_algn_d
from wf_MCM_Nielsen import wf_MCM_Nielsen
from wf_lg_cust_hca_addr_d import wf_lg_cust_hca_addr_d
from wf_lg_acct_hcp_tgt_f import wf_lg_acct_hcp_tgt_f
from wf_MCM_Abode import wf_MCM_Abode
from wf_lg_formulary_restrictions_f import wf_lg_formulary_restrictions_f
from wf_XPO_Weekly import wf_XPO_Weekly
from wf_MCM_Publicis import wf_MCM_Publicis
from wf_RAC_Alignment import wf_RAC_Alignment
from wf_lg_hcp_blocklist import wf_lg_hcp_blocklist
from wf_DDD_Monthly import wf_DDD_Monthly
from wf_Payer_Bridge import wf_Payer_Bridge
from wf_RAC_Segmentation import wf_RAC_Segmentation
from wf_lg_cust_mcp_payer_addr_d import wf_lg_cust_mcp_payer_addr_d
from wf_lg_cust_mcp_pbm_demog_d import wf_lg_cust_mcp_pbm_demog_d
from wf_lg_cust_hca_affil_prnt_d import wf_lg_cust_hca_affil_prnt_d
from wf_lg_consumer_rebates_f import wf_lg_consumer_rebates_f
from wf_dev_Purush_lg_cust_merged_xref import wf_dev_Purush_lg_cust_merged_xref
from wf_lg_promo_acty_atnd_f import wf_lg_promo_acty_atnd_f
from wf_lg_cust_mcp_payer_demog_d import wf_lg_cust_mcp_payer_demog_d
from wf_lg_cust_mcp_other_demog_d import wf_lg_cust_mcp_other_demog_d
from wf_Speaker_Program import wf_Speaker_Program
from wf_RAC_Person import wf_RAC_Person
from wf_lg_cust_hca_hca_affil_prnt_d import wf_lg_cust_hca_hca_affil_prnt_d
from wf_lg_cust_mcp_plan_addr_d import wf_lg_cust_mcp_plan_addr_d
from wf_lg_cust_hcp_prof_typ_d import wf_lg_cust_hcp_prof_typ_d
from wf_lg_cust_hcp_title_d import wf_lg_cust_hcp_title_d
from wf_lg_promo_acty_f import wf_lg_promo_acty_f
from wf_lg_cust_mcp_plan_demog_d import wf_lg_cust_mcp_plan_demog_d
from wf_MCM_MCRM import wf_MCM_MCRM
from wf_RAC_Targeting import wf_RAC_Targeting
from wf_lg_cust_hca_affil_child_d import wf_lg_cust_hca_affil_child_d
from wf_lg_ddd_hcos_olet_f import wf_lg_ddd_hcos_olet_f
from wf_lg_persn_d import wf_lg_persn_d
from wf_Activity import wf_Activity
from wf_lg_full_plan_bridge_acct_frmly_bmtrx_ import wf_lg_full_plan_bridge_acct_frmly_bmtrx_
from wf_dev_Purush_lg_cust_hcp_id_d import wf_dev_Purush_lg_cust_hcp_id_d
from wf_Concur import wf_Concur
from wf_lg_dddmd_olet_hcp_f import wf_lg_dddmd_olet_hcp_f
from wf_lg_cust_hca_hca_affil_child_d import wf_lg_cust_hca_hca_affil_child_d
from wf_lg_cust_hca_demog_d import wf_lg_cust_hca_demog_d
from wf_lg_cust_hcp_spec_d import wf_lg_cust_hcp_spec_d
from wf_dev_Purush_lg_cust_hcp_demog_d import wf_dev_Purush_lg_cust_hcp_demog_d
from wf_lg_cust_hcp_spec_typ_d import wf_lg_cust_hcp_spec_typ_d
from wf_lg_hcp_milestone import wf_lg_hcp_milestone
from wf_lg_cust_mcp_pbm_addr_d import wf_lg_cust_mcp_pbm_addr_d
from wf_MDM import wf_MDM
from wf_lg_cust_hca_id_d import wf_lg_cust_hca_id_d
from wf_lg_promo_acty_info_lkp import wf_lg_promo_acty_info_lkp
from wf_lg_product_ref_t import wf_lg_product_ref_t
from wf_XPO_Monthly import wf_XPO_Monthly




DEFAULT_ARGS = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date':datetime.now()
}

main_dag = DAG(
  dag_id='wf_lg_generated_master',
  concurrency=12,
  max_active_runs=1,
  default_args=DEFAULT_ARGS,
  start_date=datetime.now(),
  catchup=False
)

end = DummyOperator(
    task_id='job_completion',
    dag=main_dag,
)

sub_dag1 = SubDagOperator(
    subdag=wf_Sample_Central(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_Sample_Central'),
    task_id = 'wf_Sample_Central',
    dag=main_dag
)

sub_dag2 = SubDagOperator(
    subdag=wf_dev_Purush_lg_cust_hcp_addr_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_dev_Purush_lg_cust_hcp_addr_d'),
    task_id = 'wf_dev_Purush_lg_cust_hcp_addr_d',
    dag=main_dag
)

sub_dag3 = SubDagOperator(
    subdag=wf_DDD_Weekly(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_DDD_Weekly'),
    task_id = 'wf_DDD_Weekly',
    dag=main_dag
)

sub_dag4 = SubDagOperator(
    subdag=wf_lg_promo_acty_atnd_lkp(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_promo_acty_atnd_lkp'),
    task_id = 'wf_lg_promo_acty_atnd_lkp',
    dag=main_dag
)

sub_dag5 = SubDagOperator(
    subdag=wf_lg_cust_mcp_rel_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_cust_mcp_rel_d'),
    task_id = 'wf_lg_cust_mcp_rel_d',
    dag=main_dag
)

sub_dag6 = SubDagOperator(
    subdag=wf_lg_hci_blocklist(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_hci_blocklist'),
    task_id = 'wf_lg_hci_blocklist',
    dag=main_dag
)

sub_dag7 = SubDagOperator(
    subdag=wf_lg_tgt_plan_lmr_f(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_tgt_plan_lmr_f'),
    task_id = 'wf_lg_tgt_plan_lmr_f',
    dag=main_dag
)

sub_dag8 = SubDagOperator(
    subdag=wf_MCM_PSKW(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_MCM_PSKW'),
    task_id = 'wf_MCM_PSKW',
    dag=main_dag
)

sub_dag9 = SubDagOperator(
    subdag=wf_lg_curr_algn_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_curr_algn_d'),
    task_id = 'wf_lg_curr_algn_d',
    dag=main_dag
)

sub_dag10 = SubDagOperator(
    subdag=wf_MCM_Nielsen(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_MCM_Nielsen'),
    task_id = 'wf_MCM_Nielsen',
    dag=main_dag
)

sub_dag11 = SubDagOperator(
    subdag=wf_lg_cust_hca_addr_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_cust_hca_addr_d'),
    task_id = 'wf_lg_cust_hca_addr_d',
    dag=main_dag
)

sub_dag12 = SubDagOperator(
    subdag=wf_lg_acct_hcp_tgt_f(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_acct_hcp_tgt_f'),
    task_id = 'wf_lg_acct_hcp_tgt_f',
    dag=main_dag
)

sub_dag13 = SubDagOperator(
    subdag=wf_MCM_Abode(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_MCM_Abode'),
    task_id = 'wf_MCM_Abode',
    dag=main_dag
)

sub_dag14 = SubDagOperator(
    subdag=wf_lg_formulary_restrictions_f(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_formulary_restrictions_f'),
    task_id = 'wf_lg_formulary_restrictions_f',
    dag=main_dag
)

sub_dag15 = SubDagOperator(
    subdag=wf_XPO_Weekly(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_XPO_Weekly'),
    task_id = 'wf_XPO_Weekly',
    dag=main_dag
)

sub_dag16 = SubDagOperator(
    subdag=wf_MCM_Publicis(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_MCM_Publicis'),
    task_id = 'wf_MCM_Publicis',
    dag=main_dag
)

sub_dag17 = SubDagOperator(
    subdag=wf_RAC_Alignment(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_RAC_Alignment'),
    task_id = 'wf_RAC_Alignment',
    dag=main_dag
)

sub_dag18 = SubDagOperator(
    subdag=wf_lg_hcp_blocklist(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_hcp_blocklist'),
    task_id = 'wf_lg_hcp_blocklist',
    dag=main_dag
)

sub_dag19 = SubDagOperator(
    subdag=wf_DDD_Monthly(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_DDD_Monthly'),
    task_id = 'wf_DDD_Monthly',
    dag=main_dag
)

sub_dag20 = SubDagOperator(
    subdag=wf_Payer_Bridge(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_Payer_Bridge'),
    task_id = 'wf_Payer_Bridge',
    dag=main_dag
)

sub_dag21 = SubDagOperator(
    subdag=wf_RAC_Segmentation(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_RAC_Segmentation'),
    task_id = 'wf_RAC_Segmentation',
    dag=main_dag
)

sub_dag22 = SubDagOperator(
    subdag=wf_lg_cust_mcp_payer_addr_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_cust_mcp_payer_addr_d'),
    task_id = 'wf_lg_cust_mcp_payer_addr_d',
    dag=main_dag
)

sub_dag23 = SubDagOperator(
    subdag=wf_lg_cust_mcp_pbm_demog_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_cust_mcp_pbm_demog_d'),
    task_id = 'wf_lg_cust_mcp_pbm_demog_d',
    dag=main_dag
)

sub_dag24 = SubDagOperator(
    subdag=wf_lg_cust_hca_affil_prnt_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_cust_hca_affil_prnt_d'),
    task_id = 'wf_lg_cust_hca_affil_prnt_d',
    dag=main_dag
)

sub_dag25 = SubDagOperator(
    subdag=wf_lg_consumer_rebates_f(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_consumer_rebates_f'),
    task_id = 'wf_lg_consumer_rebates_f',
    dag=main_dag
)

sub_dag26 = SubDagOperator(
    subdag=wf_dev_Purush_lg_cust_merged_xref(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_dev_Purush_lg_cust_merged_xref'),
    task_id = 'wf_dev_Purush_lg_cust_merged_xref',
    dag=main_dag
)

sub_dag27 = SubDagOperator(
    subdag=wf_lg_promo_acty_atnd_f(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_promo_acty_atnd_f'),
    task_id = 'wf_lg_promo_acty_atnd_f',
    dag=main_dag
)

sub_dag28 = SubDagOperator(
    subdag=wf_lg_cust_mcp_payer_demog_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_cust_mcp_payer_demog_d'),
    task_id = 'wf_lg_cust_mcp_payer_demog_d',
    dag=main_dag
)

sub_dag29 = SubDagOperator(
    subdag=wf_lg_cust_mcp_other_demog_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_cust_mcp_other_demog_d'),
    task_id = 'wf_lg_cust_mcp_other_demog_d',
    dag=main_dag
)

sub_dag30 = SubDagOperator(
    subdag=wf_Speaker_Program(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_Speaker_Program'),
    task_id = 'wf_Speaker_Program',
    dag=main_dag
)

sub_dag31 = SubDagOperator(
    subdag=wf_RAC_Person(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_RAC_Person'),
    task_id = 'wf_RAC_Person',
    dag=main_dag
)

sub_dag32 = SubDagOperator(
    subdag=wf_lg_cust_hca_hca_affil_prnt_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_cust_hca_hca_affil_prnt_d'),
    task_id = 'wf_lg_cust_hca_hca_affil_prnt_d',
    dag=main_dag
)

sub_dag33 = SubDagOperator(
    subdag=wf_lg_cust_mcp_plan_addr_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_cust_mcp_plan_addr_d'),
    task_id = 'wf_lg_cust_mcp_plan_addr_d',
    dag=main_dag
)

sub_dag34 = SubDagOperator(
    subdag=wf_lg_cust_hcp_prof_typ_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_cust_hcp_prof_typ_d'),
    task_id = 'wf_lg_cust_hcp_prof_typ_d',
    dag=main_dag
)

sub_dag35 = SubDagOperator(
    subdag=wf_lg_cust_hcp_title_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_cust_hcp_title_d'),
    task_id = 'wf_lg_cust_hcp_title_d',
    dag=main_dag
)

sub_dag36 = SubDagOperator(
    subdag=wf_lg_promo_acty_f(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_promo_acty_f'),
    task_id = 'wf_lg_promo_acty_f',
    dag=main_dag
)

sub_dag37 = SubDagOperator(
    subdag=wf_lg_cust_mcp_plan_demog_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_cust_mcp_plan_demog_d'),
    task_id = 'wf_lg_cust_mcp_plan_demog_d',
    dag=main_dag
)

sub_dag38 = SubDagOperator(
    subdag=wf_MCM_MCRM(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_MCM_MCRM'),
    task_id = 'wf_MCM_MCRM',
    dag=main_dag
)

sub_dag39 = SubDagOperator(
    subdag=wf_RAC_Targeting(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_RAC_Targeting'),
    task_id = 'wf_RAC_Targeting',
    dag=main_dag
)

sub_dag40 = SubDagOperator(
    subdag=wf_lg_cust_hca_affil_child_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_cust_hca_affil_child_d'),
    task_id = 'wf_lg_cust_hca_affil_child_d',
    dag=main_dag
)

sub_dag41 = SubDagOperator(
    subdag=wf_lg_ddd_hcos_olet_f(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_ddd_hcos_olet_f'),
    task_id = 'wf_lg_ddd_hcos_olet_f',
    dag=main_dag
)

sub_dag42 = SubDagOperator(
    subdag=wf_lg_persn_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_persn_d'),
    task_id = 'wf_lg_persn_d',
    dag=main_dag
)

sub_dag43 = SubDagOperator(
    subdag=wf_Activity(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_Activity'),
    task_id = 'wf_Activity',
    dag=main_dag
)

sub_dag44 = SubDagOperator(
    subdag=wf_lg_full_plan_bridge_acct_frmly_bmtrx_(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_full_plan_bridge_acct_frmly_bmtrx_'),
    task_id = 'wf_lg_full_plan_bridge_acct_frmly_bmtrx_',
    dag=main_dag
)

sub_dag45 = SubDagOperator(
    subdag=wf_dev_Purush_lg_cust_hcp_id_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_dev_Purush_lg_cust_hcp_id_d'),
    task_id = 'wf_dev_Purush_lg_cust_hcp_id_d',
    dag=main_dag
)

sub_dag46 = SubDagOperator(
    subdag=wf_Concur(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_Concur'),
    task_id = 'wf_Concur',
    dag=main_dag
)

sub_dag47 = SubDagOperator(
    subdag=wf_lg_dddmd_olet_hcp_f(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_dddmd_olet_hcp_f'),
    task_id = 'wf_lg_dddmd_olet_hcp_f',
    dag=main_dag
)

sub_dag48 = SubDagOperator(
    subdag=wf_lg_cust_hca_hca_affil_child_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_cust_hca_hca_affil_child_d'),
    task_id = 'wf_lg_cust_hca_hca_affil_child_d',
    dag=main_dag
)

sub_dag49 = SubDagOperator(
    subdag=wf_lg_cust_hca_demog_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_cust_hca_demog_d'),
    task_id = 'wf_lg_cust_hca_demog_d',
    dag=main_dag
)

sub_dag50 = SubDagOperator(
    subdag=wf_lg_cust_hcp_spec_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_cust_hcp_spec_d'),
    task_id = 'wf_lg_cust_hcp_spec_d',
    dag=main_dag
)

sub_dag51 = SubDagOperator(
    subdag=wf_dev_Purush_lg_cust_hcp_demog_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_dev_Purush_lg_cust_hcp_demog_d'),
    task_id = 'wf_dev_Purush_lg_cust_hcp_demog_d',
    dag=main_dag
)

sub_dag52 = SubDagOperator(
    subdag=wf_lg_cust_hcp_spec_typ_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_cust_hcp_spec_typ_d'),
    task_id = 'wf_lg_cust_hcp_spec_typ_d',
    dag=main_dag
)

sub_dag53 = SubDagOperator(
    subdag=wf_lg_hcp_milestone(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_hcp_milestone'),
    task_id = 'wf_lg_hcp_milestone',
    dag=main_dag
)

sub_dag54 = SubDagOperator(
    subdag=wf_lg_cust_mcp_pbm_addr_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_cust_mcp_pbm_addr_d'),
    task_id = 'wf_lg_cust_mcp_pbm_addr_d',
    dag=main_dag
)

sub_dag55 = SubDagOperator(
    subdag=wf_MDM(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_MDM'),
    task_id = 'wf_MDM',
    dag=main_dag
)

sub_dag56 = SubDagOperator(
    subdag=wf_lg_cust_hca_id_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_cust_hca_id_d'),
    task_id = 'wf_lg_cust_hca_id_d',
    dag=main_dag
)

sub_dag57 = SubDagOperator(
    subdag=wf_lg_promo_acty_info_lkp(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_promo_acty_info_lkp'),
    task_id = 'wf_lg_promo_acty_info_lkp',
    dag=main_dag
)

sub_dag58 = SubDagOperator(
    subdag=wf_lg_product_ref_t(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_lg_product_ref_t'),
    task_id = 'wf_lg_product_ref_t',
    dag=main_dag
)

sub_dag59 = SubDagOperator(
    subdag=wf_XPO_Monthly(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='wf_XPO_Monthly'),
    task_id = 'wf_XPO_Monthly',
    dag=main_dag
)
