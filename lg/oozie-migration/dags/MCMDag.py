from datetime import timedelta

import airflow
from airflow.models import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.operators.emr_add_steps_operator import EmrAddStepsOperator
from airflow.contrib.sensors.emr_step_sensor import EmrStepSensor

from airflow.utils.dates import days_ago

DEFAULT_ARGS = {
    'owner': 'airflow',
    'depends_on_past': False
}

lg_prsn_d_step = [
    {
        'Name': 'StepExecution',
        'ActionOnFailure': 'CONTINUE',
        'HadoopJarStep': {
            'Jar': 'command-runner.jar',
            'Args': [
                '/usr/lib/spark/bin/spark-submit',
                '--master',
                'yarn',
                '--deploy-mode',
                'cluster',
                's3://az-us-lg-pilot-config/oozie-migration/source_code/StepExecutor.py',
                '--metadata-file-name',
                'lg_prsn_d.txt'
            ]
        }
    }
]

lg_prsn_addr_d_step = [
    {
        'Name': 'StepExecution',
        'ActionOnFailure': 'CONTINUE',
        'HadoopJarStep': {
            'Jar': 'command-runner.jar',
            'Args': [
                '/usr/lib/spark/bin/spark-submit',
                '--master',
                'yarn',
                '--deploy-mode',
                'cluster',
                's3://az-us-lg-pilot-config/oozie-migration/source_code/StepExecutor.py',
                '--metadata-file-name',
                'lg_prsn_addr_d.txt'
            ]
        }
    }
]

dag = DAG(
    'LG_StepExecution',
    default_args=DEFAULT_ARGS,
    schedule_interval='@once',
    start_date=days_ago(1)
)


lg_prsn_d = EmrAddStepsOperator(
    task_id='lg_prsn_d_tsk',
    job_flow_id="j-17TOATNJNS4YZ",
    aws_conn_id='lg_emr_ingestion',
    steps=lg_prsn_d_step,
    dag=dag
)

lg_prsn_d_watch_step_checker = EmrStepSensor(
    task_id='lg_prsn_d_watch_step_tsk',
    job_flow_id="j-17TOATNJNS4YZ",
    step_id="{{ task_instance.xcom_pull('lg_prsn_d_tsk', key='return_value')[0] }}",
    aws_conn_id='lg_emr_ingestion',
    dag=dag
)

lg_prsn_addr_d = EmrAddStepsOperator(
    task_id='lg_prsn_addr_d_tsk',
    job_flow_id="j-17TOATNJNS4YZ",
    aws_conn_id='lg_emr_ingestion',
    steps=lg_prsn_addr_d_step,
    dag=dag
)

lg_prsn_addr_d_watch_step_checker = EmrStepSensor(
    task_id='lg_prsn_addr_d_watch_step_tsk',
    job_flow_id="j-17TOATNJNS4YZ",
    step_id="{{ task_instance.xcom_pull('lg_prsn_addr_d_tsk', key='return_value')[0] }}",
    aws_conn_id='lg_emr_ingestion',
    dag=dag
)


op = DummyOperator(task_id='dummy', dag=dag)

op.set_downstream(lg_prsn_d)
lg_prsn_d.set_downstream(lg_prsn_d_watch_step_checker)

lg_prsn_d_watch_step_checker.set_downstream(lg_prsn_addr_d)
lg_prsn_addr_d.set_downstream(lg_prsn_addr_d_watch_step_checker)
