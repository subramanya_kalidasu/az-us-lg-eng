from lgutils.utils import lgfunctions
from airflow.models import DAG
from datetime import datetime


#Initialize lg functions with dag
#optional arguments skip_processed, load_date, task_submit_timeout, task_timeout, run_job_mode, cluster_id
#If no overrides passed, it takes values from Airflow Variables defined on Airflow portal running on 8080 port.
DEFAULT_ARGS = {
    'owner': 'airflow',
    'depends_on_past': False
}

dag = DAG(
    'wf_Sample_Central',
    concurrency=3,
    max_active_runs=1,
    default_args=DEFAULT_ARGS,
    start_date=datetime.now(),
    catchup=False
)

lg = lgfunctions(dag=dag)

#Truncate temp tables before processing
s1 = lg.createlivytablestep(None, table='lg_smpl_prsbr_terr_alloc_f', executor_memory='5g', driver_memory='3g')

s2 = lg.createlivytablestep(None, table='lg_smpl_terr_alloc_f', executor_memory='5g', driver_memory='3g')

s3 = lg.createlivytablestep(None, table='lg_smpl_prsbr_alloc_f', executor_memory='5g', driver_memory='3g')

s4 = lg.createlivytablestep(None, table='lg_smpl_ord_ffevnt_f', executor_memory='5g', driver_memory='3g')

