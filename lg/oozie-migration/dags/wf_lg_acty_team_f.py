from lgutils.utils import lgfunctions
from airflow.models import DAG
from datetime import datetime


#Initialize lg functions with dag
#optional arguments skip_processed, load_date, task_submit_timeout, task_timeout, run_job_mode, cluster_id
#If no overrides passed, it takes values from Airflow Variables defined on Airflow portal running on 8080 port.
DEFAULT_ARGS = {
    'owner': 'airflow',
    'depends_on_past': False
}

dag = DAG(
    'wf_lg_acty_team_f',
    concurrency=3,
    max_active_runs=1,
    default_args=DEFAULT_ARGS,
    start_date=datetime.now(),
    catchup=False
)

lg = lgfunctions(dag=dag)

s1pre = lg.createlivyhsqlstep(None, task_type='hsql', table='lg_acty_team_f', hsql_file='s3://az-us-lg-pilot-config/oozie-migration/hsql/repair_trunc_lg_acty_team_f.hql')

#Process table ETL
s1 = lg.createlivytablestep(s1pre, table='lg_acty_team_f', executor_memory='5g', driver_memory='3g', spark_default_parallelism='150')

#Run distributed copy to copy data from HDFS to S3
s2 = lg.createlivycreatedstcpstep(s1, task_type='distcp', table='lg_acty_team_f', source='/user/azhueadmin/lg_acty_team_f_tmp')

s1post = lg.createlivyhsqlstep(s2, task_type='hsql', table='lg_acty_team_f', hsql_file='s3://az-us-lg-pilot-config/oozie-migration/hsql/repair_trunc_lg_acty_team_f.hql')
