from airflow.models import DAG

from airflow.models import Variable
from airflow.operators.python_operator import PythonOperator
from airflow.hooks.base_hook import BaseHook
from airflow.sensors.sql_sensor import SqlSensor
from botocore.exceptions import ClientError
from airflow.hooks.mysql_hook import MySqlHook

import boto3
from datetime import datetime


DEFAULT_ARGS = {
    'owner': 'airflow',
    'depends_on_past': False
}

dag = DAG(
    'botoCoreTestDag',
    concurrency=3,
    max_active_runs=1,
    default_args=DEFAULT_ARGS,
    start_date=datetime.now(),
    catchup=False
)

def botoCheck(**kwargs):
    connection = BaseHook.get_connection("lg_emr_ingestion")
    key = connection.login
    secretkey = connection.password
    print('keys {}{}'.format(key, secretkey))
    emrX = boto3.client('emr', aws_access_key_id=key, aws_secret_access_key=secretkey, region_name='us-east-1')

    instances = emrX.list_instances(ClusterId=Variable.get("lg_cluster_id"), InstanceGroupTypes=['MASTER'])
    master_ip = instances['Instances'][0]['PrivateIpAddress']

    print('Master IP {}'.format(master_ip))

    connection = BaseHook.get_connection("lg_s3_connection")
    key = connection.login
    secretkey = connection.password
    print('keys {}{}'.format(key, secretkey))

    s3client = boto3.client('s3', aws_access_key_id=key, aws_secret_access_key=secretkey)
    print(s3client)

    try:
        r = s3client.head_object(Bucket='az-us-lg-pilot-config',
                                 Key='etl/audit/hsql/dag_livyTestDag_task_repair_lg_cust_mcp_plan_demog_d_loaddate_2019-06-10_SUCCESS')
        if r['ResponseMetadata']['HTTPStatusCode'] == 200:
            print('Job Tracker File found {}. Will continue the job'.format('s3://az-us-lg-pilot-config/etl/audit/hsql/dag_livyTestDag_task_repair_lg_cust_mcp_plan_demog_d_loaddate_2019-06-10_SUCCESS'))

    except ClientError as e:
        print(e)
        #raise e

    #kwargs['ti'].xcom_push(key='RUN_ID', value='Test-123')
    return 'Test-123'

tableStep = PythonOperator(
    task_id='BotCheckTsk',
    provide_context=True,
    python_callable=botoCheck,
    dag=dag)

def sqlsensor(**context):
    print('Tracking RUN_ID {}')

    connection = MySqlHook(mysql_conn_id='lg_job_tracker')
    run_id = context['task_instance'].xcom_pull(task_ids='BotCheckTsk')
    #query = "SELECT STATUS from JOB_AUDIT where RUN_ID= '" + run_id + "'"
    query = "SELECT STATUS from JOB_AUDIT where RUN_ID=%(run_id)s"
    print('Running query for Run_id {}'.format(run_id))

    row = connection.get_first(query, parameters={'run_id':run_id})
    print('Value for DBhook {})')
    if len(row) > 0:
        print(type(row))
        print(row[0])
    else:
        print('Job failed. No Job Tracker record')

    return True
    #
    # SqlSensor(
    #     task_id='hdfs_sensor_check',
    #     conn_id='lg_job_tracker',
    #     sql="SELECT STATUS FROM lgetl.JOB_AUDIT where RUN_ID='" + context['task_instance'].xcom_pull(
    #                                                                                   task_ids='BotCheckTsk') + "'",
    #     poke_interval=60,  # (seconds); checking file every minute
    #     timeout=300,
    #     dag=dag)


temp = PythonOperator(
    task_id='sqlsensor',
    provide_context=True,
    python_callable=sqlsensor,
    dag=dag)


tableStep >> temp