
import airflow
from airflow.models import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import BranchPythonOperator

from airflow.utils.dates import days_ago
from airflow.models import Variable

from lgutils.utils import lgfunctions
from airflow.contrib.sensors.emr_step_sensor import EmrStepSensor
from airflow.contrib.operators.emr_add_steps_operator import EmrAddStepsOperator


DEFAULT_ARGS = {
    'owner': 'airflow',
    'depends_on_past': False
}

skipPreviousSteps = Variable.get("lg_ingestion_skip_processed")
partitionDate = Variable.get("lg_load_date")

# emrX = boto3.client('emr', region_name='us-east-1')
clusterId = 'j-55BV0X84633R'#Variable.get("lg_ingestion_cluster_id")



DEFAULT_ARGS = {
    'owner': 'airflow',
    'depends_on_past': False
}

dag = DAG(
    'wf_lg_full_plan_bridge_acct_frmly_bmtrx_norm',
    default_args=DEFAULT_ARGS,
    schedule_interval='@once',
    start_date=days_ago(1),
    catchup=False
)

#Create LG Utility funtions object
lg = lgfunctions(clusterId, partitionDate, skipPreviousSteps, dag)


lg_full_plan_bridge_acct_frmly_bmtrx_metadata = lg.createTableStep(table='lg_full_plan_bridge_acct_frmly_bmtrx_metadata', executor_memory='5GB', driver_memory='3GB', spark_default_parallelism='150')
lg_full_plan_bridge_acct_frmly_bmtrx_metadata_step_checker = lg.createWatchStep(table='lg_full_plan_bridge_acct_frmly_bmtrx_metadata')

lg_plan_bridge_acct_frmly_bmtrx_norm_metadata = lg.createTableStep(table='lg_plan_bridge_acct_frmly_bmtrx_norm_metadata', executor_memory='5GB', driver_memory='3GB', spark_default_parallelism='150')
lg_plan_bridge_acct_frmly_bmtrx_norm_metadata_step_checker = lg.createWatchStep(table='lg_plan_bridge_acct_frmly_bmtrx_norm_metadata')

lg_plan_bridge_acct_frmly_bmtrx_norm_dstcp = lg.createDstCpStep(table='lg_plan_bridge_acct_frmly_bmtrx_norm', source='/user/azhueadmin/lg_plan_bridge_acct_frmly_bmtrx_norm_tmp/*')
lg_plan_bridge_acct_frmly_bmtrx_norm_dstcp_checker = lg.createWatchStep(table='lg_plan_bridge_acct_frmly_bmtrx_norm', dist_cp_id='lg_plan_bridge_acct_frmly_bmtrx_norm')



