from datetime import timedelta, datetime, date

from airflow.models import DAG
from airflow.operators.subdag_operator import SubDagOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.models import Variable
from airflow import AirflowException
import json
import requests
import uuid
import time
from airflow.hooks.mysql_hook import MySqlHook
from airflow.operators.bash_operator import BashOperator
import boto3
from botocore.exceptions import WaiterError

from lg_wkly_wf_Sample_Central import lg_wkly_wf_Sample_Central
from lg_wkly_wf_dev_Purush_lg_cust_hcp_addr_d import lg_wkly_wf_dev_Purush_lg_cust_hcp_addr_d
from lg_wkly_wf_DDD_Weekly import lg_wkly_wf_DDD_Weekly
from lg_wkly_wf_lg_promo_acty_atnd_lkp import lg_wkly_wf_lg_promo_acty_atnd_lkp
from lg_wkly_wf_lg_cust_mcp_rel_d import lg_wkly_wf_lg_cust_mcp_rel_d
from lg_wkly_wf_lg_hci_blocklist import lg_wkly_wf_lg_hci_blocklist
from lg_wkly_wf_lg_tgt_plan_lmr_f import lg_wkly_wf_lg_tgt_plan_lmr_f
from lg_wkly_wf_MCM_PSKW import lg_wkly_wf_MCM_PSKW
from lg_wkly_wf_lg_curr_algn_d import lg_wkly_wf_lg_curr_algn_d
from lg_wkly_wf_MCM_Nielsen import lg_wkly_wf_MCM_Nielsen
from lg_wkly_wf_lg_cust_hca_addr_d import lg_wkly_wf_lg_cust_hca_addr_d
from lg_wkly_wf_lg_acct_hcp_tgt_f import lg_wkly_wf_lg_acct_hcp_tgt_f
from lg_wkly_wf_MCM_Abode import lg_wkly_wf_MCM_Abode
from lg_wkly_wf_lg_formulary_restrictions_f import lg_wkly_wf_lg_formulary_restrictions_f
from lg_wkly_wf_XPO_Weekly import lg_wkly_wf_XPO_Weekly
from lg_wkly_wf_MCM_Publicis import lg_wkly_wf_MCM_Publicis
from lg_wkly_wf_RAC_Alignment import lg_wkly_wf_RAC_Alignment
from lg_wkly_wf_lg_hcp_blocklist import lg_wkly_wf_lg_hcp_blocklist
from lg_wkly_wf_Payer_Bridge import lg_wkly_wf_Payer_Bridge
from lg_wkly_wf_RAC_Segmentation import lg_wkly_wf_RAC_Segmentation
from lg_wkly_wf_lg_cust_mcp_payer_addr_d import lg_wkly_wf_lg_cust_mcp_payer_addr_d
from lg_wkly_wf_lg_cust_mcp_pbm_demog_d import lg_wkly_wf_lg_cust_mcp_pbm_demog_d
from lg_wkly_wf_lg_cust_hca_affil_prnt_d import lg_wkly_wf_lg_cust_hca_affil_prnt_d
from lg_wkly_wf_lg_consumer_rebates_f import lg_wkly_wf_lg_consumer_rebates_f
from lg_wkly_wf_dev_Purush_lg_cust_merged_xref import lg_wkly_wf_dev_Purush_lg_cust_merged_xref
from lg_wkly_wf_lg_promo_acty_atnd_f import lg_wkly_wf_lg_promo_acty_atnd_f
from lg_wkly_wf_lg_cust_mcp_payer_demog_d import lg_wkly_wf_lg_cust_mcp_payer_demog_d
from lg_wkly_wf_lg_cust_mcp_other_demog_d import lg_wkly_wf_lg_cust_mcp_other_demog_d
from lg_wkly_wf_Speaker_Program import lg_wkly_wf_Speaker_Program
from lg_wkly_wf_RAC_Person import lg_wkly_wf_RAC_Person
from lg_wkly_wf_lg_cust_hca_hca_affil_prnt_d import lg_wkly_wf_lg_cust_hca_hca_affil_prnt_d
from lg_wkly_wf_lg_cust_mcp_plan_addr_d import lg_wkly_wf_lg_cust_mcp_plan_addr_d
from lg_wkly_wf_lg_cust_hcp_prof_typ_d import lg_wkly_wf_lg_cust_hcp_prof_typ_d
from lg_wkly_wf_lg_cust_hcp_title_d import lg_wkly_wf_lg_cust_hcp_title_d
from lg_wkly_wf_lg_promo_acty_f import lg_wkly_wf_lg_promo_acty_f
from lg_wkly_wf_lg_cust_mcp_plan_demog_d import lg_wkly_wf_lg_cust_mcp_plan_demog_d
from lg_wkly_wf_MCM_MCRM import lg_wkly_wf_MCM_MCRM
from lg_wkly_wf_RAC_Targeting import lg_wkly_wf_RAC_Targeting
from lg_wkly_wf_lg_cust_hca_affil_child_d import lg_wkly_wf_lg_cust_hca_affil_child_d
from lg_wkly_wf_lg_ddd_hcos_olet_f import lg_wkly_wf_lg_ddd_hcos_olet_f
from lg_wkly_wf_lg_persn_d import lg_wkly_wf_lg_persn_d
from lg_wkly_wf_Activity import lg_wkly_wf_Activity
from lg_wkly_wf_dev_Purush_lg_cust_hcp_id_d import lg_wkly_wf_dev_Purush_lg_cust_hcp_id_d
from lg_wkly_wf_Concur import lg_wkly_wf_Concur
from lg_wkly_wf_lg_dddmd_olet_hcp_f import lg_wkly_wf_lg_dddmd_olet_hcp_f
from lg_wkly_wf_lg_cust_hca_hca_affil_child_d import lg_wkly_wf_lg_cust_hca_hca_affil_child_d
from lg_wkly_wf_lg_cust_hca_demog_d import lg_wkly_wf_lg_cust_hca_demog_d
from lg_wkly_wf_lg_cust_hcp_spec_d import lg_wkly_wf_lg_cust_hcp_spec_d
from lg_wkly_wf_dev_Purush_lg_cust_hcp_demog_d import lg_wkly_wf_dev_Purush_lg_cust_hcp_demog_d
from lg_wkly_wf_lg_cust_hcp_spec_typ_d import lg_wkly_wf_lg_cust_hcp_spec_typ_d
from lg_wkly_wf_lg_hcp_milestone import lg_wkly_wf_lg_hcp_milestone
from lg_wkly_wf_lg_cust_mcp_pbm_addr_d import lg_wkly_wf_lg_cust_mcp_pbm_addr_d
from lg_wkly_wf_MDM import lg_wkly_wf_MDM
from lg_wkly_wf_lg_cust_hca_id_d import lg_wkly_wf_lg_cust_hca_id_d
from lg_wkly_wf_lg_promo_acty_info_lkp import lg_wkly_wf_lg_promo_acty_info_lkp
from lg_wkly_wf_lg_product_ref_t import lg_wkly_wf_lg_product_ref_t


def buildDefaultArgs():
    args = {}

    try:
        lg_cluster_id = Variable.get("lg_cluster_id")
        args.update(lg_cluster_id= lg_cluster_id)
        print('Using Cluster ID for the Dag# {}'.format(lg_cluster_id))

        lg_host_ip = Variable.get("lg_host_ip")
        args.update(lg_host_ip=lg_host_ip)
        print('Using lg_host_ip for the Dag# {}'.format(lg_host_ip))


        lg_skip_processed = Variable.get("lg_skip_processed")
        args.update(lg_skip_processed=lg_skip_processed)
        print('Using lg_skip_processed for the Dag# {}'.format(lg_skip_processed))

        lg_load_date = Variable.get("lg_load_date")
        args.update(lg_load_date=lg_load_date)
        print('Using lg_load_date for the Dag# {}'.format(lg_load_date))

        lg_task_submit_timeout = Variable.get("lg_task_submit_timeout")
        args.update(lg_task_submit_timeout=lg_task_submit_timeout)
        print('Using lg_task_submit_timeout for the Dag# {}'.format(lg_task_submit_timeout))

        lg_task_timeout = Variable.get("lg_task_timeout")
        args.update(lg_task_timeout=lg_task_timeout)
        print('Using lg_task_timeout for the Dag# {}'.format(lg_task_timeout))

        lg_run_job_mode = Variable.get("lg_run_job_mode")
        args.update(lg_run_job_mode=lg_run_job_mode)
        print('Using lg_run_job_mode for the Dag# {}'.format(lg_run_job_mode))

        args.update(owner='airflow')
        args.update(depends_on_past=False)

        return args
    except Exception as e:
        print(e)
        raise AirflowException('FATAL lgutils# initialization failure')


DEFAULT_ARGS = buildDefaultArgs()

main_dag = DAG(
  dag_id='lg_wkly_wf_master',
  concurrency=30,
  max_active_runs=1,
  default_args=DEFAULT_ARGS,
  start_date=datetime.now(),
  catchup=False
)

def getipfromcluster(emrX, c_id):
    cd = emrX.describe_cluster(ClusterId=c_id)
    print(cd)
    print(cd['Cluster'])
    print(cd['Cluster']['MasterPublicDnsName'])

    ip = cd['Cluster']['MasterPublicDnsName']
    c = ip.find('.ec2.internal')
    master_ip = ip[3: c].replace('-', '.')
    Variable.set('lg_host_ip', master_ip)
    return master_ip

def initialize_cluster():
    #Check if the Cluster already exists before spinning up one.
    emrX = boto3.client('emr', region_name='us-east-1')
    response = emrX.list_clusters(
        CreatedAfter=str(date.today() - timedelta(days=10)) + " 00:00:00",
        ClusterStates=['STARTING', 'BOOTSTRAPPING', 'RUNNING', 'WAITING']
    )

    for r in response['Clusters']:
        if r['Name'] == 'AZ-US-Commercial-LG-Ingestion-EMR':
            c_id = r['Id']
            print('Found an existing Cluster {}. Attempting to use it.'.format(c_id))
            Variable.set('lg_cluster_id', c_id)

            getipfromcluster(emrX, c_id)

            # As Cluster already exists, ensure its readiness before exiting the initialization.
            waiter = emrX.get_waiter('cluster_running')
            try:
                waiter.wait(
                    ClusterId=c_id,
                    WaiterConfig={
                        'Delay': 60,
                        'MaxAttempts': 40
                    }
                )
                return
            except WaiterError as e:
                print('ERROR# Failed to spin up Cluster')
                print(e)
                raise e


    #Cluster doesn't exists, create a new one.
    client = boto3.client('lambda', region_name='us-east-1')
    print(client)
    payload3 = b"""{
      "ClusterName": "AZ-US-Commercial-LG-Ingestion-EMR",
      "EMR_CONFIGBUCKET": "az-us-lg-pilot-config",
      "EMR_CONFIGBUCKET_KEYNAME": "Lambda/Config/AZ-Commercial-LG-Ingestion-EMR.json",
      "EMR_CORE_COUNT":"4",
      "EMR_CORE_TYPE":"r5d.24xlarge",
        "Action":"Create"
    }"""

    response = client.invoke(
        FunctionName="AZ-Commercial-LG-EMRCreate",
        InvocationType='RequestResponse',
        Payload=payload3
    )

    cluster_id = response['Payload'].read().decode().replace('\"','').replace('\\','')
    Variable.set('lg_cluster_id', cluster_id)
    print('Using Cluster ID for the Dag# {}'.format(cluster_id))


    waiter = emrX.get_waiter('cluster_running')

    try:
        waiter.wait(
            ClusterId=cluster_id,
            WaiterConfig={
                'Delay': 60,
                'MaxAttempts': 40
            }
        )

    except WaiterError as e:
        print('ERROR# Failed to spin up Cluster')
        print(e)
        raise e

    getipfromcluster(emrX, cluster_id)

clusterCreate = PythonOperator(
    task_id='cluster_create_tsk',
    python_callable=initialize_cluster,
    dag=main_dag)

DEFAULT_ARGS = buildDefaultArgs()



def clustersetupwatch(**context):
    print(context)
    task_nm = context['templates_dict']['run-id']
    run_id = task_nm
    print('Waiting on job completion. Tracking RUN_ID {}'.format(run_id))

    connection = MySqlHook(mysql_conn_id='lg_job_tracker')
    query = "SELECT STATUS from lgetl.JOB_AUDIT where STATUS != 'STARTED' and RUN_ID=%(run_id)s"

    lg_task_timeout = int(Variable.get("lg_task_timeout"))
    max_attempts = lg_task_timeout / 60
    while (max_attempts >= 0):
        max_attempts -= 1
        print('Attempting to Job Tracker Status')
        row = connection.get_first(query, parameters={'run_id': run_id})
        print('Value for DBhook {}'.format(row))
        if row is not None and len(row) > 0:
            print(row[0])
            if row[0] == 'SUCCESS' or row[0] == 'SKIP':
                return True
            elif row[0] == 'FAIL':
                break
        else:
            time.sleep(60)
            print('Job in progress. No Job Tracker record')

    raise AirflowException('Failed to detect Job Tracker File. Failing the task')

def clustersetup(**kwargs):
    batch_run_id = kwargs['dag_run'].run_id
    run_uuid = str(uuid.uuid1())
    lg_load_date = Variable.get("lg_load_date")
    job_config = {"name": run_uuid,
                  "file": "s3://az-us-lg-pilot-config/oozie-migration/source_code/TempTableLocationFix.py",
                  "conf": {"spark.default.parallelism": 200,
                           "spark.driver.memory": '1g',
                           "spark.submit.deployMode": 'cluster',
                           "spark.executor.memory": '2g'},
                  "args": ["--dag-id", 'wf_lg_wkly_master.initialization',
                           "--run-id", run_uuid,
                           "--batch-id", batch_run_id,
                           "--load-date", lg_load_date
                           ]}

    host = 'http://' + Variable.get('lg_host_ip') + ':8998'
    r = requests.post(host + '/batches', data=json.dumps(job_config))
    if r.status_code >= 200 and r.status_code < 300:
        response = r.json()
        livy_session_id = str(response['id'])
        print('Livy Session Id# {}'.format(livy_session_id))

    elif r.status_code != 200 :
        print('Failed to submit job. HTTP Response# {}'.format(r.json()))
        raise AirflowException('Failed to submit job. HTTP Response# {}'.format(r.json()))

    return run_uuid

# qc1_task = BashOperator(
#     task_id='qc1_task',
#     bash_command="/usr/share/airflow/dags/qc/qc1/edhQCMaster/edhQCMaster_run.sh '" + Variable.get("lg_load_date") + "' ",
#     dag=main_dag)

schema_switch_setup_tsk = BashOperator(
    task_id='schema_switch_setup_tsk',
    bash_command='/usr/share/airflow/dags/schema_switch/setup_schema_switch.sh ' + Variable.get('lg_host_ip') + ' ',
    dag=main_dag)


setup = PythonOperator(
    task_id="clustersetup_tsk",
    provide_context=True,
    python_callable=clustersetup,
    dag=main_dag)

watchStep = PythonOperator(
    task_id='clustersetup_watch',
    provide_context=True,
    python_callable=clustersetupwatch,
    templates_dict={'run-id': "{{ ti.xcom_pull(task_ids='clustersetup_tsk') }}"},
    dag=main_dag)

#clusterCreate >> sub_dag1
clusterCreate >> setup >> watchStep >> schema_switch_setup_tsk
# clusterCreate >> qc1_task

sub_dag1 = SubDagOperator(
    subdag=lg_wkly_wf_Sample_Central(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_Sample_Central'),
    task_id = 'lg_wkly_wf_Sample_Central',
    dag=main_dag
)

schema_switch_setup_tsk >> sub_dag1

sub_dag2 = SubDagOperator(
    subdag=lg_wkly_wf_dev_Purush_lg_cust_hcp_addr_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_dev_Purush_lg_cust_hcp_addr_d'),
    task_id = 'lg_wkly_wf_dev_Purush_lg_cust_hcp_addr_d',
    dag=main_dag
)

schema_switch_setup_tsk >> sub_dag2

sub_dag3 = SubDagOperator(
    subdag=lg_wkly_wf_DDD_Weekly(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_DDD_Weekly'),
    task_id = 'lg_wkly_wf_DDD_Weekly',
    dag=main_dag
)

schema_switch_setup_tsk >> sub_dag3

sub_dag4 = SubDagOperator(
    subdag=lg_wkly_wf_lg_promo_acty_atnd_lkp(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_promo_acty_atnd_lkp'),
    task_id = 'lg_wkly_wf_lg_promo_acty_atnd_lkp',
    dag=main_dag
)

schema_switch_setup_tsk >> sub_dag4

sub_dag5 = SubDagOperator(
    subdag=lg_wkly_wf_lg_cust_mcp_rel_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_cust_mcp_rel_d'),
    task_id = 'lg_wkly_wf_lg_cust_mcp_rel_d',
    dag=main_dag
)

schema_switch_setup_tsk >> sub_dag5

sub_dag6 = SubDagOperator(
    subdag=lg_wkly_wf_lg_hci_blocklist(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_hci_blocklist'),
    task_id = 'lg_wkly_wf_lg_hci_blocklist',
    dag=main_dag
)

schema_switch_setup_tsk >> sub_dag6

sub_dag7 = SubDagOperator(
    subdag=lg_wkly_wf_lg_tgt_plan_lmr_f(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_tgt_plan_lmr_f'),
    task_id = 'lg_wkly_wf_lg_tgt_plan_lmr_f',
    dag=main_dag
)

schema_switch_setup_tsk >> sub_dag7

sub_dag8 = SubDagOperator(
    subdag=lg_wkly_wf_MCM_PSKW(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_MCM_PSKW'),
    task_id = 'lg_wkly_wf_MCM_PSKW',
    dag=main_dag
)

schema_switch_setup_tsk >> sub_dag8

sub_dag9 = SubDagOperator(
    subdag=lg_wkly_wf_lg_curr_algn_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_curr_algn_d'),
    task_id = 'lg_wkly_wf_lg_curr_algn_d',
    dag=main_dag
)

schema_switch_setup_tsk >> sub_dag9

sub_dag10 = SubDagOperator(
    subdag=lg_wkly_wf_MCM_Nielsen(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_MCM_Nielsen'),
    task_id = 'lg_wkly_wf_MCM_Nielsen',
    dag=main_dag
)

schema_switch_setup_tsk >> sub_dag10

sub_dag11 = SubDagOperator(
    subdag=lg_wkly_wf_lg_cust_hca_addr_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_cust_hca_addr_d'),
    task_id = 'lg_wkly_wf_lg_cust_hca_addr_d',
    dag=main_dag
)

schema_switch_setup_tsk >> sub_dag11

sub_dag12 = SubDagOperator(
    subdag=lg_wkly_wf_lg_acct_hcp_tgt_f(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_acct_hcp_tgt_f'),
    task_id = 'lg_wkly_wf_lg_acct_hcp_tgt_f',
    dag=main_dag
)

schema_switch_setup_tsk >> sub_dag12

sub_dag13 = SubDagOperator(
    subdag=lg_wkly_wf_MCM_Abode(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_MCM_Abode'),
    task_id = 'lg_wkly_wf_MCM_Abode',
    dag=main_dag
)

schema_switch_setup_tsk >> sub_dag13

sub_dag14 = SubDagOperator(
    subdag=lg_wkly_wf_lg_formulary_restrictions_f(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_formulary_restrictions_f'),
    task_id = 'lg_wkly_wf_lg_formulary_restrictions_f',
    dag=main_dag
)

schema_switch_setup_tsk >> sub_dag14


sub_dag15 = SubDagOperator(
    subdag=lg_wkly_wf_XPO_Weekly(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_XPO_Weekly'),
    task_id = 'lg_wkly_wf_XPO_Weekly',
    dag=main_dag
)

schema_switch_setup_tsk >> sub_dag15

sub_dag16 = SubDagOperator(
    subdag=lg_wkly_wf_MCM_Publicis(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_MCM_Publicis'),
    task_id = 'lg_wkly_wf_MCM_Publicis',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag16

sub_dag17 = SubDagOperator(
    subdag=lg_wkly_wf_RAC_Alignment(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_RAC_Alignment'),
    task_id = 'lg_wkly_wf_RAC_Alignment',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag17

sub_dag18 = SubDagOperator(
    subdag=lg_wkly_wf_lg_hcp_blocklist(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_hcp_blocklist'),
    task_id = 'lg_wkly_wf_lg_hcp_blocklist',
    dag=main_dag
)

schema_switch_setup_tsk >> sub_dag18

sub_dag20 = SubDagOperator(
    subdag=lg_wkly_wf_Payer_Bridge(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_Payer_Bridge'),
    task_id = 'lg_wkly_wf_Payer_Bridge',
    dag=main_dag
)

schema_switch_setup_tsk >> sub_dag20

sub_dag21 = SubDagOperator(
    subdag=lg_wkly_wf_RAC_Segmentation(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_RAC_Segmentation'),
    task_id = 'lg_wkly_wf_RAC_Segmentation',
    dag=main_dag
)

schema_switch_setup_tsk >> sub_dag21


sub_dag22 = SubDagOperator(
    subdag=lg_wkly_wf_lg_cust_mcp_payer_addr_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_cust_mcp_payer_addr_d'),
    task_id = 'lg_wkly_wf_lg_cust_mcp_payer_addr_d',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag22

sub_dag23 = SubDagOperator(
    subdag=lg_wkly_wf_lg_cust_mcp_pbm_demog_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_cust_mcp_pbm_demog_d'),
    task_id = 'lg_wkly_wf_lg_cust_mcp_pbm_demog_d',
    dag=main_dag
)

schema_switch_setup_tsk >> sub_dag23

sub_dag24 = SubDagOperator(
    subdag=lg_wkly_wf_lg_cust_hca_affil_prnt_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_cust_hca_affil_prnt_d'),
    task_id = 'lg_wkly_wf_lg_cust_hca_affil_prnt_d',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag24

sub_dag25 = SubDagOperator(
    subdag=lg_wkly_wf_lg_consumer_rebates_f(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_consumer_rebates_f'),
    task_id = 'lg_wkly_wf_lg_consumer_rebates_f',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag25

sub_dag26 = SubDagOperator(
    subdag=lg_wkly_wf_dev_Purush_lg_cust_merged_xref(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_dev_Purush_lg_cust_merged_xref'),
    task_id = 'lg_wkly_wf_dev_Purush_lg_cust_merged_xref',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag26

sub_dag27 = SubDagOperator(
    subdag=lg_wkly_wf_lg_promo_acty_atnd_f(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_promo_acty_atnd_f'),
    task_id = 'lg_wkly_wf_lg_promo_acty_atnd_f',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag27

sub_dag28 = SubDagOperator(
    subdag=lg_wkly_wf_lg_cust_mcp_payer_demog_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_cust_mcp_payer_demog_d'),
    task_id = 'lg_wkly_wf_lg_cust_mcp_payer_demog_d',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag28

sub_dag29 = SubDagOperator(
    subdag=lg_wkly_wf_lg_cust_mcp_other_demog_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_cust_mcp_other_demog_d'),
    task_id = 'lg_wkly_wf_lg_cust_mcp_other_demog_d',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag29

sub_dag30 = SubDagOperator(
    subdag=lg_wkly_wf_Speaker_Program(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_Speaker_Program'),
    task_id = 'lg_wkly_wf_Speaker_Program',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag30

sub_dag31 = SubDagOperator(
    subdag=lg_wkly_wf_RAC_Person(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_RAC_Person'),
    task_id = 'lg_wkly_wf_RAC_Person',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag31

sub_dag32 = SubDagOperator(
    subdag=lg_wkly_wf_lg_cust_hca_hca_affil_prnt_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_cust_hca_hca_affil_prnt_d'),
    task_id = 'lg_wkly_wf_lg_cust_hca_hca_affil_prnt_d',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag32

sub_dag33 = SubDagOperator(
    subdag=lg_wkly_wf_lg_cust_mcp_plan_addr_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_cust_mcp_plan_addr_d'),
    task_id = 'lg_wkly_wf_lg_cust_mcp_plan_addr_d',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag33

sub_dag34 = SubDagOperator(
    subdag=lg_wkly_wf_lg_cust_hcp_prof_typ_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_cust_hcp_prof_typ_d'),
    task_id = 'lg_wkly_wf_lg_cust_hcp_prof_typ_d',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag34

sub_dag35 = SubDagOperator(
    subdag=lg_wkly_wf_lg_cust_hcp_title_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_cust_hcp_title_d'),
    task_id = 'lg_wkly_wf_lg_cust_hcp_title_d',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag35

sub_dag36 = SubDagOperator(
    subdag=lg_wkly_wf_lg_promo_acty_f(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_promo_acty_f'),
    task_id = 'lg_wkly_wf_lg_promo_acty_f',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag36

sub_dag37 = SubDagOperator(
    subdag=lg_wkly_wf_lg_cust_mcp_plan_demog_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_cust_mcp_plan_demog_d'),
    task_id = 'lg_wkly_wf_lg_cust_mcp_plan_demog_d',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag37

sub_dag38 = SubDagOperator(
    subdag=lg_wkly_wf_MCM_MCRM(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_MCM_MCRM'),
    task_id = 'lg_wkly_wf_MCM_MCRM',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag38

sub_dag39 = SubDagOperator(
    subdag=lg_wkly_wf_RAC_Targeting(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_RAC_Targeting'),
    task_id = 'lg_wkly_wf_RAC_Targeting',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag39

sub_dag40 = SubDagOperator(
    subdag=lg_wkly_wf_lg_cust_hca_affil_child_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_cust_hca_affil_child_d'),
    task_id = 'lg_wkly_wf_lg_cust_hca_affil_child_d',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag40

sub_dag41 = SubDagOperator(
    subdag=lg_wkly_wf_lg_ddd_hcos_olet_f(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_ddd_hcos_olet_f'),
    task_id = 'lg_wkly_wf_lg_ddd_hcos_olet_f',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag41

sub_dag42 = SubDagOperator(
    subdag=lg_wkly_wf_lg_persn_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_persn_d'),
    task_id = 'lg_wkly_wf_lg_persn_d',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag42

sub_dag43 = SubDagOperator(
    subdag=lg_wkly_wf_Activity(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_Activity'),
    task_id = 'lg_wkly_wf_Activity',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag43

sub_dag45 = SubDagOperator(
    subdag=lg_wkly_wf_dev_Purush_lg_cust_hcp_id_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_dev_Purush_lg_cust_hcp_id_d'),
    task_id = 'lg_wkly_wf_dev_Purush_lg_cust_hcp_id_d',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag45

sub_dag46 = SubDagOperator(
    subdag=lg_wkly_wf_Concur(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_Concur'),
    task_id = 'lg_wkly_wf_Concur',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag46

sub_dag47 = SubDagOperator(
    subdag=lg_wkly_wf_lg_dddmd_olet_hcp_f(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_dddmd_olet_hcp_f'),
    task_id = 'lg_wkly_wf_lg_dddmd_olet_hcp_f',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag47

sub_dag48 = SubDagOperator(
    subdag=lg_wkly_wf_lg_cust_hca_hca_affil_child_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_cust_hca_hca_affil_child_d'),
    task_id = 'lg_wkly_wf_lg_cust_hca_hca_affil_child_d',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag48

sub_dag49 = SubDagOperator(
    subdag=lg_wkly_wf_lg_cust_hca_demog_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_cust_hca_demog_d'),
    task_id = 'lg_wkly_wf_lg_cust_hca_demog_d',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag49

sub_dag50 = SubDagOperator(
    subdag=lg_wkly_wf_lg_cust_hcp_spec_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_cust_hcp_spec_d'),
    task_id = 'lg_wkly_wf_lg_cust_hcp_spec_d',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag50

sub_dag51 = SubDagOperator(
    subdag=lg_wkly_wf_dev_Purush_lg_cust_hcp_demog_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_dev_Purush_lg_cust_hcp_demog_d'),
    task_id = 'lg_wkly_wf_dev_Purush_lg_cust_hcp_demog_d',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag51

sub_dag52 = SubDagOperator(
    subdag=lg_wkly_wf_lg_cust_hcp_spec_typ_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_cust_hcp_spec_typ_d'),
    task_id = 'lg_wkly_wf_lg_cust_hcp_spec_typ_d',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag52

sub_dag53 = SubDagOperator(
    subdag=lg_wkly_wf_lg_hcp_milestone(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_hcp_milestone'),
    task_id = 'lg_wkly_wf_lg_hcp_milestone',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag53

sub_dag54 = SubDagOperator(
    subdag=lg_wkly_wf_lg_cust_mcp_pbm_addr_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_cust_mcp_pbm_addr_d'),
    task_id = 'lg_wkly_wf_lg_cust_mcp_pbm_addr_d',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag54

sub_dag55 = SubDagOperator(
    subdag=lg_wkly_wf_MDM(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_MDM'),
    task_id = 'lg_wkly_wf_MDM',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag55

sub_dag56 = SubDagOperator(
    subdag=lg_wkly_wf_lg_cust_hca_id_d(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_cust_hca_id_d'),
    task_id = 'lg_wkly_wf_lg_cust_hca_id_d',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag56

sub_dag57 = SubDagOperator(
    subdag=lg_wkly_wf_lg_promo_acty_info_lkp(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_promo_acty_info_lkp'),
    task_id = 'lg_wkly_wf_lg_promo_acty_info_lkp',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag57

sub_dag58 = SubDagOperator(
    subdag=lg_wkly_wf_lg_product_ref_t(main_dag.dag_id, main_dag.start_date, DEFAULT_ARGS, task_id='lg_wkly_wf_lg_product_ref_t'),
    task_id = 'lg_wkly_wf_lg_product_ref_t',
    dag=main_dag
)
schema_switch_setup_tsk >> sub_dag58
