
import airflow
from airflow.models import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import BranchPythonOperator

from airflow.utils.dates import days_ago
from airflow.models import Variable

from lgutils.utils import lgfunctions
from airflow.contrib.sensors.emr_step_sensor import EmrStepSensor
from airflow.contrib.operators.emr_add_steps_operator import EmrAddStepsOperator


DEFAULT_ARGS = {
    'owner': 'airflow',
    'depends_on_past': False
}

skipPreviousSteps = Variable.get("lg_ingestion_skip_processed")
partitionDate = Variable.get("lg_load_date")

# emrX = boto3.client('emr', region_name='us-east-1')
clusterId = 'j-55BV0X84633R'#Variable.get("lg_ingestion_cluster_id")



DEFAULT_ARGS = {
    'owner': 'airflow',
    'depends_on_past': False
}

dag = DAG(
    'wf_MCM_MCRM',
    default_args=DEFAULT_ARGS,
    schedule_interval='@once',
    start_date=days_ago(1),
    catchup=False
)

#Create LG Utility funtions object
lg = lgfunctions(clusterId, partitionDate, skipPreviousSteps, dag)


lg_mcm_mcrm_trckng_logs = lg.createTableStep(table='lg_mcm_mcrm_trckng_logs', executor_memory='5GB', driver_memory='3GB')
lg_mcm_mcrm_trckng_logs_step_checker = lg.createWatchStep(table='lg_mcm_mcrm_trckng_logs')


lg_mcm_mcrm_srvy_rsps = lg.createTableStep(table='lg_mcm_mcrm_srvy_rsps', executor_memory='5GB', driver_memory='3GB')
lg_mcm_mcrm_srvy_rsps_step_checker = lg.createWatchStep(table='lg_mcm_mcrm_srvy_rsps')


lg_mcm_mcrm_sbscrptn_ref = lg.createTableStep(table='lg_mcm_mcrm_sbscrptn_ref', executor_memory='5GB', driver_memory='3GB')
lg_mcm_mcrm_sbscrptn_ref_step_checker = lg.createWatchStep(table='lg_mcm_mcrm_sbscrptn_ref')

lg_mcm_mcrm_sbscrptn_opt = lg.createTableStep(table='lg_mcm_mcrm_sbscrptn_opt', executor_memory='5GB', driver_memory='3GB')
lg_mcm_mcrm_sbscrptn_opt_step_checker = lg.createWatchStep(table='lg_mcm_mcrm_sbscrptn_opt')


lg_mcm_mcrm_sbscrptn_opt_hstry = lg.createTableStep(table='lg_mcm_mcrm_sbscrptn_opt_hstry', executor_memory='5GB', driver_memory='3GB')
lg_mcm_mcrm_sbscrptn_opt_hstry_step_checker = lg.createWatchStep(table='lg_mcm_mcrm_sbscrptn_opt_hstry')


lg_mcm_mcrm_pgrm_ref = lg.createTableStep(table='lg_mcm_mcrm_pgrm_ref', executor_memory='5GB', driver_memory='3GB')
lg_mcm_mcrm_pgrm_ref_step_checker = lg.createWatchStep(table='lg_mcm_mcrm_pgrm_ref')


lg_mcm_mcrm_mrgd_cnsmrs = lg.createTableStep(table='lg_mcm_mcrm_mrgd_cnsmrs', executor_memory='5GB', driver_memory='3GB')
lg_mcm_mcrm_mrgd_cnsmrs_step_checker = lg.createWatchStep(table='lg_mcm_mcrm_mrgd_cnsmrs')


lg_mcm_mcrm_media_code = lg.createTableStep(table='lg_mcm_mcrm_media_code', executor_memory='5GB', driver_memory='3GB')
lg_mcm_mcrm_media_code_step_checker = lg.createWatchStep(table='lg_mcm_mcrm_media_code')

lg_mcm_mcrm_failed_dlvrys = lg.createTableStep(table='lg_mcm_mcrm_failed_dlvrys', executor_memory='5GB', driver_memory='3GB')
lg_mcm_mcrm_failed_dlvrys_step_checker = lg.createWatchStep(table='lg_mcm_mcrm_failed_dlvrys')

lg_mcm_mcrm_exclsn_logs = lg.createTableStep(table='lg_mcm_mcrm_exclsn_logs', executor_memory='5GB', driver_memory='3GB')
lg_mcm_mcrm_exclsn_logs_step_checker = lg.createWatchStep(table='lg_mcm_mcrm_exclsn_logs')

lg_mcm_mcrm_dlvry = lg.createTableStep(table='lg_mcm_mcrm_dlvry', executor_memory='5GB', driver_memory='3GB')
lg_mcm_mcrm_dlvry_step_checker = lg.createWatchStep(table='lg_mcm_mcrm_dlvry')

lg_mcm_mcrm_dlvry_logs = lg.createTableStep(table='lg_mcm_mcrm_dlvry_logs', executor_memory='5GB', driver_memory='3GB')
lg_mcm_mcrm_dlvry_logs_step_checker = lg.createWatchStep(table='lg_mcm_mcrm_dlvry_logs')

lg_mcm_mcrm_disease_state_ref = lg.createTableStep(table='lg_mcm_mcrm_disease_state_ref', executor_memory='5GB', driver_memory='3GB')
lg_mcm_mcrm_disease_state_ref_step_checker = lg.createWatchStep(table='lg_mcm_mcrm_disease_state_ref')

lg_mcm_mcrm_disease_state_opt = lg.createTableStep(table='lg_mcm_mcrm_disease_state_opt', executor_memory='5GB', driver_memory='3GB')
lg_mcm_mcrm_disease_state_opt_step_checker = lg.createWatchStep(table='lg_mcm_mcrm_disease_state_opt')

lg_mcm_mcrm_disease_state_opt_hstry = lg.createTableStep(table='lg_mcm_mcrm_disease_state_opt_hstry', executor_memory='5GB', driver_memory='3GB')
lg_mcm_mcrm_disease_state_opt_hstry_step_checker = lg.createWatchStep(table='lg_mcm_mcrm_disease_state_opt_hstry')

lg_mcm_mcrm_cnsmr_srvy_xref = lg.createTableStep(table='lg_mcm_mcrm_cnsmr_srvy_xref', executor_memory='5GB', driver_memory='3GB')
lg_mcm_mcrm_cnsmr_srvy_xref_step_checker = lg.createWatchStep(table='lg_mcm_mcrm_cnsmr_srvy_xref')

lg_mcm_mcrm_cnsmr_sgmnt = lg.createTableStep(table='lg_mcm_mcrm_cnsmr_sgmnt', executor_memory='5GB', driver_memory='3GB')
lg_mcm_mcrm_cnsmr_sgmnt_step_checker = lg.createWatchStep(table='lg_mcm_mcrm_cnsmr_sgmnt')

lg_mcm_mcrm_cnsmr_prmtn_rsp = lg.createTableStep(table='lg_mcm_mcrm_cnsmr_prmtn_rsp', executor_memory='5GB', driver_memory='3GB')
lg_mcm_mcrm_cnsmr_prmtn_rsp_step_checker = lg.createWatchStep(table='lg_mcm_mcrm_cnsmr_prmtn_rsp')

lg_mcm_mcrm_cnsmr = lg.createTableStep(table='lg_mcm_mcrm_cnsmr', executor_memory='5GB', driver_memory='3GB')
lg_mcm_mcrm_cnsmr_step_checker = lg.createWatchStep(table='lg_mcm_mcrm_cnsmr')

lg_mcm_mcrm_cmpgns = lg.createTableStep(table='lg_mcm_mcrm_cmpgns', executor_memory='5GB', driver_memory='3GB')
lg_mcm_mcrm_cmpgns_step_checker = lg.createWatchStep(table='lg_mcm_mcrm_cmpgns')

lg_mcm_mcrm_cnsmr_pgrm_product_opt_hist = lg.createTableStep(table='lg_mcm_mcrm_cnsmr_pgrm_product_opt_hist', executor_memory='5GB', driver_memory='3GB')
lg_mcm_mcrm_cnsmr_pgrm_product_opt_hist_step_checker = lg.createWatchStep(table='lg_mcm_mcrm_cnsmr_pgrm_product_opt_hist')

lg_mcm_mcrm_cnsmr_pgrm_product_opt = lg.createTableStep(table='lg_mcm_mcrm_cnsmr_pgrm_product_opt', executor_memory='5GB', driver_memory='3GB')
lg_mcm_mcrm_cnsmr_pgrm_product_opt_step_checker = lg.createWatchStep(table='lg_mcm_mcrm_cnsmr_pgrm_product_opt')


begin = DummyOperator(
    task_id='begin_tsk',
    dag=dag
)

begin >> lg_mcm_mcrm_trckng_logs >> lg_mcm_mcrm_trckng_logs_step_checker

begin >> lg_mcm_mcrm_srvy_rsps >> lg_mcm_mcrm_srvy_rsps_step_checker

begin >> lg_mcm_mcrm_sbscrptn_ref >> lg_mcm_mcrm_sbscrptn_ref_step_checker

begin >> lg_mcm_mcrm_sbscrptn_opt >> lg_mcm_mcrm_sbscrptn_opt_step_checker

begin >> lg_mcm_mcrm_sbscrptn_opt_hstry >> lg_mcm_mcrm_sbscrptn_opt_hstry_step_checker

begin >> lg_mcm_mcrm_pgrm_ref >> lg_mcm_mcrm_pgrm_ref_step_checker

begin >> lg_mcm_mcrm_mrgd_cnsmrs >> lg_mcm_mcrm_mrgd_cnsmrs_step_checker

begin >> lg_mcm_mcrm_media_code >> lg_mcm_mcrm_media_code_step_checker

begin >> lg_mcm_mcrm_failed_dlvrys >> lg_mcm_mcrm_failed_dlvrys_step_checker

begin >> lg_mcm_mcrm_exclsn_logs >> lg_mcm_mcrm_exclsn_logs_step_checker

begin >> lg_mcm_mcrm_dlvry >> lg_mcm_mcrm_dlvry_step_checker

begin >> lg_mcm_mcrm_dlvry_logs >> lg_mcm_mcrm_dlvry_logs_step_checker

begin >> lg_mcm_mcrm_disease_state_ref >> lg_mcm_mcrm_disease_state_ref_step_checker

begin >> lg_mcm_mcrm_disease_state_opt >> lg_mcm_mcrm_disease_state_opt_step_checker

begin >> lg_mcm_mcrm_disease_state_opt_hstry >> lg_mcm_mcrm_disease_state_opt_hstry_step_checker

begin >> lg_mcm_mcrm_cnsmr_srvy_xref >> lg_mcm_mcrm_cnsmr_srvy_xref_step_checker

begin >> lg_mcm_mcrm_cnsmr_sgmnt >> lg_mcm_mcrm_cnsmr_sgmnt_step_checker

begin >> lg_mcm_mcrm_cnsmr_prmtn_rsp >> lg_mcm_mcrm_cnsmr_prmtn_rsp_step_checker

begin >> lg_mcm_mcrm_cnsmr >> lg_mcm_mcrm_cnsmr_step_checker

begin >> lg_mcm_mcrm_cmpgns >> lg_mcm_mcrm_cmpgns_step_checker

begin >> lg_mcm_mcrm_cnsmr_pgrm_product_opt_hist >> lg_mcm_mcrm_cnsmr_pgrm_product_opt_hist_step_checker

begin >> lg_mcm_mcrm_cnsmr_pgrm_product_opt >> lg_mcm_mcrm_cnsmr_pgrm_product_opt_step_checker

