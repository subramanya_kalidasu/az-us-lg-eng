import boto3, requests

from airflow.operators.python_operator import ShortCircuitOperator
from datetime import date, datetime

from datetime import timedelta
from livy import LivySession, SessionKind, SessionState
from airflow.contrib.sensors.emr_step_sensor import EmrStepSensor
from airflow.contrib.operators.emr_add_steps_operator import EmrAddStepsOperator
from airflow.operators.python_operator import PythonOperator
from airflow import AirflowException
import json
import requests
from botocore.exceptions import ClientError
import time
from airflow.models import Variable
from airflow.hooks.base_hook import BaseHook

from airflow.sensors.s3_key_sensor import S3KeySensor
from airflow.hooks.mysql_hook import MySqlHook
import uuid


class lgfunctions:

    def __init__(self, **kwargs):
        if 'dag' in kwargs:
            self.dag = kwargs['dag']
        else:
            raise AirflowException('DAG parameter is required to initialize lgutils')

        try:
            if 'cluster_id' in kwargs:
                self.cluster_id = kwargs['cluster_id']
            else:
                self.cluster_id = Variable.get("lg_cluster_id")

            print('Using Cluster ID for the Dag# {}'.format(self.cluster_id))

            self.master_ip = Variable.get("lg_host_ip")
            if self.master_ip is None:
                keys = self.getEMRConnection("lg_emr_ingestion")
                self.emrX = boto3.client('emr', aws_access_key_id=keys[0], aws_secret_access_key=keys[1],
                                         region_name='us-east-1')
                # self.emrX = boto3.client('emr', region_name='us-east-1')

                instances = self.emrX.list_instances(ClusterId=self.cluster_id, InstanceGroupTypes=['MASTER'])
                self.master_ip = instances['Instances'][0]['PrivateIpAddress']

            self.host = 'http://' + self.master_ip + ':8998'

            if 'skip_processed' in kwargs:
                self.skip_processed = kwargs['skip_processed']
            else:
                self.skip_processed = Variable.get("lg_skip_processed")

            print('Using Skip Previous Steps Flag Dag# {}'.format(self.skip_processed))

            if 'load_date' in kwargs:
                self.load_date = kwargs['load_date']
            else:
                self.load_date = Variable.get("lg_load_date")

            print('Using Load Date or Partition Date # {}'.format(self.load_date))

            if 'task_submit_timeout' in kwargs:
                self.task_submit_timeout = int(kwargs['task_submit_timeout'])
            else:
                self.task_submit_timeout = int(Variable.get("lg_task_submit_timeout"))

            print('Using Task Submit Timeout # {}'.format(self.task_submit_timeout))

            if 'task_timeout' in kwargs:
                self.task_timeout = int(kwargs['task_timeout'])
            else:
                self.task_timeout = int(Variable.get("lg_task_timeout"))

            print('Using Task Timeout # {}'.format(self.task_timeout))

            if 'run_job_mode' in kwargs:
                self.run_job_mode = kwargs['run_job_mode']
            else:
                self.run_job_mode = Variable.get("lg_run_job_mode")

            print('Using Run job mode # {}'.format(self.run_job_mode))

            self.tsk_counter = 0
        except Exception as e:
            print(e)
            raise AirflowException('FATAL lgutils# initialization failure')

    def getEMRConnection(self, connectstring):
        connection = BaseHook.get_connection(connectstring)
        key = connection.login
        secretkey = connection.password

        return [key, secretkey]

    def getNextId(self):
        self.tsk_counter = self.tsk_counter + 1
        return str(self.tsk_counter)

    def getLastPartitionDay(self):
        if self.load_date is None or len(self.load_date) == 0:
            today = date.today()
            offset = (today.weekday() - 5) % 7
            lastSaturday = today - timedelta(days=offset)
            print('New Partition Date is determined as {}'.format(lastSaturday))
            self.load_date = str(lastSaturday)

        return self.load_date

    def getTaskId(self, **kwargs):
        if kwargs is not None and 'task_type' in kwargs and kwargs['task_type']:
            if kwargs['task_type'] == 'distcp':
                return kwargs['table'] + '_dstcp_tsk_' + self.getNextId()
            elif kwargs['task_type'] == 'hsql':
                return kwargs['table'] + '_hsql_tsk_' + self.getNextId()
            else:
                return kwargs['table'] + '_tsk_' + self.getNextId()
        else:
            return kwargs['table'] + '_tsk_' + self.getNextId()

    def tableCheckCalable(self, table_name, **kwargs):
        if self.skip_processed == 'True':
            instances = self.emrX.list_instances(ClusterId=self.cluster_id, InstanceGroupTypes=['MASTER'])
            master_ip = instances['Instances'][0]['PrivateIpAddress']
            host = 'http://' + master_ip + ':8998'

            with LivySession(host, kind=SessionKind.PYSPARK) as session:
                assert session.state == SessionState.IDLE

                session.run(
                    'v = spark.sql("SELECT COUNT(*) FROM lg_stage.' + table_name + ' where load_date=\'' + self.getLastPartitionDay() + '\'")')
                v = session.read('v')
                print('{} Record Count {}'.format(table_name, v['count(1)'][0]))

                count = v['count(1)'][0]
                return count == 0
        else:
            return True

    def dynamicTableCheckOperator(self, **kwargs):
        return ShortCircuitOperator(
            task_id="TableStatusCheck_{}_tsk".format(kwargs['table']),
            python_callable=self.tableCheckCalable,
            depends_on_past=False,
            provide_context=True,
            op_kwargs={'table_name': kwargs['table']},
            dag=self.dag)


    def getEMRStepConfig(self, **kwargs):
        executor_memory = (kwargs is not None and 'executor_memory' in kwargs and kwargs['executor_memory']) or ('5g')
        driver_memory = (kwargs is not None and 'driver_memory' in kwargs and kwargs['driver_memory']) or ('3g')
        num_executors = (kwargs is not None and 'num_executors' in kwargs and kwargs['num_executors']) or ('10')
        spark_default_parallelism = (kwargs is not None and 'spark_default_parallelism' in kwargs and kwargs['spark_default_parallelism']) or ('200')
        spark_executor_cores = (kwargs is not None and 'spark_executor_cores' in kwargs and kwargs['spark_executor_cores']) or ('4')
        spark_kryoserializer_buffer_max = (kwargs is not None and 'spark_kryoserializer_buffer_max' in kwargs and kwargs['spark_kryoserializer_buffer_max']) or ('512m')
        spark_broadcast_blockSize = (kwargs is not None and 'spark_broadcast_blockSize' in kwargs and kwargs['spark_broadcast_blockSize']) or ('4m')
        spark_files_overwrite = (kwargs is not None and 'spark_files_overwrite' in kwargs and kwargs['spark_files_overwrite']) or ('true')
        spark_python_worker_memory = (kwargs is not None and 'spark_python_worker_memory' in kwargs and kwargs['spark_python_worker_memory']) or ('512m')
        spark_shuffle_file_buffer = (kwargs is not None and 'spark_shuffle_file_buffer' in kwargs and kwargs['spark_shuffle_file_buffer']) or ('256k')
        spark_reducer_maxSizeInFlight = (kwargs is not None and 'spark_shuffle_file_buffer' in kwargs and kwargs['spark_shuffle_file_buffer']) or ('96m')

        step = [
            {
                'Name': 'StepExecution-' + kwargs['table'],
                'ActionOnFailure': 'CONTINUE',
                'HadoopJarStep': {
                    'Jar': 'command-runner.jar',
                    'Args': [
                        '/usr/lib/spark/bin/spark-submit',
                        '--master',
                        'yarn',
                        '--deploy-mode',
                        'cluster',
                        '--executor-memory',
                        executor_memory,
                        '--driver-memory',
                        driver_memory,
                        '--num-executors',
                        num_executors,
                        's3://az-us-lg-pilot-config/oozie-migration/source_code/StepExecutorV2.py',
                        '--metadata-file-name',
                        kwargs['table'] + '.txt',
                        '--spark_default_parallelism',
                        spark_default_parallelism,
                        '--spark_submit_deployMode',
                        self.run_job_mode,
                        '--load-date',
                        self.getLastPartitionDay(),
                        '--skip-process',
                        self.skip_processed
                    ]
                }
            }
        ]

        return step

    def getHsqlStepConfig(self, **kwargs):
        step = [
            {
                'Name': 'StepExecution-' + kwargs['table'],
                'ActionOnFailure': 'CONTINUE',
                'HadoopJarStep': {
                    'Jar': 'command-runner.jar',
                    'Args': [
                        '/usr/lib/spark/bin/spark-submit',
                        '--master',
                        'yarn',
                        '--deploy-mode',
                        'cluster',
                        's3://az-us-lg-pilot-config/oozie-migration/source_code/hsqlStep.py',
                        '--metadata-file-name',
                        kwargs['hsql_file'],
                        '--load-date',
                        self.getLastPartitionDay(),
                        '--spark_submit_deployMode',
                        self.run_job_mode,
                        '--skip-process',
                        self.skip_processed
                    ]
                }
            }
        ]

        return step

    def getDistCpStepConfig(self, **kwargs):
        step = [
            {
                "Name": "DistCopy",
                "ActionOnFailure": "CONTINUE",
                "HadoopJarStep": {
                    "Jar": "s3://az-us-lg-pilot-config/oozie-migration/source_code/script-runner.jar",
                    "Args": [
                        "s3://az-us-lg-pilot-config/oozie-migration/source_code/run-dist-cp.sh",
                        kwargs['source'] + '/load_date=' + self.getLastPartitionDay(),
                        's3://az-us-lg-pilot-stage/staging/' + kwargs['table'] + '/load_date=' + self.getLastPartitionDay()
                    ]
                }
            }
        ]
        return step

    def getLivyTableConfig(self, **kwargs):
        lg_run_id = str(uuid.uuid1())

        #lg_run_id = self.dag.dag_id + '_' + kwargs['table'] + '_table_' + str(datetime.now().timestamp())

        executor_memory = (kwargs is not None and 'executor_memory' in kwargs and kwargs['executor_memory']) or ('5g')
        driver_memory = (kwargs is not None and 'driver_memory' in kwargs and kwargs['driver_memory']) or ('1g')
        #num_executors = (kwargs is not None and 'num_executors' in kwargs and kwargs['num_executors']) or ('10')
        spark_default_parallelism = (kwargs is not None and 'spark_default_parallelism' in kwargs and kwargs['spark_default_parallelism']) or ('200')
        spark_executor_cores = (kwargs is not None and 'spark_executor_cores' in kwargs and kwargs['spark_executor_cores']) or ('4')
        spark_kryoserializer_buffer_max = (kwargs is not None and 'spark_kryoserializer_buffer_max' in kwargs and kwargs['spark_kryoserializer_buffer_max']) or ('512m')
        spark_broadcast_blockSize = (kwargs is not None and 'spark_broadcast_blockSize' in kwargs and kwargs['spark_broadcast_blockSize']) or ('4m')
        spark_files_overwrite = (kwargs is not None and 'spark_files_overwrite' in kwargs and kwargs['spark_files_overwrite']) or ('true')
        spark_python_worker_memory = (kwargs is not None and 'spark_python_worker_memory' in kwargs and kwargs['spark_python_worker_memory']) or ('512m')
        spark_shuffle_file_buffer = (kwargs is not None and 'spark_shuffle_file_buffer' in kwargs and kwargs['spark_shuffle_file_buffer']) or ('256k')
        spark_reducer_maxSizeInFlight = (kwargs is not None and 'spark_shuffle_file_buffer' in kwargs and kwargs['spark_shuffle_file_buffer']) or ('96m')

        job_config = {"name": lg_run_id,
                      "file": "s3://az-us-lg-pilot-config/oozie-migration/source_code/StepExecutorV2.py",
                      "conf": {"spark.default.parallelism": spark_default_parallelism, "spark.driver.memory": driver_memory,
                               "spark.submit.deployMode":self.run_job_mode,
                               "spark.executor.memory": executor_memory, "spark.executor.cores": spark_executor_cores,
                               #"spark.serializer":"org.apache.spark.serializer", "spark.kryoserializer.buffer.max": spark_kryoserializer_buffer_max,
                               "spark.broadcast.blockSize": spark_broadcast_blockSize, "spark.files.overwrite": spark_files_overwrite,
                               "spark.python.worker.memory": spark_python_worker_memory, "spark.shuffle.file.buffer": spark_shuffle_file_buffer,
                               "spark.reducer.maxSizeInFlight": spark_reducer_maxSizeInFlight,
                               "spark.yarn.appMasterEnv.SPARK_HOME":'/usr/lib/spark',
                               "spark.executorEnv.SPARK_HOME":'/usr/lib/spark' },
                      "args": ["--dag-id", self.dag.dag_id + '_' + kwargs['table'] + '_table',
                               "--run-id", lg_run_id,
                               "--metadata-file-name", kwargs['table'] + '.txt',
                               "--load-date", self.getLastPartitionDay(),
                               '--skip-process',
                               self.skip_processed
                               ]}

        print('Using Livy Config--> {}'.format(job_config))

        return job_config


    def getdistcpyconfig(self, **kwargs):
        #lg_run_id = self.dag.dag_id + '_' + kwargs['table'] + '_distcp_' + str(datetime.now().timestamp())
        lg_run_id = str(uuid.uuid1())


        executor_memory = (kwargs is not None and 'executor_memory' in kwargs and kwargs['executor_memory']) or ('5g')
        driver_memory = (kwargs is not None and 'driver_memory' in kwargs and kwargs['driver_memory']) or ('3g')
        #num_executors = (kwargs is not None and 'num_executors' in kwargs and kwargs['num_executors']) or ('10')
        spark_default_parallelism = (kwargs is not None and 'spark_default_parallelism' in kwargs and kwargs['spark_default_parallelism']) or ('200')
        spark_executor_cores = (kwargs is not None and 'spark_executor_cores' in kwargs and kwargs['spark_executor_cores']) or ('4')
        spark_kryoserializer_buffer_max = (kwargs is not None and 'spark_kryoserializer_buffer_max' in kwargs and kwargs['spark_kryoserializer_buffer_max']) or ('512m')
        spark_broadcast_blockSize = (kwargs is not None and 'spark_broadcast_blockSize' in kwargs and kwargs['spark_broadcast_blockSize']) or ('4m')
        spark_files_overwrite = (kwargs is not None and 'spark_files_overwrite' in kwargs and kwargs['spark_files_overwrite']) or ('true')
        spark_python_worker_memory = (kwargs is not None and 'spark_python_worker_memory' in kwargs and kwargs['spark_python_worker_memory']) or ('512m')
        spark_shuffle_file_buffer = (kwargs is not None and 'spark_shuffle_file_buffer' in kwargs and kwargs['spark_shuffle_file_buffer']) or ('256k')
        spark_reducer_maxSizeInFlight = (kwargs is not None and 'spark_shuffle_file_buffer' in kwargs and kwargs['spark_shuffle_file_buffer']) or ('96m')

        job_config = {"name": lg_run_id,
                      "file": "s3://az-us-lg-pilot-config/oozie-migration/source_code/DistCp.py",
                      "conf": {"spark.default.parallelism": spark_default_parallelism,
                               "spark.driver.memory": driver_memory,
                               "spark.submit.deployMode": self.run_job_mode,
                               "spark.executor.memory": executor_memory, "spark.executor.cores": spark_executor_cores,
                               # "spark.serializer":"org.apache.spark.serializer", "spark.kryoserializer.buffer.max": spark_kryoserializer_buffer_max,
                               "spark.broadcast.blockSize": spark_broadcast_blockSize,
                               "spark.files.overwrite": spark_files_overwrite,
                               "spark.python.worker.memory": spark_python_worker_memory,
                               "spark.shuffle.file.buffer": spark_shuffle_file_buffer,
                               "spark.reducer.maxSizeInFlight": spark_reducer_maxSizeInFlight},
                      "args": ["--dag-id", self.dag.dag_id + '_' + kwargs['table'] + '_distcp',
                               "--run-id", lg_run_id,
                               "--load-date", self.getLastPartitionDay(),
                               "--source", kwargs['source'],
                               "--target", kwargs['table'],
                               '--skip-process',
                               self.skip_processed
                               ]}
        print('Using Livy Config--> {}'.format(job_config))
        return job_config

    def getLivyHsqlConfig(self, **kwargs):
        #lg_run_id = self.dag.dag_id + '_' + kwargs['table'] + '_hsql_' + str(datetime.now().timestamp())
        lg_run_id = str(uuid.uuid1())


        executor_memory = (kwargs is not None and 'executor_memory' in kwargs and kwargs['executor_memory']) or ('5g')
        driver_memory = (kwargs is not None and 'driver_memory' in kwargs and kwargs['driver_memory']) or ('3g')
        #num_executors = (kwargs is not None and 'num_executors' in kwargs and kwargs['num_executors']) or ('10')
        spark_default_parallelism = (kwargs is not None and 'spark_default_parallelism' in kwargs and kwargs['spark_default_parallelism']) or ('200')
        spark_executor_cores = (kwargs is not None and 'spark_executor_cores' in kwargs and kwargs['spark_executor_cores']) or ('4')
        spark_kryoserializer_buffer_max = (kwargs is not None and 'spark_kryoserializer_buffer_max' in kwargs and kwargs['spark_kryoserializer_buffer_max']) or ('512m')
        spark_broadcast_blockSize = (kwargs is not None and 'spark_broadcast_blockSize' in kwargs and kwargs['spark_broadcast_blockSize']) or ('4m')
        spark_files_overwrite = (kwargs is not None and 'spark_files_overwrite' in kwargs and kwargs['spark_files_overwrite']) or ('true')
        spark_python_worker_memory = (kwargs is not None and 'spark_python_worker_memory' in kwargs and kwargs['spark_python_worker_memory']) or ('512m')
        spark_shuffle_file_buffer = (kwargs is not None and 'spark_shuffle_file_buffer' in kwargs and kwargs['spark_shuffle_file_buffer']) or ('256k')
        spark_reducer_maxSizeInFlight = (kwargs is not None and 'spark_shuffle_file_buffer' in kwargs and kwargs['spark_shuffle_file_buffer']) or ('96m')

        job_config = {"name": lg_run_id,
                      "file": "s3://az-us-lg-pilot-config/oozie-migration/source_code/hsqlStep.py",
                      "conf": {"spark.default.parallelism": spark_default_parallelism,
                               "spark.driver.memory": driver_memory,
                               "spark.submit.deployMode": self.run_job_mode,
                               "spark.executor.memory": executor_memory, "spark.executor.cores": spark_executor_cores,
                               # "spark.serializer":"org.apache.spark.serializer", "spark.kryoserializer.buffer.max": spark_kryoserializer_buffer_max,
                               "spark.broadcast.blockSize": spark_broadcast_blockSize,
                               "spark.files.overwrite": spark_files_overwrite,
                               "spark.python.worker.memory": spark_python_worker_memory,
                               "spark.shuffle.file.buffer": spark_shuffle_file_buffer,
                               "spark.reducer.maxSizeInFlight": spark_reducer_maxSizeInFlight},
                      "args": ["--dag-id", self.dag.dag_id + '_' + kwargs['table'] + '_hsql',
                               "--run-id", lg_run_id,
                               "--load-date", self.getLastPartitionDay(), "--metadata-file-name", kwargs['hsql_file'],
                               "--table", kwargs['table'],
                               '--skip-process',
                               self.skip_processed
                               ]}
        print('Using Livy Config--> {}'.format(job_config))
        return job_config

    def createWatchStep(self, **kwargs):
        #task_nm = (kwargs is not None and 'dist_cp_id' in kwargs and kwargs['dist_cp_id'] + '_dstcp') or (kwargs['table'])

        task_nm = kwargs['operator'].task_id

        #step_id = "{{ task_instance.xcom_pull('" + kwargs['operator'].task_id + "', key='return_value')[0] }}"

        step_id = "{{ task_instance.xcom_pull('" + task_nm + "', key='return_value')[0] }}"
        print('Building Watch Step with task_nm {}'.format(task_nm))
        print('Building Watch Step with step_id {}'.format(step_id))

        emrWatchStep = EmrStepSensor(
        task_id=task_nm + '_watch_tsk',
        job_flow_id=self.cluster_Id,
        step_id=step_id,
        aws_conn_id='lg_emr_ingestion',
        dag=self.dag)

        return emrWatchStep


    def createTableStep(self, **kwargs):
        #task_nm = (kwargs is not None and 'tsk_id' in kwargs and kwargs['tsk_id']) or (kwargs['table'])

        task_nm = self.getTaskId(**kwargs)

        tableStep = EmrAddStepsOperator(
            task_id=task_nm,
            job_flow_id=self.cluster_Id,
            aws_conn_id='lg_emr_ingestion',
            steps=self.getEMRStepConfig(**kwargs),
            dag=self.dag
        )
        return tableStep

    def createDstCpStep(self, **kwargs):
        #task_nm = (kwargs is not None and 'tsk_id' in kwargs and kwargs['tsk_id']) or (kwargs['table'])
        task_nm = self.getTaskId(**kwargs)

        dstcpStep = EmrAddStepsOperator(
            task_id=task_nm ,
            job_flow_id=self.cluster_id,
            aws_conn_id='lg_emr_ingestion',
            provide_context=True,
            steps=self.getDistCpStepConfig(**kwargs),
            dag=self.dag
        )
        return dstcpStep

    def createHsqlStep(self, **kwargs):
        #task_nm = (kwargs is not None and 'tsk_id' in kwargs and kwargs['tsk_id']) or (kwargs['table'])
        task_nm = self.getTaskId(**kwargs)

        dstcpStep = EmrAddStepsOperator(
            task_id=task_nm,
            job_flow_id=self.cluster_id,
            aws_conn_id='lg_emr_ingestion',
            provide_context=True,
            steps=self.getHsqlStepConfig(**kwargs),
            dag=self.dag
        )
        return dstcpStep

    def trackstatementprogress(self, id):
        print('Begin waiting on Livy session for job acceptance status Session# {}'.format(id))
        max_attempts = self.task_submit_timeout / 60
        counter = 0
        while (max_attempts >= 0):
            max_attempts = max_attempts - 1
            counter = counter + 1
            r = requests.get(self.host + '/batches/' + id + '/state')
            if r.status_code != 200:
                print('Livy Session for the previously submitted job is not found. id {}'.format(id))
                r.raise_for_status()

            response = r.json()
            job_state = response['state']
            print('RESPONSE FROM THE LIVY SUBMIT for task {}, attempt {} and state {}'.format(id, counter, job_state))

            if job_state.lower() == 'accepted' or job_state.lower() == 'starting':
                time.sleep(60)
                continue
            elif job_state.lower() == 'failed' or job_state.lower() == 'killed' or job_state.lower() == 'dead':
                print('Job Failed or killed via Livy')
                raise AirflowException('Job Failed or killed via Livy. Forcing the task status to fail.')
            else:
                print('Final Job Status {} '.format(job_state))
                print(response)
                return True

        print('Job Failed to start within TIME OUT {} minutes on EMR via Livy'.format(self.task_submit_timeout/60))

        return False

    def sparkSubmit(self, **kwargs):

        if self.skip_process(**kwargs) == True:
            return 'SKIP'

        json_config = self.getLivyTableConfig(**kwargs)

        if 'task_type' in kwargs and kwargs['task_type'] == 'hsql':
            json_config = self.getLivyHsqlConfig( **kwargs)
        elif 'task_type' in kwargs and kwargs['task_type'] == 'distcp':
            json_config = self.getdistcpyconfig(**kwargs)

        #Retry 5 times before giving up.
        retry_count = 5
        while retry_count >= 0 :
            retry_count -= 1
            r = requests.post(self.host + '/batches', data=json.dumps(json_config))
            if r.status_code >= 200 and r.status_code < 300:
                response = r.json()
                kwargs['task_livy_id'] = response['id']
                print('Job submitted to Livy. Checking to see if the job goes into Running state.')
                #kwargs['ti'].xcom_push(key=self.dag.dag_id + "-" + kwargs['table'], value=str(response['id']))

                #kwargs['task_instance'].xcom_push(key=self.dag.dag_id + "-" + kwargs['table'], value=response['name'])
                run_id = response['name']
                livy_session_id = str(response['id'])

                if self.trackstatementprogress(livy_session_id):
                    print('Job started on EMR via Livy')
                    return run_id
            elif r.status_code != 200 and retry_count >= 0:
                print('Failed to submit job, retrying it. HTTP Response# {}'.format(r.json()))
                time.sleep(30)
                continue


        print('Job Failed to start within TIME OUT on EMR via Livy')

        raise AirflowException('Failed to start the Livy Job in a timely manner. TIME OUT # {}'.format(
            self.task_submit_timeout))

    # def deletestep(self, **kwargs):
    #     if self.skip_processed == 'True':
    #         print('Skipping the Delete of Job Tracker File as Skip Processed is turned on')
    #         return
    #
    #     job_success_marker = 'etl/audit/'
    #     if 'task_type' in kwargs and kwargs['task_type'] == 'hsql':
    #         job_success_marker = job_success_marker + 'hsql/' + 'dag_' + self.dag.dag_id + '_task_' + kwargs['table'] + '_loaddate_' + self.getLastPartitionDay() + '_SUCCESS'
    #     elif 'task_type' in kwargs and kwargs['task_type'] == 'distcp':
    #         job_success_marker = job_success_marker + 'distcp/' + 'dag_' + self.dag.dag_id + '_task_' + kwargs['table'] + '_loaddate_' + self.getLastPartitionDay() + '_SUCCESS'
    #     else:
    #         job_success_marker = job_success_marker + 'dag_' + self.dag.dag_id + '_task_' + kwargs['table'] + '_loaddate_' + self.getLastPartitionDay() + '_SUCCESS'
    #
    #
    #     print('deleting Job Tracker File {}{}'.format('s3://az-us-lg-pilot-config/', job_success_marker))
    #
    #     try:
    #         r = self.s3client.head_object(Bucket='az-us-lg-pilot-config',
    #                                Key=job_success_marker)
    #         if r['ResponseMetadata']['HTTPStatusCode'] == 200:
    #             print('Job Tracker File found {}{}. Will continue the job'.format('s3://az-us-lg-pilot-config/',
    #                 job_success_marker))
    #         else:
    #             print('Error querying Job Tracker File. HTTP response code {}'.format(r['ResponseMetadata']['HTTPStatusCode']))
    #     except ClientError as e:
    #         if int(e.response['Error']['Code']) == 404:
    #             print('Job Tracker File NOT found {}{}. Will continue the job'.format('s3://az-us-lg-pilot-config/',
    #                 job_success_marker))
    #         else:
    #             raise e
    #
    #     response = self.s3client.delete_object(Bucket='az-us-lg-pilot-config',
    #                                Key=job_success_marker)
    #     print(response)

    def createlivytablestep(self, previous_task, **kwargs):
        task_nm = self.getTaskId(**kwargs)

        tableStep = PythonOperator(
            task_id=task_nm,
            provide_context=True,
            python_callable=self.sparkSubmit,
            op_kwargs=kwargs,
            dag=self.dag)

        watchStep = PythonOperator(
            task_id=task_nm + '_watch',
            provide_context=True,
            python_callable=self.createlivywatchV2,
            templates_dict={'run-id': "{{ ti.xcom_pull(task_ids='" + task_nm + "') }}"},
            #op_args={'parent_task_id':tableStep.task_id},
            #op_args={'run-id': "{{ ti.xcom_pull(task_ids='" + task_nm + "') }}"},
            dag=self.dag)

        #tableStepSensor = self.createlivywatch(operator=tableStep, **kwargs)
        #tableStepSensor = self.createlivywatchV2(operator=tableStep, **kwargs)

        if previous_task is not None:
            previous_task >> tableStep

        tableStep >> watchStep

        #return [tableStep, watchStep]

        return watchStep

    def createlivyhsqlstep(self, previous_task, **kwargs):
        return self.createlivytablestep(previous_task, **kwargs)

    def createlivycreatedstcpstep(self, previous_task, **kwargs):
        step = self.createlivytablestep(previous_task, **kwargs)

        #step_checker = self.createlivywatch(operator=step, table=kwargs['table'])

        return step

    def createlivywatch(self, operator, **kwargs):
        task_nm = operator.task_id
        job_success_marker = 's3://az-us-lg-pilot-config/etl/audit/'

        if 'task_type' in kwargs and kwargs['task_type'] == 'hsql':
            job_success_marker = job_success_marker + 'hsql/' + 'dag_' + self.dag.dag_id + '_task_' + kwargs['table'] + '_loaddate_' + self.getLastPartitionDay() + '_SUCCESS'
        elif 'task_type' in kwargs and kwargs['task_type'] == 'distcp':
            job_success_marker = job_success_marker + 'distcp/' + 'dag_' + self.dag.dag_id + '_task_' + kwargs[
                'table'] + '_loaddate_' + self.getLastPartitionDay() + '_SUCCESS'
        else:
            job_success_marker = job_success_marker + 'dag_' + self.dag.dag_id + '_task_' + kwargs['table'] + '_loaddate_' + self.getLastPartitionDay() + '_SUCCESS'

        print('Task {} Livy Watch Sensor waiting on file {}'.format(task_nm, job_success_marker))

        time_out = (kwargs is not None and 'task_timeout' in kwargs and kwargs['task_timeout']) or self.task_timeout
        file_sensor = S3KeySensor(
            task_id=task_nm + '_watch_tsk',
            aws_conn_id='lg_s3_connection',
            poke_interval=60, # (seconds); checking file every minute
            timeout=time_out,
            bucket_key=job_success_marker,
            bucket_name=None,
            wildcard_match=True,
            dag=self.dag)

        return file_sensor

    #Livy Manages sessions internally and attempts to resubmit a job [regardless of its status [success or failure)
    #results in bad request or 400. It considers it as a duplicate session with the same name.
    def delete_preexisting_livy_job(self, **kwargs):
        print('Delete any preexisting in flight Livy sessions for the same task')
        try:
            r = requests.get(self.host + '/batches')
            if r.status_code != 200:
                print('Failed to fetch status of the batches with GET request from Livy')
                print(r)
                #Ignore error
                return

            response = r.json()
            jobs = response['sessions']
            for s in jobs:
                if s['name'] == self.dag.dag_id + '-' + kwargs['table']:
                    id = str(s['id'])
                    print('Found an existing session with the same name [DAG + TABLE] {}. Deleting it before submitting the task session id {}'.format(self.dag.dag_id + '-' + kwargs['table'], id))
                    requests.delete(self.host + '/batches/' + id)
        except Exception as e:
            print('Failed to delete session with Livy DELETE request')
            print(e)
            pass


    # def s3monitor(self, **kwargs):
    #
    #     job_success_marker = 'etl/audit/'
    #     if 'task_type' in kwargs and kwargs['task_type'] == 'hsql':
    #         job_success_marker = job_success_marker + 'hsql/' + 'dag_' + self.dag.dag_id + '_task_' + kwargs['table'] + '_loaddate_' + self.getLastPartitionDay() + '_SUCCESS'
    #     elif 'task_type' in kwargs and kwargs['task_type'] == 'distcp':
    #         job_success_marker = job_success_marker + 'distcp/' + 'dag_' + self.dag.dag_id + '_task_' + kwargs['table'] + '_loaddate_' + self.getLastPartitionDay() + '_SUCCESS'
    #     else:
    #         job_success_marker = job_success_marker + 'dag_' + self.dag.dag_id + '_task_' + kwargs['table'] + '_loaddate_' + self.getLastPartitionDay() + '_SUCCESS'
    #
    #
    #     print('Waiting on Job Tracker File {}{}'.format('s3://az-us-lg-pilot-config/', job_success_marker))
    #
    #     max_attempts = self.task_timeout / 60
    #
    #     while(max_attempts >= 0):
    #         try:
    #             max_attempts = max_attempts - 1
    #
    #             r = self.s3client.head_object(Bucket='az-us-lg-pilot-config',
    #                                    Key=job_success_marker)
    #             if r['ResponseMetadata']['HTTPStatusCode'] == 200:
    #                 print('Job Tracker File found {}{}. Will continue the job'.format('s3://az-us-lg-pilot-config/',
    #                     job_success_marker))
    #                 return True
    #             else:
    #                 print('Continue to monitor....')
    #                 time.sleep(100)
    #                 continue
    #         except ClientError as e:
    #             print(e)
    #             time.sleep(100)
    #             continue
    #
    #     print('Failed to detect Job Tracker File. {}{}. Failing the task'.format('s3://az-us-lg-pilot-config/',
    #                                                                   job_success_marker))
    #
    #     raise AirflowException('Failed to detect Job Tracker File. Failing the task')

    def createlivywatchV2(self, **context):
        print(context)
        print('run id type {}'.format(type(context['templates_dict']['run-id'])))
        print('value of run id {}'.format(context['templates_dict']['run-id']))
        print('value of run id {}'.format(context['templates_dict']['run-id'][0]))
        task_nm = context['templates_dict']['run-id']

        print('Task Name Parameter# {}'.format(task_nm))
        #run_id = context['task_instance'].xcom_pull(task_ids='run-id')[0]

        run_id = task_nm
        if run_id == 'SKIP':
            print('As the Table step is skipped, skipping the watch')
            return True

        print('Waiting on job completion. Tracking RUN_ID {}'.format(run_id))

        # sensor = SqlSensor(
        #     task_id=task_nm + '_watch_tsk',
        #     conn_id='lg_job_tracker',
        #     sql="SELECT STATUS FROM lgetl.JOB_AUDIT where RUN_ID='" + kwargs['ti'].xcom_pull(key=self.dag.dag_id +
        #      "-" + kwargs['table'], task_ids=operator.task_id) + "'",
        #     poke_interval=60,  # (seconds); checking file every minute
        #     timeout=self.task_timeout,
        #     dag=self.dag)


        connection = MySqlHook(mysql_conn_id='lg_job_tracker')
        query = "SELECT STATUS from JOB_AUDIT where STATUS != 'STARTED' and RUN_ID=%(run_id)s"
        print('Running query for Run_id {}'.format(run_id))


        max_attempts = self.task_timeout / 60
        while(max_attempts >= 0):
            max_attempts -= 1
            print('Attempting to Job Tracker Status')
            row = connection.get_first(query, parameters={'run_id':run_id})
            print('Value for DBhook {}'.format(row))
            if row is not None and len(row) > 0:
                print(type(row))
                print(row[0])
                if row[0] == 'SUCCESS':
                    return True
                elif row[0] == 'FAIL':
                    break
            else:
                time.sleep(60)
                print('Job failed. No Job Tracker record')

        raise AirflowException('Failed to detect Job Tracker File. Failing the task')



    def get_job_id(self, **kwargs):
        if 'task_type' in kwargs and kwargs['task_type'] == 'hsql':
            return  self.dag.dag_id + '_' + kwargs['table'] + '_hsql'
        elif 'task_type' in kwargs and kwargs['task_type'] == 'distcp':
            return  self.dag.dag_id + '_' + kwargs['table'] + '_distcp'
        else:
            return  self.dag.dag_id + '_' + kwargs['table'] + '_table'

    def skip_process(self, **kwargs):
        if self.skip_processed == 'False':
            print('Skip Process is turned off')
            return False

        print('kip Process Check for lgetl.JOB_AUDIT entry')

        connection = MySqlHook(mysql_conn_id='lg_job_tracker')
        query = "select status, MAX(START_TIME) from lgetl.JOB_AUDIT WHERE LOAD_DATE = %(load_date)s and JOB_ID = %(dag_id)s"

        row = connection.get_first(query, parameters={'load_date': self.getLastPartitionDay(),'dag_id': self.get_job_id(**kwargs)})
        print('Value for DBhook {}'.format(row))
        if row is not None and len(row) > 0:
            print(row[0])
            if row[0] == 'SUCCESS':
                return True

        return False

